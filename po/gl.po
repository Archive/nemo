# translation of nemo.master.po to
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# Marcoslans <antiparvos@gmail.com>, 2010.
#
msgid ""
msgstr ""
"Project-Id-Version: nemo.master\n"
"Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?"
"product=nemo&component=general\n"
"POT-Creation-Date: 2010-02-23 22:00+0000\n"
"PO-Revision-Date: 2010-01-12 00:09+0100\n"
"Last-Translator: Antón Méixome\n"
"Language-Team:  <gnome@g11n.net>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1)\n"
"X-Poedit-Language: Galician\n"
"X-Generator: KBabel 1.11.4\n"

#: ../broker/Tracker.cs:56 ../broker/Xesam.cs:49
msgid "ERROR: Could not connect to the Dbus session"
msgstr "ERRO: Non foi posíbel conectar á sesión Dbus"

#: ../broker/Tracker.cs:57 ../broker/Xesam.cs:50
msgid "Please check the DBUS_SESSION_BUS_ADDRESS environment variable"
msgstr "Comprobe a variábel de contorno DBUS_SESSION_BUS_ADDRESS"

#: ../broker/Tracker.cs:58 ../broker/Xesam.cs:51
msgid "This is normally set by running export `dbus-launch`"
msgstr "Normalmente pódese facer isto mediante exportar `dbus-launch`"

#. 0 = day of week, 1 = day of month, 2 = month
#: ../gtk/CalendarDrawer.cs:51
#, csharp-format
msgid "{0}, {1}/{2}"
msgstr "{0}, {1}/{2}"

#: ../gtk/CalendarDrawer.cs:58 ../gtk/CalendarDrawer.cs:109
#, csharp-format
msgid "Zoom to {0}"
msgstr "Ampliar até o {0}"

#: ../gtk/CalendarDrawer.cs:72
#, csharp-format
msgid "Sat, {0}/{1} - Sun, {2}/{3}"
msgstr "Sáb. {0}/{1} - Dom. {2}/{3}"

#: ../gtk/CalendarDrawer.cs:79
#, csharp-format
msgid "Zoom to Sat {0} {1}"
msgstr "Ampliar até o sábado {0} de {1}"

#: ../gtk/CalendarDrawer.cs:98
#, csharp-format
msgid "Week {0}"
msgstr "Semana {0}"

#: ../gtk/CalendarDrawer.cs:470
msgid "Previous page"
msgstr "Páxina anterior"

#. next
#: ../gtk/CalendarDrawer.cs:493 ../gtk/SearchPopup.cs:110
msgid "Next page"
msgstr "Páxina seguinte"

#: ../gtk/CalendarDrawer.cs:522
msgid "Sorry, no files for this day"
msgstr "Non hai ficheiros para este día"

#: ../gtk/CalendarDrawer.cs:551
msgid "This week"
msgstr "Esta semana"

#: ../gtk/CalendarDrawer.cs:552
#, csharp-format
msgid "Week {0} - {1}"
msgstr "Semana {0} - {1}"

#: ../gtk/CalendarDrawer.cs:742
msgid "This month"
msgstr "Este mes"

#: ../gtk/CalendarDrawer.cs:849
#, csharp-format
msgid "Zoom to {0} {1}, {2}"
msgstr "Ampliar até o {0} {1} de {2}"

#: ../gtk/CalendarDrawer.cs:881
msgid "This year"
msgstr "Este ano"

#: ../gtk/CalendarDrawer.cs:899
#, csharp-format
msgid "Zoom to {0}, {1}"
msgstr "Ampliar até o {0} de {1}"

#: ../gtk/CategoryDrawer.cs:97
msgid "Rename label"
msgstr "Cambiar o nome da etiqueta"

#: ../gtk/CategoryDrawer.cs:101
msgid "Delete label"
msgstr "Eliminar a etiqueta"

#: ../gtk/CategoryDrawer.cs:107 ../gtk/CategoryDrawer.cs:320
msgid "Create a new label"
msgstr "Crear unha etiqueta nova"

#: ../gtk/CategoryDrawer.cs:324
msgid "Rename category"
msgstr "Cambiar o nome da categoría"

#: ../gtk/CategoryDrawer.cs:329
msgid "Delete category"
msgstr "Eliminar a categoría"

#: ../gtk/CategoryDrawer.cs:336
msgid "Create a new category"
msgstr "Crear unha categoría nova"

#: ../gtk/Common.cs:61 ../gtk/DisplayItem.cs:117
msgid "Open"
msgstr "Abrir"

#: ../gtk/Common.cs:65
msgid "Open with"
msgstr "Abrir con"

#: ../gtk/Common.cs:87
msgid "Open dir containing file"
msgstr "Abrir o cartafol que contén o ficheiro"

#: ../gtk/Common.cs:100
msgid "Copy path to clipboard"
msgstr "Copiar o camiño ao portapapeis"

#: ../gtk/Common.cs:109
msgid "Labels"
msgstr "Etiquetas"

#: ../gtk/Common.cs:167
msgid "Add star"
msgstr "Engadir unha marca"

#: ../gtk/Common.cs:169
msgid "Remove star"
msgstr "Eliminar a marca"

#: ../gtk/Common.cs:215
msgid "January"
msgstr "Xaneiro"

#: ../gtk/Common.cs:217
msgid "February"
msgstr "Febreiro"

#: ../gtk/Common.cs:219
msgid "March"
msgstr "Marzo"

#: ../gtk/Common.cs:221
msgid "April"
msgstr "Abril"

#: ../gtk/Common.cs:223
msgid "May"
msgstr "Maio"

#: ../gtk/Common.cs:225
msgid "June"
msgstr "Xuño"

#: ../gtk/Common.cs:227
msgid "July"
msgstr "Xullo"

#: ../gtk/Common.cs:229
msgid "August"
msgstr "Agosto"

#: ../gtk/Common.cs:231
msgid "September"
msgstr "Setembro"

#: ../gtk/Common.cs:233
msgid "October"
msgstr "Outubro"

#: ../gtk/Common.cs:235
msgid "November"
msgstr "Novembro"

#: ../gtk/Common.cs:237
msgid "December"
msgstr "Decembro"

#: ../gtk/Common.cs:246
msgid "Monday"
msgstr "Luns"

#: ../gtk/Common.cs:248
msgid "Tuesday"
msgstr "Martes"

#: ../gtk/Common.cs:250
msgid "Wednesday"
msgstr "Mércores"

#: ../gtk/Common.cs:252
msgid "Thursday"
msgstr "Xoves"

#: ../gtk/Common.cs:254
msgid "Friday"
msgstr "Venres"

#: ../gtk/Common.cs:256
msgid "Saturday"
msgstr "Sábado"

#: ../gtk/Common.cs:258
msgid "Sunday"
msgstr "Domingo"

#: ../gtk/DisplayDocumentItem.cs:27
msgid "Title:"
msgstr "Título:"

#: ../gtk/DisplayDocumentItem.cs:28
msgid "Pages:"
msgstr "Páxinas:"

#: ../gtk/DisplayItem.cs:182
msgid "Path:"
msgstr "Camiño:"

#: ../gtk/DisplayItem.cs:183
msgid "Size:"
msgstr "Tamaño:"

#: ../gtk/DisplayItem.cs:184
msgid "Last modified:"
msgstr "Última modificación:"

#: ../gtk/DisplayItem.cs:185
msgid "Last accessed:"
msgstr "Último acceso:"

#: ../gtk/DisplayPictureItem.cs:21
msgid "Resolution:"
msgstr "Resolución:"

#: ../gtk/FixedResizeWidget.cs:204 ../gtk/FixedResizeWidget.cs:230
#: ../gtk/FixedResizeWidget.cs:384 ../gtk/FixedResizeWidget.cs:410
#, csharp-format
msgid "+ {0} more"
msgstr "+ {0} máis"

#: ../gtk/MainWindow.cs:37
msgid "MONO_MANAGED_WATCHER is buggy, please don't use that."
msgstr ""
"Non debe empregar MONO_MANAGED_WATCHER porque  funciona defectuosamente."

#: ../gtk/SearchPopup.cs:96
msgid "No elements found"
msgstr "Non se atopou elemento ningún"

#. prev
#: ../gtk/SearchPopup.cs:107
msgid "Prev page"
msgstr "Páxina anterior"

#: ../gtk/SearchPopup.cs:182
#, csharp-format
msgid "Search results for: {0}"
msgstr "Resultados da busca para: {0}"

#: ../gtk/SearchPopup.cs:256
#, csharp-format
msgid "Open: {0}"
msgstr "Abrir: {0}"

#: ../gtk/SearchPopup.cs:317
msgid "Go to week"
msgstr "Ir á semana"

#: ../gtk/SearchPopup.cs:318
msgid "Go to month"
msgstr "Ir ao mes"

#: ../gtk/SearchPopup.cs:319
msgid "Go to year"
msgstr "Ir ao ano"

#: ../gtk/Tray.cs:64
msgid "Show window"
msgstr "Mostrar a xanela"

#: ../gtk/Tray.cs:66
msgid "Hide window"
msgstr "Ocultar a xanela"

#: ../gtk/Tray.cs:83 ../gtk-gui/Nemo.Indexing.cs:34
msgid "Indexing status"
msgstr "Estado da indexación"

#: ../gtk/Tray.cs:89
msgid "About"
msgstr "Sobre"

#: ../gtk/Tray.cs:97
msgid ""
"Nemo is a file manager for those who would rather have their files manage "
"themselves."
msgstr ""
"Nemo é un xestor de ficheiros para os que prefiren que os seus ficheiros se "
"xestionen sós."

#. add quit
#: ../gtk/Tray.cs:106
msgid "Quit"
msgstr "Saír"

#: ../gtk-gui/MainWindow.cs:77
msgid "Nemo 0.2.4"
msgstr "Nemo 0.2.4"

#: ../gtk-gui/MainWindow.cs:96
msgid "      All Files"
msgstr "      Todos os ficheiros"

#: ../gtk-gui/MainWindow.cs:120
msgid " Starred"
msgstr "Marcados"

#: ../gtk-gui/MainWindow.cs:200
msgid "Search"
msgstr "Buscar"

#: ../gtk-gui/MainWindow.cs:288
msgid "Today"
msgstr "Hoxe"

#: ../gtk-gui/MainWindow.cs:324
msgid "Day"
msgstr "Día"

#: ../gtk-gui/MainWindow.cs:335
msgid "Week"
msgstr "Semana"

#: ../gtk-gui/MainWindow.cs:346
msgid "Month"
msgstr "Mes"

#: ../gtk-gui/MainWindow.cs:357
msgid "Year"
msgstr "Ano"

#: ../gtk-gui/Nemo.Indexing.cs:49
msgid "Indexer:"
msgstr "Indexador:"

#: ../gtk-gui/Nemo.Indexing.cs:57
msgid "Idle"
msgstr "Inactivo"

#: ../gtk-gui/Nemo.Indexing.cs:94
msgid "Full reindex"
msgstr "Reindexación completa"

#: ../metadata/MetadataStore.cs:1107
msgid "All"
msgstr "Todos"

#: ../uicommon/Categories.cs:46
msgid "New Category"
msgstr "Categoría nova"

#. FIXME: hardcoded
#: ../uicommon/Types.cs:30
msgid "Other"
msgstr "Outros"

#: ../uicommon/Types.cs:31
msgid "Documents"
msgstr "Documentos"

#: ../uicommon/Types.cs:32
msgid "Pdfs"
msgstr "PDF"

#: ../uicommon/Types.cs:33
msgid "Spreadsheets"
msgstr "Follas de cálculo"

#: ../uicommon/Types.cs:34
msgid "Presentations"
msgstr "Presentacións"

#: ../uicommon/Types.cs:35
msgid "Images"
msgstr "Imaxes"

#: ../uicommon/Types.cs:36
msgid "Videos"
msgstr "Vídeos"

#: ../uicommon/Types.cs:37
msgid "Archives"
msgstr "Arquivos"
