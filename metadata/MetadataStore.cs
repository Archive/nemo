using System.Threading;
using System.Collections.Generic;
using System.IO;
using System.Data;
using System;
using System.Text;
using Mono.Data.Sqlite;
using System.Text.RegularExpressions;
using System.Diagnostics;

namespace Nemo 
{
    public class Query
    {
        public List<MetaLabel> labels = new List<MetaLabel>();
        public List<long> types = new List<long>();
        public DateTime date_start;
        public DateTime date_end;
		public bool starred;
    }

    public enum FileTypeCategory {
        None, Document, Pdf, Spreadsheet, Presentation, Image, Video, Archive
    }
    
    public class FileType
    {
        public long db_id;
        public string type;
        public FileTypeCategory category;
    }
    
    public class MetaLabel: IComparable
    {
    	public bool initial;
        public long db_id;
        public string label;
        public Color color = new Color();
        public long parent_db_id; // -1 means no parent

        public int CompareTo(Object other) 
        {
            MetaLabel o = (MetaLabel) other;
            int label_cmp = label.CompareTo(o.label);
            if (label_cmp == 0)
                return db_id.CompareTo(o.db_id);
            else
                return label_cmp;
        }
    }
    
    public class File
    {
        public long db_id;
        public string path;
        public DateTime last_accessed, last_modified;
        public long bytesize;
        public List<MetaLabel> labels;
        public FileType type;
		public bool starred;
	}

    public struct MostActiveResult 
    {
        public DateTime period;
        public int no_results;
        public Tuple<string, int>[] labels;
    }

    /*
      public interface IMetadataStore
      {
      // enable/disable generation of events
      void suppress_events(bool suppress);
        
      enum Period { Week, Month, Year };
        
      MostActiveResult[] get_most_active(string[] labels, string[] types,
      double[] date_range, Period period,
      int no_active);

      }
    */

	public class UserLabelColors
	{
		List<Color> colors;
	
		public int index;
	
		public UserLabelColors()
		{
			index = 0;
			
			colors = new List<Color>();
			
			colors.Add(new Color(244, 124, 32));
			colors.Add(new Color(50, 166, 228));
			colors.Add(new Color(152, 97, 194));
			colors.Add(new Color(107, 157, 65));
			colors.Add(new Color(255, 196, 12));
			colors.Add(new Color(0, 95, 164));
			colors.Add(new Color(240, 110, 169));
			colors.Add(new Color(210, 4, 98));
			colors.Add(new Color(180, 173, 53));
		}
		
		public Color unique_color()
		{
			Color c = colors[index];
			index = ++index % colors.Count;
			return c;
		}
	}

    public class MetadataStore
    {
        private SqliteDatabase database;
        private FileSystemWatcher watcher;

        private Thread watcher_thread;
		private Thread db_runner_thread;
		
		static readonly int min_number_of_wait_cycles = 10; 		
		static readonly int wait_cycles_scaling_factor = 4; 		
		
		// helper function to turn an enumeration into a enumerator that will fit into the work queues
		private IEnumerator<bool> TurnEnumerableIntoEnumerator(IEnumerable<bool> enumerable)
		{
			IEnumerator<bool> t = enumerable.GetEnumerator();
	    
			while (t.MoveNext()) {
				yield return t.Current;
			}
	    
			// make compiler happy
			yield break;
		}
		
		// helper function to turn a function into a Enumerator that will fit into the work queues
		private IEnumerator<bool> TurnFunctionIntoEnumerator(VoidFunction func)
		{
			func(); 

			// make compiler happy
			yield break;
		}
			
		// IEnumerable<bool> to support continuing functions (break functions into pieces to lower cpu load)
		private LinkedList<IEnumerator<bool>> background_db_work;
		private LinkedList<IEnumerator<bool>> db_work;
        private volatile bool stop_db_runner;
        private AutoResetEvent work_ready;
        private volatile string watch_path;
        private Query current_query;
        private VoidFunction<List<File>> query_result_changed_callback;
        private VoidFunction<List<MetaLabel>> labels_changed_callback;
        private VoidFunction<List<Tuple<FileTypeCategory, int>>> type_labels_changed_callback;
        
        string[] exclude_dirs;
        string[] excludes;
        string[] includes;

		// for reindexing
		List<string> indexed_filenames;
		
        // main thread methods
        public MetadataStore(string watch_path)
        {
            this.watch_path = new FileInfo(watch_path).FullName;
            current_query = null;
            database_work_count = 0;

            excludes = Singleton<Configuration>.Instance.data.exclude_files.Split(' ');
            includes = Singleton<Configuration>.Instance.data.include_files.Split(' ');
            exclude_dirs = Singleton<Configuration>.Instance.data.exclude_dirs.Split(' ');
			
			indexed_filenames = new List<string>();
        }

		// workaround braindead filter interface on FileSystemWatcher
		private bool filename_is_good(string filename)
		{
			if (database.database_filename == filename)
				return false;
			
			foreach (string v in exclude_dirs)
				if (filename.Contains(v))
					return false;

			bool found = false;

			int index = filename.LastIndexOf('.');
			string extension = index != -1 ? filename.Substring(index) : ""; 
			extension = extension.ToLower();
			
			foreach (string v in includes)
				if (extension == v) {
					found = true;
					break;
				}
				
			if (!found)
				return false;
				
			foreach (string v in excludes)
				if (extension == v)
					return false;
	
			// regex slow
			// return include_regex.IsMatch(filename) && !exclude_regex.IsMatch(filename);

			return true;
		}

		private bool directory_is_good(string dir)
		{
			foreach (string v in exclude_dirs)
				if (dir.Contains(v))
					return false;

			// regex slow
			// return include_regex.IsMatch(filename) && !exclude_regex.IsMatch(filename);

			return true;
		}

		private void watcher_runner()
		{
		    if (System.IO.Directory.Exists(watch_path)) {
			watcher = new FileSystemWatcher(watch_path);

			System.Console.WriteLine("watching dir {0}", watch_path);

			watcher.IncludeSubdirectories = true;
			watcher.NotifyFilter = //NotifyFilters.CreationTime |
			    NotifyFilters.FileName | NotifyFilters.LastAccess |
			    NotifyFilters.LastWrite | NotifyFilters.Size;

			watcher.Error += on_watch_error;
			watcher.Changed += on_file_changed;
			watcher.Created += on_file_created;
			watcher.Deleted += on_file_deleted;
			watcher.Renamed += on_file_renamed;

			watcher.EnableRaisingEvents = true;
		    } else {
			System.Console.WriteLine("dir to watch doesn't exist {0}", watch_path);
			System.Environment.Exit(0);
		    }
		}
		
		private string database_dir;
		
        public void start(string database_dir)
        {
			this.database_dir = database_dir;
            db_work = new LinkedList<IEnumerator<bool>>();
			background_db_work = new LinkedList<IEnumerator<bool>>();
            work_ready = new AutoResetEvent(false);
            
            watcher_thread = new Thread(watcher_runner);
            watcher_thread.Start();

			stop_db_runner = false;
            db_runner_thread = new Thread(db_runner);
            db_runner_thread.Start();
        }
        
        public void stop()
        {
	    if (watcher != null)
		watcher.EnableRaisingEvents = false;
            stop_db_runner = true;
            work_ready.Set(); // one last dance?
        }

        public void sync_with_filesystem()
        {
			Helpers.RunInMainThread(delegate {
				Singleton<Indexing>.Instance.change_status(true);
			})();
        	
            add_low_priority_db_work(TurnEnumerableIntoEnumerator(db_sync_with_filesystem(watch_path)));         	
        }
        
        // set query to use for query_changed event, note DON'T touch the query
        // object after calling this function
        public void set_query(Query query)
        {
            query.labels.Sort();
            add_db_work(delegate { current_query = query; });
        }

        public void set_query_result_callback(VoidFunction<List<File>> cb)
        {
            add_db_work(delegate { query_result_changed_callback = cb; });
        }
        
        // fire query event now
        public void trigger_query_result_changed()
        {
            add_db_work(delegate { db_search(current_query, query_result_changed_callback); });
        }

        public void set_labels_changed_callback(VoidFunction<List<MetaLabel>> cb)
        {
            add_db_work(delegate { labels_changed_callback = cb; });
        }
        
        // fire labels event now
        public void trigger_labels_changed()
        {
            add_db_work(db_trigger_labels_changed);
        }

        public void set_type_labels_changed_callback(VoidFunction<List<Tuple<FileTypeCategory, int>>> cb)
        {
            add_db_work(delegate { type_labels_changed_callback = cb; });
        }
        
        // fire labels event now
        public void trigger_type_labels_changed()
        {
            add_db_work(db_trigger_type_labels_changed);
        }

        // set query to use for query_changed event, note DON'T touch the label
        // and file objects after calling this function
        public void add_label_to_file(File file, MetaLabel label, VoidFunction callback)
        {
            add_db_work(delegate { db_add_label_to_file(file, label, callback); });
        }

        // set query to use for query_changed event, note DON'T touch the label
        // and file objects after calling this function
        public void remove_label_from_file(File file, MetaLabel label, VoidFunction callback)
        {
            add_db_work(delegate { db_remove_label_from_file(file, label, callback); });
        }

        // add prototype label to database, note that label_prototype MUST be
        // thrown away afterwards since its database id will not be filled in
        public void add_label(MetaLabel label_prototype, VoidFunction callback)
        {
            add_db_work(delegate { db_add_label(label_prototype, callback); });
        }
        
        // recursively remove label and sublabels etc., including from the
        // files that are connected to the labels, will update the query result
        // as necessary and call the labels changed callback, note DON'T
        // touch the query object after calling this function
        public void remove_label(MetaLabel label, VoidFunction callback)
        {
            add_db_work(delegate { db_remove_label(label, callback); });
        }

		public void rename_label(MetaLabel label, string name)
		{
			add_db_work(delegate { db_rename_label(label, name); });
		}
        
        public void set_starred(File file, bool starred, VoidFunction callback)
        {
            add_db_work(delegate { db_update_starred(file, starred, callback); });
        }

        // search for stuff, returning result through callback, note DON'T
        // touch the query object after calling this function
        public void search(Query query, VoidFunction<List<File>> callback)
        {
            add_db_work(delegate { db_search(query, callback); });
        }
        
        public void get_file_from_path(string path, VoidFunction<File> callback)
        {
            add_db_work(delegate { callback(database.get_file_from_path(path)); });
        }
		
        public void get_files_from_paths(List<string> paths, VoidFunction<List<File>> callback)
        {
            add_db_work(delegate { callback(database.get_files_from_paths(paths)); });
        }

        private void add_low_priority_db_work(IEnumerator<bool> item)
        {
            lock (background_db_work)
                background_db_work.AddLast(item);
            work_ready.Set();
        }

		private void add_low_priority_db_work(VoidFunction item)
        {
            lock (background_db_work)
                background_db_work.AddLast(TurnFunctionIntoEnumerator(item));
            work_ready.Set();
        }

        private void add_db_work(VoidFunction item)
        {
            lock (db_work)
                db_work.AddLast(TurnFunctionIntoEnumerator(item));
            work_ready.Set();
        }
        
        // event handlers (thread pool methods)
        private void on_file_changed(object sender, FileSystemEventArgs e) 
        {
			if (!watcher.EnableRaisingEvents)
				return; // mono bug
			else if (!filename_is_good(e.FullPath))
				return;

            System.Console.WriteLine("GOT CHANGE");
            add_low_priority_db_work(delegate { db_handle_change(e.FullPath); });
        }

        private void on_file_created(object sender, FileSystemEventArgs e) 
        {
			if (!watcher.EnableRaisingEvents)
				return; // mono bug
			else if (!filename_is_good(e.FullPath))
				return;

            System.Console.WriteLine("GOT CREATE");
            add_low_priority_db_work(delegate { db_handle_create(e.FullPath); });
        }

        private void on_file_deleted(object sender, FileSystemEventArgs e) 
        {
			if (!watcher.EnableRaisingEvents)
				return; // mono bug
			else if (!filename_is_good(e.FullPath))
				return;

            System.Console.WriteLine("GOT DELETE");
            add_low_priority_db_work(delegate { db_handle_delete(e.FullPath); });
        }
        
        private void on_file_renamed(object sender, RenamedEventArgs e) 
        {
			if (!watcher.EnableRaisingEvents)
				return; // mono bug
			else if (!filename_is_good(e.FullPath))
				return;

            System.Console.WriteLine("GOT RENAME");
            add_low_priority_db_work(delegate { db_handle_rename(e); });
        }
        
        private void on_watch_error(object sender, ErrorEventArgs args)
        {
			if (!watcher.EnableRaisingEvents)
				return; // mono bug

            // FIXME
            System.Console.WriteLine("error error");
        }
        
        // db updater thread
		private void db_runner()
		{
            database = new SqliteDatabase(database_dir);
            
            while (true) {
                IEnumerator<bool> item = null;

				bool item_from_db_work = false;
				
                lock (db_work) {
                    if (db_work.Count > 0) {
                        item = db_work.First.Value;
                        db_work.RemoveFirst();
						item_from_db_work = true;
                    }
				}

				if (item == null)  {

					if (database_work_count > 0) {
						database_work_count -= 1;
						System.Threading.Thread.Sleep(25); // ms
						continue;
					}
					
		            lock (background_db_work) {
    	                if (background_db_work.Count > 0) {
    	           	        item = background_db_work.First.Value;
    	           	        background_db_work.RemoveFirst();
    	           	    }
	                }
                }

				if (stop_db_runner)
					break;
                else if (item != null) {
					if (item.MoveNext()) {
						if (item_from_db_work)
							db_work.AddFirst(item);
						else
							background_db_work.AddFirst(item);
					}
				}
				
                bool work_surely_queued = false;
                lock (db_work) {
                    if (db_work.Count > 0)
                        work_surely_queued = true;
                }

				if (!work_surely_queued) {
                	lock (background_db_work) {
                    	if (background_db_work.Count > 0)
                    	    work_surely_queued = true;
                	}
                }

                if (!work_surely_queued)
                    work_ready.WaitOne();
            }

            database.close();
        }

        private bool is_watched(string path)
        {
            return path.StartsWith(watch_path);
        }

        private void db_handle_change(string path)
        {
            try {
				FileInfo f = new FileInfo(path);

                // first filter out strange stuff
                FileAttributes a = f.Attributes;
                if ((a & FileAttributes.Hidden) == FileAttributes.Hidden
                    || (a & FileAttributes.Directory) == FileAttributes.Directory
                    || (a & FileAttributes.Device) == FileAttributes.Device
                    || (a & FileAttributes.Temporary) == FileAttributes.Temporary
                    || (a & FileAttributes.ReparsePoint) == FileAttributes.ReparsePoint)
                    return;

                bool was_in_query = database.file_path_is_in_query(path, current_query);
				
                long filetype_id = db_determine_filetype_id(path);

                bool id_found = false;
                long id = database.get_file_id(path, out id_found);

				if (id_found) 
                {
					Helpers.RunInMainThread(delegate {
						Singleton<Indexing>.Instance.add_text(String.Format("file updated {0}\n", path));
					})();
                    // update database
                    database.update_file(id, path, f.LastAccessTimeUtc,
                                         f.LastWriteTimeUtc, f.Length, filetype_id);
                }
                else 
                {
					Helpers.RunInMainThread(delegate {
						Singleton<Indexing>.Instance.add_text(String.Format("file added {0}\n", path));
					})();
					database.add_file(path, f.LastAccessTimeUtc,
                                      f.LastWriteTimeUtc, f.Length, filetype_id);
					db_trigger_type_labels_changed();
		 		}

				bool is_in_query = database.file_path_is_in_query(path, current_query);
                if (is_in_query || was_in_query != is_in_query)
                    db_schedule_query_result_update();
            }
            catch (System.IO.FileNotFoundException) {
                // these are ok
            }
            catch {
                throw; // FIXME
            }
        }

        private void db_handle_create(string path)
        {
            db_handle_change(path);
        }

        private void db_handle_delete(string path)
        {
            try {
                bool was_in_query = database.file_path_is_in_query(path, current_query);

				Helpers.RunInMainThread(delegate {
					Singleton<Indexing>.Instance.add_text(String.Format("file deleted {0}\n", path));
				})();

                // update database
                database.delete_file(path);

	 			db_trigger_type_labels_changed();

                // we can take a shortcut here since it's delete
                bool is_in_query = false;
                
                if (is_in_query | was_in_query != is_in_query)
                    db_schedule_query_result_update();
            }
            catch {
                throw; // FIXME
            }
        }
        
        private void db_schedule_query_result_update()
        {
			Helpers.RunOnlyOnceFIFO(trigger_query_result_changed, 100);        
        }

        private void db_handle_rename(RenamedEventArgs e)
        {
            try {
                if (!is_watched(e.FullPath)) {
                    db_handle_delete(e.OldFullPath);
                    return;
                }
                
                bool was_in_query = database.file_path_is_in_query(e.OldFullPath, current_query);

                bool id_found = false;
                long id = database.get_file_id(e.OldFullPath, out id_found);
                if (id_found) {
					Helpers.RunInMainThread(delegate {
						Singleton<Indexing>.Instance.add_text(
							String.Format("file renamed from {0} to {1}\n", e.OldFullPath, e.FullPath));
					})();
                    database.rename_file(id, e.FullPath);
                    database.update_type_for_file(id, db_determine_filetype_id(e.FullPath));
                }
                else {
                    // we just learned of a new file
                    db_handle_change(e.FullPath);
                    return;
                }
                
                
                bool is_in_query = database.file_path_is_in_query(e.FullPath, current_query);
                if (is_in_query || was_in_query != is_in_query)
                    db_schedule_query_result_update();
            }
            catch {
                throw; // FIXME
            }
        }

        private void db_search(Query query, VoidFunction<List<File>> callback)
        {
			List<File> res = database.search(query);
			if (callback != null)
				callback(res);
        }

        private void db_add_label_to_file(File file, MetaLabel label, VoidFunction callback)
        {
            bool was_in_query = database.file_id_is_in_query(file.db_id, current_query);
            
            database.add_label_to_file_id(file.db_id, label.db_id);

            bool is_in_query = database.file_id_is_in_query(file.db_id, current_query);
            if (is_in_query || was_in_query != is_in_query)
                db_schedule_query_result_update();
            
            callback();
        }

        private void db_remove_label_from_file(File file, MetaLabel label, VoidFunction callback)
        {
            bool was_in_query = database.file_id_is_in_query(file.db_id, current_query);
            
            database.remove_label_from_file_id(file.db_id, label.db_id);

            bool is_in_query = database.file_id_is_in_query(file.db_id, current_query);
            if (is_in_query || was_in_query != is_in_query)
                db_schedule_query_result_update();
            
            callback();
        }
        
        public void db_add_label(MetaLabel label, VoidFunction callback)
        {
            database.add_label(label);
            db_trigger_labels_changed();
            callback();
        }
        
        public void db_rename_label(MetaLabel label, string new_name)
        {
            database.rename_label(label.db_id, new_name);
            db_trigger_labels_changed();
        }

        public void db_remove_label(MetaLabel label, VoidFunction callback)
        {
            // first get all the labels that are connected to this label
            List<long> labels = new List<long>();
            labels.Add(label.db_id);
     
            List<long> todo = new List<long>(labels);
            while (todo.Count > 0) {
                long l = todo[todo.Count - 1];
                todo.RemoveAt(todo.Count - 1);
                
                List<long> sublabels = database.get_sublabel_ids(l);
                foreach (long sub in sublabels) {
                    if (!labels.Exists(delegate(long o) { return sub == o; })) {
                        labels.Add(sub);
                        todo.Add(sub);
                    }
                }
            }

#if false
            // then find those of them that are in the query
            List<long> labels_in_query = new List<long>();
            foreach (long l in labels) {
                if (current_query.labels.Exists(delegate(Label o) { return o.db_id == l; }))
                    labels_in_query.Add(l);
            }
#endif

            // find the corresponding files
            List<long> files_for_labels = database.get_file_ids_for_labels(labels);

            bool[] was_in_query = new bool[files_for_labels.Count];
            for (int i = 0; i < files_for_labels.Count; ++i)
                was_in_query[i] = database.file_id_is_in_query(files_for_labels[i], current_query);

            // whack the labels
            foreach (long l in labels)
                database.delete_label(l);
            
            bool[] is_in_query = new bool[files_for_labels.Count];
            for (int i = 0; i < files_for_labels.Count; ++i)
                is_in_query[i] = database.file_id_is_in_query(files_for_labels[i], current_query);

                    
            for (int i = 0; i < files_for_labels.Count; ++i) {
                if (is_in_query[i] || was_in_query[i] != is_in_query[i])
                    db_schedule_query_result_update();
            }
            
            db_trigger_labels_changed();
            callback();
        }

		private void db_update_starred(File file, bool starred, VoidFunction callback)
		{
            bool was_in_query = database.file_id_is_in_query(file.db_id, current_query);
            
            database.updated_starred_state_on_file(file.db_id, starred);

            bool is_in_query = database.file_id_is_in_query(file.db_id, current_query);
            if (is_in_query || was_in_query != is_in_query)
                db_schedule_query_result_update();
            
            callback();
		}
		
        private void db_trigger_labels_changed()
        {
			if (labels_changed_callback != null)
				labels_changed_callback(database.get_labels());
        }

        private void db_trigger_type_labels_changed()
        {
			if (type_labels_changed_callback != null) {
				type_labels_changed_callback(database.get_used_file_type_categories());
			}
        }

		public static int Sqlite3Sort(string a, string b) 
		{
			int y, z;
				
			for (int i = 0; i < a.Length; ++i)
			{
			  
			  if (i >= b.Length)
			    return -1;

				if (a[i].Equals(b[i])) 
				  continue;

				bool w = Int32.TryParse(a.Substring(i, 1), out y), x = Int32.TryParse(b.Substring(i, 1), out z);
				bool bothNumbers = w && x, bothNotNumbers = !w && !x;

				if (bothNumbers) 
					return y.CompareTo(z);
				else if (bothNotNumbers) 
					return a[i].CompareTo(b[i]);
				else if (w) {
					if (b[i] == '.')
						return 1;
					else
						return -1;
				} else {
					if (a[i] == '.')
						return -1;
					else
						return 1;
				}
			}
				
			return 0;
		} 
		
		// returns how many was inserted
		private IEnumerable<int> db_sync_directory_with_filesystem(string directory_path)
        {
			if (directory_path.EndsWith("/"))
				directory_path = directory_path.Substring(0, directory_path.Length-1);
			
            // get filesystem files and database files
            string[] files = new string[0];
            try {
                files = Directory.GetFiles(directory_path);
            }
            catch {
            }
            
            List<string> fsfiles = new List<string>();

            foreach (string file in files)
            	if (filename_is_good(file))
            		fsfiles.Add(file);
            
            fsfiles.Sort(Sqlite3Sort);
            
			List<string> dbfiles = database.get_file_paths_in_directory(directory_path);

			// compare them
            List<string> update_files = new List<string>();
            List<string> delete_files = new List<string>();
            List<string> add_files = new List<string>();
                
            List<string>.Enumerator ifs = fsfiles.GetEnumerator();
            List<string>.Enumerator idb = dbfiles.GetEnumerator();
            bool morefs = ifs.MoveNext(), moredb = idb.MoveNext();

			while (morefs && moredb) {
                int val = ifs.Current.CompareTo(idb.Current);
                if (val < 0) {
                    add_files.Add(ifs.Current);
                    morefs = ifs.MoveNext();
                }
                else if (val > 0) {
                    delete_files.Add(idb.Current);
                    moredb = idb.MoveNext();
                }
                else {
                    update_files.Add(idb.Current);
                    morefs = ifs.MoveNext();
                    moredb = idb.MoveNext();
                }
            }

            while (morefs) {
                add_files.Add(ifs.Current);
                morefs = ifs.MoveNext();
            }

            while (moredb) {
                delete_files.Add(idb.Current);
                moredb = idb.MoveNext();
            }
			
			System.Console.WriteLine("checking path {0}: added {1}, deleted {2}, updated {3}", directory_path, add_files.Count, delete_files.Count, update_files.Count);			
			
			bool inserted = delete_files.Count > 0 || add_files.Count > 0 || update_files.Count > 0;

			int nr_changes = 0;
			
			foreach (string filename in add_files)
			{
				indexed_filenames.Add(filename);			
			}
			
			foreach (string filename in update_files)
			{
				indexed_filenames.Add(filename);			
			}

			// performance
			if (inserted)
				database.start_transaction();

            // and action!
            foreach (string file in delete_files) {
				if (++nr_changes == 100) {
					if (inserted)
						database.end_transaction();
					yield return nr_changes;
					if (inserted)
						database.start_transaction();
					nr_changes = 0;
				}
                db_handle_delete(file);
			}

			foreach (string file in add_files) {
				if (++nr_changes == 100) {
					if (inserted)
						database.end_transaction();
					yield return nr_changes;
					if (inserted)
						database.start_transaction();
					nr_changes = 0;
				}
				db_handle_create(file);
			}

			foreach (string file in update_files) {
                if (++nr_changes == 100) {
					if (inserted)
						database.end_transaction();
					yield return nr_changes;
					if (inserted)
						database.start_transaction();
					nr_changes = 0;
				}
				db_handle_change(file);
			}

			if (dir_sync_count > 0)
				dir_sync_count -= 1;

			if (inserted)
				database.end_transaction();
			
			yield return nr_changes;
        }

		static int dir_sync_count = 0;
		int database_work_count;
        
        private IEnumerable<bool> db_sync_with_filesystem(string directory_path)
        {
            // fix files in this dir
            foreach (int nr_changes in db_sync_directory_with_filesystem(directory_path)) {
				database_work_count = Math.Max(nr_changes/wait_cycles_scaling_factor, min_number_of_wait_cycles);
				yield return true;
			}

            // then recurse
			try {
				string[] subdirs = Directory.GetDirectories(directory_path);
	            foreach (string subdir in subdirs) {

	            	if (!directory_is_good(subdir))
						continue;

	            	string tmp_subdir = subdir;

		            if ((System.IO.File.GetAttributes(subdir) & FileAttributes.ReparsePoint) != FileAttributes.ReparsePoint) {
						add_low_priority_db_work(TurnEnumerableIntoEnumerator(db_sync_with_filesystem(tmp_subdir)));         	
						dir_sync_count += 1;
					}
				}
			} catch (System.IO.DirectoryNotFoundException) {
				// swallow this error
			}
			
			// finished indexing
			if (dir_sync_count == 0) {

				database.clean_database_of_nonexisting_files(indexed_filenames);
				indexed_filenames.Clear();
				
				Helpers.RunInMainThread(delegate {
					Singleton<Indexing>.Instance.change_status(false);
				})();
			}

			yield break;
        }

        private long db_determine_filetype_id(string path)
        {
            string extension = "";
            int dot = path.LastIndexOf('.');
            if (dot != -1)
                extension = path.Substring(dot + 1).ToLower();

            long db_id = database.get_file_type_id(extension);
            return db_id;
        }
    }


    public class SqliteDatabase 
    {
        IDbConnection db_connection;

		public string database_filename;
		
        public SqliteDatabase(string database_dir)
        {
			database_filename = database_dir + "nemo.db";

			string str = "Data Source=" + database_filename;

            db_connection = new SqliteConnection(str);

			// mono is buggy so we need this evil hack
			System.Threading.Thread.Sleep(250);

            db_connection.Open();

            using (IDbCommand cmd = get_command()) {
                // find out which version this is
                int version = 0;

                try {
                    cmd.CommandText = "select * from version;";
                    using (IDataReader reader = cmd.ExecuteReader()) {
                        while (reader.Read()) {
                            version = reader.GetInt32(0);
                        }
                    }
                }
                catch (Exception) {
                }

                if (version == 0) {
                    create_db_tables(cmd);
                    fill_in_default_file_types(cmd);
                }
                else if (version == 1) {
                    update_v1_to_v2(cmd);
                }
            }
        }

        public void close()
        {
            db_connection.Close();
        }

		IDbTransaction current_transaction;
		
		public void start_transaction()
		{
			if (current_transaction != null)
				throw new Exception("double transaction start in metadata store");
		
			current_transaction = db_connection.BeginTransaction();
		}

		public void end_transaction()
		{
			if (current_transaction != null) {
				current_transaction.Commit();
				current_transaction = null;
			}
		}		

        public IDbCommand get_command()
        {
            return db_connection.CreateCommand();
        }

		private void update_v1_to_v2(IDbCommand cmd)
		{
			System.Console.WriteLine("Updating database from version 1 to version 2");
			cmd.CommandText += "update version set version = 2;";
            cmd.CommandText += "create index file_types_category on file_types(category);";
            cmd.ExecuteNonQuery();
		}
		
        private void create_db_tables(IDbCommand cmd)
        {
            // nothing is in the DB so create all the tables
            cmd.CommandText = "create table version(version integer);";
            cmd.CommandText += "insert into version values (2);";
                    
            cmd.CommandText += "create table files ("
                + "id integer primary key, "
                + "path text, "
                + "last_accessed datetime, "
                + "last_modified datetime, "
                + "size integer, "
                + "type integer references file_types(id),"
                + "starred integer "
                + ");";
            cmd.CommandText += "create index file_path_index on files(path);";
            cmd.CommandText += "create index file_last_accessed_index on files(last_accessed);";
            cmd.CommandText += "create index file_last_modified_index on files(last_modified);";
            cmd.CommandText += "create index file_type_index on files(type);";
            cmd.CommandText += "create table labels ("
                + "id integer primary key, "
                + "label text,"
                + "color integer references label_colors,"
                + "parent integer references labels(id),"
                + "initial integer"
                + ");";
            cmd.CommandText += "create index labels_label_index on labels(label);";
            cmd.CommandText += "create index labels_parent_index on labels(parent);";
            
            cmd.CommandText += String.Format("insert into labels values (0, \"{0}\", 1, -1, 1);", Mono.Unix.Catalog.GetString("All"));

            cmd.CommandText += "create table label_colors ("
                + "id integer primary key, "
                + "r integer,"
                + "g integer,"
                + "b integer,"
                + "a integer"
                + ");";

            cmd.CommandText += "insert into label_colors (r, g, b, a) values (244, 124, 32, 0);";
            cmd.CommandText += "insert into label_colors (r, g, b, a) values (50, 166, 228, 0);";
            cmd.CommandText += "insert into label_colors (r, g, b, a) values (152, 97, 194, 0);";
            cmd.CommandText += "insert into label_colors (r, g, b, a) values (107, 157, 65, 0);";
            cmd.CommandText += "insert into label_colors (r, g, b, a) values (255, 196, 12, 0);";
            cmd.CommandText += "insert into label_colors (r, g, b, a) values (0, 95, 164, 0);";
            cmd.CommandText += "insert into label_colors (r, g, b, a) values (240, 110, 169, 0);";
            cmd.CommandText += "insert into label_colors (r, g, b, a) values (210, 4, 98, 0);";
            cmd.CommandText += "insert into label_colors (r, g, b, a) values (180, 173, 53, 0);";

            cmd.CommandText += "create table label_colors_index (id integer);";
            cmd.CommandText += "insert into label_colors_index values (2);";

            cmd.CommandText += "create table file_types ("
                + "id integer primary key, "
                + "type text, "
                + "category integer"
                + ");";
            cmd.CommandText += "create index file_types_category on file_types(category);";
            cmd.CommandText += "create index file_types_type_index on file_types(type);";
            cmd.CommandText += String.Format("insert into file_types (id, type, category) values (1, \"\", {0});", (int) FileTypeCategory.None);
            cmd.CommandText += "create table file_labels ("
                + "file_id integer references files(id), "
                + "label_id integer references labels(id)"
                + ");";
            cmd.CommandText += "create index file_labels_file_id_index on file_labels(file_id);";
            cmd.CommandText += "create index file_labels_label_id_index on file_labels(label_id);";
            cmd.ExecuteNonQuery();
        }

		// helper functions

        private void fill_in_default_file_types(IDbCommand cmd)
        {
            add_file_type(cmd, "txt", FileTypeCategory.Document);
            add_file_type(cmd, "doc", FileTypeCategory.Document);
            add_file_type(cmd, "docx", FileTypeCategory.Document);
            add_file_type(cmd, "odt", FileTypeCategory.Document);

            add_file_type(cmd, "pdf", FileTypeCategory.Pdf);

            add_file_type(cmd, "png", FileTypeCategory.Image);
            add_file_type(cmd, "jpg", FileTypeCategory.Image);
            add_file_type(cmd, "gif", FileTypeCategory.Image);
            add_file_type(cmd, "tiff", FileTypeCategory.Image);
            add_file_type(cmd, "xfc", FileTypeCategory.Image);

            add_file_type(cmd, "ppt", FileTypeCategory.Presentation);

            add_file_type(cmd, "xls", FileTypeCategory.Spreadsheet);
            add_file_type(cmd, "xlsx", FileTypeCategory.Spreadsheet);
            add_file_type(cmd, "ods", FileTypeCategory.Spreadsheet);

            add_file_type(cmd, "avi", FileTypeCategory.Video);
            add_file_type(cmd, "mpg", FileTypeCategory.Video);
            add_file_type(cmd, "mkv", FileTypeCategory.Video);

            add_file_type(cmd, "bz2", FileTypeCategory.Archive);
            add_file_type(cmd, "gz", FileTypeCategory.Archive);
            add_file_type(cmd, "zip", FileTypeCategory.Archive);
        }
        
        private void add_par(IDbCommand cmd, string name, DbType type, object value)
        {
            SqliteParameter p = cmd.CreateParameter() as SqliteParameter;
            p.ParameterName = name;
            p.DbType = type;
            p.Value = value;
            cmd.Parameters.Add(p);
        }

        private void add_file_type(IDbCommand cmd, string type, FileTypeCategory category)
        {
	    try {
		cmd.CommandText = "insert into file_types (type, category) " +
		    "values (@type, @category);";
		cmd.Parameters.Clear();
		add_par(cmd, "@type", DbType.String, type);
		add_par(cmd, "@category", DbType.Int32, (int)category);
		cmd.ExecuteNonQuery();
	    } catch (Exception e) {
		System.Console.WriteLine("Caught exception while adding file type: {0} - {1}", e.Message, cmd.CommandText);
	    }
        }
        
        public long get_file_id(string path, out bool found)
        {
             using (IDbCommand cmd = get_command()) {
                 cmd.CommandText = "select id from files where path = @path";
                 cmd.Parameters.Clear();
                 add_par(cmd, "@path", DbType.String, path);
                 using (IDataReader reader = cmd.ExecuteReader()) {
                     if (reader.Read()) {
                         found = true;
                         return reader.GetInt64(0);
                     }
				 }
			}

            found = false;
            return 0;

#if false
            found = false;
            long ret = 0;

            using (IDbCommand cmd = get_command()) {
                cmd.CommandText = "select id from files where path = @path";
                cmd.Parameters.Clear();
                add_par(cmd, "@path", DbType.String, path);
                IDataReader reader = cmd.ExecuteReader();
                if (reader.Read()) {
                   found = true;
                   ret = reader.GetInt64(0);
                }
                reader.Close();
            }
            
            return ret;
#endif
        }

        public void add_file(string path, DateTime last_accessed,
                             DateTime last_modified, long size, long filetype_id)
        {
//            System.Console.WriteLine("inserting {0}", path);
	    
	    using (IDbCommand cmd = get_command()) {
		try {
		    cmd.CommandText = "insert into files (path, last_accessed, last_modified, size, type, starred) " +
			"values (@path, @last_accessed, @last_modified, @size, @filetype_id, 0)";
                    
		    cmd.Parameters.Clear();
		    add_par(cmd, "@path", DbType.String, path);
		    add_par(cmd, "@last_accessed", DbType.Int64, last_accessed.ToFileTime());
		    add_par(cmd, "@last_modified", DbType.Int64, last_modified.ToFileTime());
		    add_par(cmd, "@size", DbType.Int64, size);
		    add_par(cmd, "@filetype_id", DbType.Int64, filetype_id);
		    cmd.ExecuteNonQuery();
		} catch (Exception e) {
		    System.Console.WriteLine("Caught exception while adding file: {0} - {1}", e.Message, cmd.CommandText);
		}
	    }
	}

		public void updated_starred_state_on_file(long id, bool starred)
		{
		    using (IDbCommand cmd = get_command()) {
			try {
			    cmd.CommandText = "update files set " +
				"starred = @starred " +
				"where id = @id";
			    cmd.Parameters.Clear();
			    add_par(cmd, "@id", DbType.Int64, id);
			    add_par(cmd, "@starred", DbType.Int32, starred);
			    cmd.ExecuteNonQuery();
			} catch (Exception e) {
			    System.Console.WriteLine("Caught exception while updating star state on file: {0} - {1}", e.Message, cmd.CommandText);
			}
		    }
		}
		
        public void update_file(long id, string path, DateTime last_accessed,
                                DateTime last_modified, long size, long filetype_id)
        {
	    using (IDbCommand cmd = get_command()) {
		try {
		    cmd.CommandText = "update files set " +
			"last_accessed = @last_accessed, " +
			"last_modified = @last_modified, " +
			"size = @size, " +
			"type = @filetype_id " +
			"where id = @id";
		    cmd.Parameters.Clear();
		    add_par(cmd, "@id", DbType.Int64, id);
		    add_par(cmd, "@last_accessed", DbType.Int64, last_accessed.ToFileTime());
		    add_par(cmd, "@last_modified", DbType.Int64, last_modified.ToFileTime());
		    add_par(cmd, "@size", DbType.Int64, size);
		    add_par(cmd, "@filetype_id", DbType.Int64, filetype_id);
		    cmd.ExecuteNonQuery();
		} catch (Exception e) {
		    System.Console.WriteLine("Caught exception while updating file: {0} - {1}", e.Message, cmd.CommandText);
		}
	    }
        }

        public void update_type_for_file(long file_id, long filetype_id)
        {
	    using (IDbCommand cmd = get_command()) {
		try {
		    cmd.CommandText = "update files set " +
			"type = @filetype_id " +
			"where id = @file_id";
		    cmd.Parameters.Clear();
		    add_par(cmd, "@file_id", DbType.Int64, file_id);
		    add_par(cmd, "@filetype_id", DbType.Int64, filetype_id);
		    cmd.ExecuteNonQuery();
		} catch (Exception e) {
		    System.Console.WriteLine("Caught exception while updating type on file: {0} - {1}", e.Message, cmd.CommandText);
		} 	    
	    }
        }
        
        public void delete_file(string path)
        {
	    using (IDbCommand cmd = get_command()) {
		try {
		    bool id_found = false;
		    long id = get_file_id(path, out id_found);

		    if (id_found) {
			System.Console.WriteLine("deleting labels for {0}", path);
			cmd.CommandText = "delete from file_labels where file_id = @id";
			cmd.Parameters.Clear();
			add_par(cmd, "@id", DbType.Int64, id);
			cmd.ExecuteNonQuery();

			System.Console.WriteLine("deleting {0}", path);
			cmd.CommandText = "delete from files where id = @id";
			cmd.Parameters.Clear();
			add_par(cmd, "@id", DbType.Int64, id);
			cmd.ExecuteNonQuery();
		    }
		} catch (Exception e) {
		    System.Console.WriteLine("Caught exception while deleting file: {0} - {1}", e.Message, cmd.CommandText);
		}
	    }
        }

        public void rename_file(long file_id, string path)
        {
	    using (IDbCommand cmd = get_command()) {
		try {
		    cmd.CommandText = "update files set path = @path where id = @file_id";
		    cmd.Parameters.Clear();
		    add_par(cmd, "@path", DbType.String, path);
		    add_par(cmd, "@file_id", DbType.Int64, file_id);
		    System.Console.WriteLine("renaming {0} to {1}", file_id, path);
		} catch (Exception e) {
		    System.Console.WriteLine("Caught exception while renaming file: {0} - {1}", e.Message, cmd.CommandText);
		}
	    }
        }

        public List<string> get_file_paths_in_directory(string directory_path)
        {
	    Debug.Assert(!directory_path.EndsWith("/"));
			
            using (IDbCommand cmd = get_command()) {
				cmd.CommandText = String.Format("select path from files where path like \"{0}{1}%\" and path not like \"{0}{1}%{1}%\" order by path", directory_path, Path.DirectorySeparatorChar);

                List<string> res = new List<string>();
                using (IDataReader reader = cmd.ExecuteReader()) {
	                while (reader.Read())
    	                res.Add(reader.GetString(0));
    	        }

#if false
                Console.WriteLine("=====> {0}", cmd.CommandText);
                foreach (string s in res)
                    Console.WriteLine("{0} in {1}", s, directory_path);
                Console.WriteLine("=====<");
#endif
                return res;
            }
        }
        
        private MetaLabel get_meta_label_from_db(IDataReader reader)
        {
            MetaLabel l = new MetaLabel();
            l.db_id = reader.GetInt64(0);
            l.label = reader.GetString(1);
            l.color.r = reader.GetByte(2);
            l.color.g = reader.GetByte(3);
            l.color.b = reader.GetByte(4);
            l.color.a = reader.GetByte(5);
            l.parent_db_id = reader.GetInt64(6);
			if (!reader.IsDBNull(7))
                l.initial = reader.GetBoolean(7);
            else
            	l.initial = false;

			return l;
        }
        
        public List<MetaLabel> get_labels_for_file_id(long file_id)
        {
            using (IDbCommand cmd = get_command()) {
                cmd.CommandText = String.Format("select labels.id, label, r, g, b, a, parent, initial from labels, label_colors, file_labels " +
                                                "where file_labels.file_id = {0} and file_labels.label_id = labels.id " + 
                                                "and label_colors.id = labels.color", file_id);

                List<MetaLabel> res = new List<MetaLabel>();

                using (IDataReader reader = cmd.ExecuteReader())
	                while (reader.Read())
    	                res.Add(get_meta_label_from_db(reader));

                res.Sort();

                return res;
            }
        }

#if false
        public List<MetaLabel> get_labels_for_path(string path)
        {
            using (IDbCommand cmd = get_command()) {
                bool id_found = false;
                long id = get_file_id(path, out id_found);

                if (id_found)
                    return get_labels_for_file_id(id);
                else
                    return new List<MetaLabel>();
            }
        }
#endif

        public void add_label(MetaLabel label)
        {
			Int32 index = 0;

            using (IDbCommand cmd = get_command()) {
                cmd.CommandText = "select id from label_colors_index";
                using (IDataReader reader = cmd.ExecuteReader()) {
                    if (reader.Read()) {
                        index = reader.GetInt32(0);
                    }
                }
            }
        
	    using (IDbCommand cmd = get_command()) {
		try {
		    cmd.CommandText = "insert into labels (label, color, parent) " +
			"values (@label, @color, @parent)";

		    cmd.Parameters.Clear();
		    add_par(cmd, "@label", DbType.String, label.label);
		    add_par(cmd, "@color", DbType.Int32, index);
		    add_par(cmd, "@parent", DbType.Int64, label.parent_db_id);
		    cmd.ExecuteNonQuery();
		} catch (Exception e) {
		    System.Console.WriteLine("Caught exception while adding label 1: {0} - {1}", e.Message, cmd.CommandText);
		}
	    }
            
            Int32 number_of_colors = 1;
            
            using (IDbCommand cmd = get_command()) {
                cmd.CommandText = "select count(*) from label_colors";
                using (IDataReader reader = cmd.ExecuteReader()) {
                    if (reader.Read()) {
                        number_of_colors = reader.GetInt32(0);
                    }
                }
            }

	    using (IDbCommand cmd = get_command()) {
		try {
		    cmd.CommandText = "update label_colors_index set id = @newid";
		    
		    cmd.Parameters.Clear();
		    add_par(cmd, "@newid", DbType.Int32, index % number_of_colors + 1);
		    cmd.ExecuteNonQuery();
		} catch (Exception e) {
		    System.Console.WriteLine("Caught exception while adding label 2: {0} - {1}", e.Message, cmd.CommandText);
		}
	    }
        }

        public void rename_label(long label_id, string new_name)
        {
	    using (IDbCommand cmd = get_command()) {
		try {
		    cmd.CommandText = "update labels set label = @label where id = @id";
		    cmd.Parameters.Clear();
		    add_par(cmd, "@label", DbType.String, new_name);
		    add_par(cmd, "@id", DbType.Int64, label_id);
		    cmd.ExecuteNonQuery();
		    System.Console.WriteLine("renaming {0} to {1}", label_id, new_name);
		} catch (Exception e) {
		    System.Console.WriteLine("Caught exception while renaming label: {0} - {1}", e.Message, cmd.CommandText);
		}
	    }
        }
        
        public List<MetaLabel> get_labels()
        {
            using (IDbCommand cmd = get_command()) {
                cmd.CommandText = "select labels.id, label, r, g, b, a, parent, initial " +
                    "from labels, label_colors where label_colors.id = labels.color " +
                    "order by parent asc, label";

                List<MetaLabel> res = new List<MetaLabel>();

                using (IDataReader reader = cmd.ExecuteReader())
	                while (reader.Read())
						res.Add(get_meta_label_from_db(reader));

                return res;
            }
        }

        public List<long> get_sublabel_ids(long label_id)
        {
            using (IDbCommand cmd = get_command()) {
                cmd.CommandText = String.Format("select id from labels where parent = {0}", label_id);

                List<long> res = new List<long>();
				using (IDataReader reader = cmd.ExecuteReader())
	                while (reader.Read())
    	                res.Add(reader.GetInt64(0));
                return res;
            }
        }

        public List<long> get_file_ids_for_labels(List<long> label_ids)
        {
            List<long> res = new List<long>();
            
            if (label_ids.Count == 0)
                return res;
            
            using (IDbCommand cmd = get_command()) {
                // FIXME: we should check here whether we have too many
                // label_ids, and split query if so
                List<string> clauses = new List<string>();
                int i = 0;
                foreach (long id in label_ids) {
                    clauses.Add(String.Format("label_id = @label_id{0}", i));
                    add_par(cmd, String.Format("@label_id{0}", i), DbType.Int64, id);
                    ++i;
                }

                cmd.CommandText = 
                    "select distinct file_id from file_labels where " + String.Join(" or ", clauses.ToArray()) + " ";

                using (IDataReader reader = cmd.ExecuteReader())
	                while (reader.Read())
    	                res.Add(reader.GetInt64(0));
                return res;
            }
        }
        
        private bool file_exists(long file_id)
        {
            using (IDbCommand cmd = get_command()) {
                cmd.CommandText = String.Format("select id from files where id = {0}", file_id);
                using (IDataReader reader = cmd.ExecuteReader())
                    if (reader.Read())
                        return true;
            }
            
            return false;
        }

        private bool label_exists(long label_id)
        {
            using (IDbCommand cmd = get_command()) {
                cmd.CommandText = String.Format("select id from labels where id = {0}", label_id);
                using (IDataReader reader = cmd.ExecuteReader())
                    if (reader.Read())
                        return true;
            }
            
            return false;
        }

        public void add_label_to_file_id(long file_id, long label_id)
        {
            if (!file_exists(file_id) || !label_exists(label_id))
                return;

	    using (IDbCommand cmd = get_command()) {
		try {
		    cmd.CommandText = "insert into file_labels (file_id, label_id) " +
			"values (@file_id, @label_id)";

		    cmd.Parameters.Clear();
		    add_par(cmd, "@file_id", DbType.Int64, file_id);
		    add_par(cmd, "@label_id", DbType.Int64, label_id);
		    cmd.ExecuteNonQuery();
		} catch (Exception e) {
		    System.Console.WriteLine("Caught exception while adding label to file id: {0} - {1}", e.Message, cmd.CommandText);
		}
	    }
        }
        
        public void remove_label_from_file_id(long file_id, long label_id)
        {
	    using (IDbCommand cmd = get_command()) {
		try {
		    cmd.CommandText = "delete from file_labels " +
			"where file_id = @file_id " +
			"and label_id = @label_id";

		    cmd.Parameters.Clear();
		    add_par(cmd, "@file_id", DbType.Int64, file_id);
		    add_par(cmd, "@label_id", DbType.Int64, label_id);
		    cmd.ExecuteNonQuery();
		} catch (Exception e) {
		    System.Console.WriteLine("Caught exception while removing label from file id: {0} - {1}", e.Message, cmd.CommandText);
		}
	    }
        }

        public void delete_label(long label_id)
        {
	    using (IDbCommand cmd = get_command()) {
		try {
		    cmd.CommandText = "delete from file_labels " +
			"where label_id = @label_id";

		    cmd.Parameters.Clear();
		    add_par(cmd, "@label_id", DbType.Int64, label_id);
		    cmd.ExecuteNonQuery();
		
		    cmd.CommandText = "delete from labels " +
			"where id = @label_id";

		    cmd.Parameters.Clear();
		    add_par(cmd, "@label_id", DbType.Int64, label_id);
		    cmd.ExecuteNonQuery();
		} catch (Exception e) {
		    System.Console.WriteLine("Caught exception while deleting label: {0} - {1}", e.Message, cmd.CommandText);
		}
	    }
        }
        
        private void construct_query_sql(IDbCommand cmd, Query query, string select, string extraclause)
        {
            cmd.Parameters.Clear();
                
            List<string> froms = new List<string>();
            List<string> clauses = new List<string>();
                
            if (extraclause.Length > 0)
                clauses.Add(extraclause);

            if (query.labels != null && query.labels.Count > 0) {
                
                bool first = true;

                froms.Add("file_labels");
                clauses.Add("file_labels.file_id = files.id");
                
                StringBuilder sub_clause = new StringBuilder("file_labels.label_id in (");

                int i = 0;
                foreach (MetaLabel label in query.labels) {
                
                    add_par(cmd, String.Format("@label_id{0}", i), DbType.Int64, label.db_id);

                    if (!first)
                    	sub_clause.Append(String.Format(", @label_id{0}", i));
                    else {
                    	first = false;
                    	sub_clause.Append(String.Format("@label_id{0}", i));
                    }

                    ++i;
                }

   				sub_clause.Append(")");

                clauses.Add(sub_clause.ToString());
            }

            if (query.types != null) {
                int i = 0;
                
                bool first = true;

                froms.Add("file_types");
                clauses.Add("file_types.id = files.type");
                
                StringBuilder sub_clause = new StringBuilder("file_types.category in (");

                foreach (long id in query.types) {
                
                    add_par(cmd, String.Format("@type_id{0}", i), DbType.Int64, id);

                    if (!first)
                    	sub_clause.Append(String.Format(", @type_id{0}", i));
                    else {
                    	first = false;
                    	sub_clause.Append(String.Format("@type_id{0}", i));
                    }

                    ++i;
                }

				sub_clause.Append(")");

                clauses.Add(sub_clause.ToString());
            }

            clauses.Add("files.last_modified >= @date_start");
            add_par(cmd, "@date_start", DbType.Int64, query.date_start.ToFileTime());
            clauses.Add("files.last_modified <= @date_end");
            add_par(cmd, "@date_end", DbType.Int64, query.date_end.ToFileTime());
                
            string extra_tables = String.Empty;
            if (froms.Count > 0)
                extra_tables = ", " + String.Join(", ", froms.ToArray()) + " ";

            string where = String.Empty;
            if (clauses.Count > 0)
                where = "where " + String.Join(" and ", clauses.ToArray()) + " ";

			if (query.starred)
				where += " and files.starred = 1";
                
            cmd.CommandText = "select " + select + " from files " +
                extra_tables + where;
        }
        
        private File get_file_from_db(IDataReader reader)
        {
            File f = new File();
            f.db_id = reader.GetInt64(0);
            f.path = reader.GetString(1);
            f.last_accessed = System.DateTime.FromFileTime(reader.GetInt64(2));
            f.last_modified = System.DateTime.FromFileTime(reader.GetInt64(3));
            f.bytesize = reader.GetInt64(4);
            f.type = get_file_type_id(reader.GetInt64(5));
            f.starred = reader.GetBoolean(6);
            f.labels = get_labels_for_file_id(f.db_id);
			return f;        
        }
        
        public List<File> search(Query query)
        {
            List<File> res = new List<File>();
            
            if (query == null)
                return res;
            
            using (IDbCommand cmd = get_command()) {
                construct_query_sql(cmd, query,
                                    "files.id, path, last_accessed, last_modified, size, files.type, starred", "");
                
                using (IDataReader reader = cmd.ExecuteReader())
	                while (reader.Read())
    	            	res.Add(get_file_from_db(reader));
            }

            return res;
        }
        
        public bool file_id_is_in_query(long file_id, Query query)
        {
            if (query == null)
                return false;
            
            using (IDbCommand cmd = get_command()) {
                construct_query_sql(cmd, query, "files.id", "files.id = @Xfile_id");
                add_par(cmd, "@Xfile_id", DbType.Int64, file_id);
                using (IDataReader reader = cmd.ExecuteReader())
                    if (reader.Read())
                        return true;
            }
                
            return false;
        }
            
        public bool file_path_is_in_query(string path, Query query)
        {
            if (query == null)
                return false;
            
            using (IDbCommand cmd = get_command()) {
                construct_query_sql(cmd, query, "files.id",  "files.path = @Xpath");
                add_par(cmd, "@Xpath", DbType.String, path);
                using (IDataReader reader = cmd.ExecuteReader())
                    if (reader.Read())
                        return true;
            }
                
            return false;
        }

        public FileType get_file_type_id(long type_id)
        {
        	FileType temp = new FileType();
        	temp.db_id = type_id;
        
            using (IDbCommand cmd = get_command()) {
                cmd.CommandText = "select type, category from file_types where id = @type_id";
                add_par(cmd, "@type_id", DbType.Int64, type_id);
                using (IDataReader reader = cmd.ExecuteReader()) {
                    if (reader.Read()) {
                        temp.type = reader.GetString(0);
                        temp.category = (FileTypeCategory)reader.GetInt32(1);
                    }
                }
            }

			return temp;
        }

        public long get_file_type_id(string type)
        {
            using (IDbCommand cmd = get_command()) {
                cmd.CommandText = "select id from file_types where type = @type";
                add_par(cmd, "@type", DbType.String, type);
                using (IDataReader reader = cmd.ExecuteReader())
                    if (reader.Read())
                        return reader.GetInt64(0);
            }

            // default to the special unknown type
            return 1;
        }

        public List<Tuple<FileTypeCategory, int>> get_used_file_type_categories()
        {
            using (IDbCommand cmd = get_command()) {
                cmd.CommandText = String.Format("select category, count(*) " +
                  "from file_types, files where file_types.id = files.type " +
                  "group by category order by category");

                List<Tuple<FileTypeCategory, int>> res = new List<Tuple<FileTypeCategory, int>>();
                using (IDataReader reader = cmd.ExecuteReader()) {
	                while (reader.Read()) {
    	                Tuple<FileTypeCategory, int> t;
    	                t.first = (FileTypeCategory)reader.GetInt32(0);
    	                t.second = reader.GetInt32(1);
    	                res.Add(t);
    	            }
    	        }
                return res;
            }
        }

		// for sync'ing between search tool and metadata database
		public File get_file_from_path(string path)
		{
            using (IDbCommand cmd = get_command()) {
                cmd.CommandText = String.Format("select * from " +
                  "files where path = '" + path + "'");

				File f = null;

                using (IDataReader reader = cmd.ExecuteReader())
	                while (reader.Read()) 
    	            	f = get_file_from_db(reader);

                return f;
            }
		}

		public List<File> get_files_from_paths(List<string> paths)
		{
            using (IDbCommand cmd = get_command()) {
                cmd.CommandText = String.Format("select * from " +
                  "files where path in ('" + String.Join("', '", paths.ToArray()) + "')");

				List<File> files = new List<File>();

                using (IDataReader reader = cmd.ExecuteReader())
	                while (reader.Read()) 
    	                files.Add(get_file_from_db(reader));

                return files;
            }
		}

		public void clean_database_of_nonexisting_files(List<string> paths)
		{
		    if (paths.Count > 0) {
			using (IDbCommand cmd = get_command()) {
			    try {
				cmd.CommandText = "delete from files where path not in ('" + String.Join("', '", paths.ToArray()) + "')";
				cmd.ExecuteNonQuery();
			    } catch (Exception e) {
				System.Console.WriteLine("Caught exception while cleaning nonexisting files: {0} - {1}", e.Message, cmd.CommandText);
			    }
			}
		    }
		}
	}
}
