using System;
using System.Collections.Generic;

namespace Nemo
{
	public sealed class FileLayout
	{
		public class Element
		{
			public ItemWrapper wrapper;
			public double pos;

			public Element(double pos, ItemWrapper wrapper)
			{
				this.wrapper = wrapper;
				this.pos = pos;
			}
		}
	
		public class Group
		{
		    private double sum;
		    public List<Element> items;
			public int slots;

			public Group(Element item)
			{
		        items = new List<Element>();
			
		        sum = item.pos;
		        items.Add(item);
		        slots = 0;
			}
	    
		    public double value()
		    {
	        	return sum / items.Count;
			}

		    public double dist(double item)
			{
		        return System.Math.Abs(value() - item);
			}
			
		    public void add(Element item)
			{
				sum += item.pos;
		        items.Add(item);
		    }

			public bool add_slot(int max_slots_per_group)
			{
		        if (slots >= max_slots_per_group || slots >= items.Count)
	    	        return false;
	    	    else {
	    	        slots += 1;
	    	        return true;
	    	    }
	    	}

	    	public void merge(Group other, int max_slots_per_group)
	    	{
	    		items.AddRange(other.items);
	        	sum += other.sum;
	        	slots += other.slots;
	        	if (slots > max_slots_per_group)
	            	slots = max_slots_per_group;
			}

		    public void position(int box_height, int line_height, out int beginning, out int ending)
		    {
		    	int no_rows = slots;
		        if (has_more())
		            no_rows += 1;

		        double tmp = value() * box_height;
		        int height = no_rows * line_height;

		        double begin = tmp - height * 0.5;
		        double end = tmp + height * 0.5;

		        double delta = 0;
		        if (begin < 0)
		            delta = 0 - begin;
		        else if (end > box_height)
		            delta = box_height - end;

		        begin += delta;
		        end += delta ;

				beginning = (int)System.Math.Round(begin); ending = (int)System.Math.Round(end);
			}

			public bool need_spacing()
			{
	        	return has_more();
			}

	    	public bool has_more()
			{
	        	return items.Count > slots;
			}
			
			// special functions to test if a more: 1 would be used
			public bool has_only_one_more(int c)
			{
				return (c == items.Count -1);
			}
		}

		public List<Group> sequential_leader_cluster(List<Element> items, int no_groups)
	    {
		    // put in first item
	    	List<Group> groups = new List<Group>();
	    	groups.Add(new Group(items[0]));
	    	double threshold = (1.0/no_groups)/2;
	    
	    	foreach(Element item in items.GetRange(1, items.Count-1)) 
	    	{
	        	// find best fit for item, easy since groups are sorted
	        	Group closest_group = groups[groups.Count-1];
	        	double dist = closest_group.dist(item.pos);

	        	// is this fit acceptable?
	        	if (dist <= threshold)
	            	closest_group.add(item);
	        	else
	            	groups.Add(new Group(item));
	        }
	        
	    	return groups;
		}

		public void distribute_slots(List<Group> groups, int slots, int max_slots_per_group)
		{
		    foreach(Group group in groups)
		    {
		        // everyone should have one
		        group.slots = 1;
		        slots -= 1;
			}

			// distribute remaining slots by popularity
		    bool slot_alotted_in_round = true;
		    while (slots > 0 && slot_alotted_in_round) 
		    {
		        // FIXME: should find group with most popular item and give it a slot

		        // FIXME: here we just distribute in round robin fashion
				slot_alotted_in_round = false;
		        foreach (Group group in groups)
		        {
		        	bool slot_alotted = group.add_slot(max_slots_per_group);

					if (!slot_alotted_in_round)
						slot_alotted_in_round = slot_alotted;
		            	
		            if (slot_alotted) {
		                slots -= 1;
		                if (slots == 0)
		                    break;
		            }
		         }
		    }
		}
		
		public void merge_overlapping(List<Group> groups, int box_height, int line_height, int group_spacing, int max_slots_per_group,
									  double max_total_item_deviation)
    	{
		    bool overlap = true;
		    while (overlap)
		    {
		        overlap = false;
		        int prev_pos_begin = 0, prev_pos_end = 0;
		        groups[0].position(box_height, line_height, out prev_pos_begin, out prev_pos_end);
		        bool prev_need_spacing = groups[0].need_spacing();
		        for (int i = 1; i < groups.Count; ++i)
				{
		            int pos_begin = 0, pos_end = 0;
		            groups[i].position(box_height, line_height, out pos_begin, out pos_end);
		            bool need_spacing = groups[i].need_spacing();
		            int spacing = 0;
		            if (prev_need_spacing || need_spacing)
		                spacing = group_spacing;

		            if (prev_pos_end + spacing > pos_begin || (prev_pos_end - prev_pos_end) + max_total_item_deviation*box_height > pos_begin)
		            {
		                groups[i - 1].merge(groups[i], max_slots_per_group);
		                groups.RemoveAt(i);
		                overlap = true;
		                break;
					}
					
		            prev_pos_begin = pos_begin; prev_pos_end = pos_end;
		            prev_need_spacing = need_spacing;
		        }
			}
		}

		public List<Group> layout(List<Element> items, int box_height, int line_height, int group_spacing, double max_item_deviation)
		{
			if (max_item_deviation > 0.5)
				throw new Exception("max item deviation too large!");
		
			List<Group> groups = new List<Group>();
		    if (items.Count == 0)
		        return groups;

		    // derive max slots per group from the permitted entry deviation -
		    // worst case is when an item is placed first or last, i.e. as far
		    // as possible away from the center of a group
		    //
		    // the 1 is for the more button, to prevent the +1 more, a group 
		    // has a has_one_more function that can be used for that purpose
		    int max_slots_per_group = (int)(max_item_deviation * 2 / (line_height / (double)box_height))-1; 
		    
		    // first produce a manageable no. of groups
		    int no_groups = box_height / (line_height + group_spacing);
		    groups = sequential_leader_cluster(items, no_groups);
		    // then distribute slots
		    distribute_slots(groups, no_groups, max_slots_per_group);
		    
		    // and merge overlapping groups
		    merge_overlapping(groups, box_height, line_height, group_spacing, max_slots_per_group, 2 * max_item_deviation);
		    
		    return groups;
		}
		
        private FileLayout() 
        {
        }
	}
}