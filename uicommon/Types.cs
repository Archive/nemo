using System;
using System.Collections.Generic;

namespace Nemo
{
	public class Type
	{
		public int id;
		public string name;
		public string icon;
		
		public Type(int id, string name, string icon)
		{
			this.id = id;
			this.name = name;
			this.icon = icon;
		}
	}

	public sealed class Types
	{
		private List<Type> types;

		private Types()
		{
			types =  new List<Type>();

			// FIXME: hardcoded
			
			types.Add(new Type(0, Mono.Unix.Catalog.GetString("Other"), "text-x-generic.png"));
			types.Add(new Type(1, Mono.Unix.Catalog.GetString("Documents"), "x-office-document.png"));
			types.Add(new Type(2, Mono.Unix.Catalog.GetString("Pdfs"), "pdf.png"));
			types.Add(new Type(3, Mono.Unix.Catalog.GetString("Spreadsheets"), "x-office-spreadsheet.png"));
			types.Add(new Type(4, Mono.Unix.Catalog.GetString("Presentations"), "x-office-presentation.png"));
			types.Add(new Type(5, Mono.Unix.Catalog.GetString("Images"), "image-x-generic.png"));
			types.Add(new Type(6, Mono.Unix.Catalog.GetString("Videos"), "video-x-generic.png"));
			types.Add(new Type(7, Mono.Unix.Catalog.GetString("Archives"), "zip.png"));
		}
		
		public Type get_type(int id)
		{
			return types[id];
		}
	}
}