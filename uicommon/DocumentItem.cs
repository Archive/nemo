using System;
using System.Collections.Generic;

using Mono.Unix;

namespace Nemo
{
	class DocumentItem : Item
	{
		private static string[] documents = { "application/msword", "application/vnd.oasis.opendocument.text", "application/pdf" }; 

		public static bool is_document(string mime)
		{
			foreach (string docs in documents)
				if (mime == docs)
					return true;
			return false;
		}

		private string pages;
		public string snippet;
		private string title;

		public delegate void OnSnippetResult(string snippet);
		public OnSnippetResult on_snippet_callback;
	
		public DocumentItem(File file, string[] fields, string search_text) : base(file)
		{
			on_snippet_callback = null;
		
			populate(search_text);

			parse_fields_and_update(fields);
		}
        
        // Broker specific
        public static string[] ItemTypes()
        {
        	return new string[] { "title", "pagecount" };
        }
        
		// used when parsing the fields array returned from broker
        static int fields_index;
        
        public static void set_fields_index(int index)
        {
        	fields_index = index;
        	System.Console.WriteLine("fields index i document: {0}", fields_index);
        }
        
        private void parse_fields_and_update(string[] fields)
        {
        	mime_type = fields[0];

			set_title(fields[fields_index]);
			set_page_count(fields[fields_index+1]);

			display_item = new DisplayDocumentItem(pages, title, this);
			register_big_starred_change(display_item.update_starred);
			display_item.set_labels();
        }

        // set data
        
        string search_text;
        
		protected void populate(string search_text)
		{
			if (!String.IsNullOrEmpty(search_text)) {
				this.search_text = search_text;
				Singleton<SingletonWrapper<Broker>>.Instance.wrapped.search_snippet(path, search_text, 
														  							Helpers.RunInMainThread<string>(on_snippet_result));
			}
#if false
			} else
				Singleton<SingletonWrapper<Broker>>.Instance.wrapped.get_text(path, Helpers.RunInMainThread<string>(on_snippet_result));
#endif
 		}
        
        protected int set_page_count(string page_count)
        {
			if (page_count.Length == 0)
				return 0;
			else 
			{
				try 
				{
					string number = page_count.Replace(".0", ""); // hack
           		    pages = number;
            		return System.Convert.ToInt32(number);
            	} 
            	catch 
            	{
//            		System.Console.WriteLine("ERROR ERROR in page count from tracker: ->{0}<-", page_count);
           		    pages = "0";
            		return 0;
            	}
            }
		}

        protected void set_title(string t)
        {
        	if (t.Length != 0 && t != " " && t != "Microsoft Word - document.doc") {
				title = t;
				if (title.Length > 25) {
					title = title.Substring(0, 25);
					title += "...";
				}
			} else
				title = String.Empty;
		}
		
		protected void on_snippet_result(string text_snippet)
        {
        	DisplayDocumentItem display_doc_item = (DisplayDocumentItem) display_item;
        
			snippet = format_snippet_text(text_snippet, 60);
			display_doc_item.set_snippet(snippet);

			if (snippet.Length != text_snippet.Length)
				display_doc_item.set_snippet_tip(text_snippet);
				
			if (on_snippet_callback != null) {
				string small_formatted_text = format_snippet_text(text_snippet, 60);
				if (!String.IsNullOrEmpty(small_formatted_text))
					on_snippet_callback(small_formatted_text);
			}
        }

        // helper functions
        public string format_snippet_text(string text, int max_length)
        {
        	text = Helpers.strip_html_tags(text);
        
            if (text.Length < max_length)
            {
            	return text;
            }
            else
            {
                // find search word
                int word_begin = text.IndexOf("<b>");
                int word_end = text.IndexOf("</b>");

                if (word_begin == -1 || word_end == -1 || word_end <= word_begin || word_end - word_begin > max_length)
                {
                	int search_word_pos = text.IndexOf(search_text);
                	
                	int half_distance = max_length/2 - search_text.Length / 2;
                	
                	if (search_word_pos == -1)
                		half_distance = max_length/2;
                		
                	if (search_word_pos > half_distance) {
                		int rest_of_string = Math.Min(text.Length - search_word_pos, 2 * half_distance + search_text.Length);
                		text = text.Substring(search_word_pos - half_distance, rest_of_string);
                	} else {
                		int rest_of_string = Math.Min(text.Length, 2 * half_distance + search_text.Length);
						text = text.Substring(0, rest_of_string);
					}

                	text = text.Replace(search_text, "<b>" + search_text + "</b>");
                	string capitalized = search_text.Substring(0,1).ToUpper() + search_text.Substring(1);
                	text = text.Replace(capitalized, "<b>" + capitalized + "</b>");
					
                    return text;
                }

                word_end += "</b>".Length;

                string small = text.Substring(word_begin, word_end - word_begin);

                int left_length = (max_length - small.Length) / 2;
                int right_length = (max_length - small.Length) / 2;

                if (left_length > word_begin)
                {
                    right_length += left_length - word_begin;
                    left_length = word_begin;
                }

                if (right_length > text.Length - word_end)
                {
                    if (left_length != word_begin)
                    {
                        left_length += right_length - (text.Length - word_end);
                        if (left_length > word_begin)
                            left_length = word_begin;
                    }
                    right_length = text.Length - word_end;
                }

                string left = text.Substring(word_begin - left_length, left_length);
                string right = text.Substring(word_end, right_length);

				return "..." + left + small + right + "...";
            }
        }
	}
}
