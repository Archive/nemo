using System;
using System.Collections.Generic;

namespace Nemo
{
	public class Categories
	{
		public List<Category> categories;

        private VoidFunction search_func;

		private CategoriesDrawer drawer;

		public void set_search_func(VoidFunction sf)
		{
			search_func = sf;
		
			foreach (Category cat in categories)
				cat.search_func = search_func;
		}

		public void set_drawer(CategoriesDrawer cdrawer)
		{
			drawer = cdrawer;
			drawer.set_categories(categories);
			drawer.create_new_empty_category_callback = create_new_temporary_category;
			drawer.delete_category_callback = delete_category;
		}

		private void delete_category(long db_id)
		{
			foreach (Category cat in categories) {
				if (cat.metalabel.db_id == db_id) {
					categories.Remove(cat);
					Singleton<SingletonWrapper<Broker>>.Instance.wrapped.remove_label(cat.metalabel);
					break;
				}
			}
        	search_func();
		}

        private Category create_new_temporary_category()
        {
			MetaLabel label = new MetaLabel();
			label.parent_db_id = -1;
			label.label = Mono.Unix.Catalog.GetString("New Category");
			Category cat = new Category(label);
			cat.search_func = search_func;
			categories.Add(cat);
			return cat;
		}

		public bool is_restricted(long db_id)
		{
			foreach (Category cat in categories)
	            foreach (UserLabel label in cat.labels)
    	            if (label.restricted && label.metalabel.db_id == db_id)
						return true;
						
			return false;
		}

		public void clear_restrictions()
		{
			foreach (Category cat in categories)
	            foreach (UserLabel label in cat.labels)
    	            label.unrestrict();
    	    drawer.clear_restrictions();
		}

		public List<MetaLabel> restrictions()
		{
			List<MetaLabel> result = new List<MetaLabel>();
            
			foreach (Category cat in categories)
	            foreach (UserLabel label in cat.labels)
    	            if (label.restricted)
    	            	result.Add(label.metalabel);

            return result;
		}

		private void got_labels(List<MetaLabel> labels)
		{
			System.Console.WriteLine("got labels in categories {0}", labels.Count);
		
			categories.Clear();
		
			foreach (MetaLabel l in labels)
			{
				if (l.parent_db_id == -1)
				{
					Category cat = new Category(l);
					cat.search_func = search_func;
					categories.Add(cat);
				} 
				else 
				{
					foreach (Category c in categories)
						if (c.metalabel.db_id == l.parent_db_id)
							c.add_label(l);
				}
			}

			System.Console.WriteLine("categories {0}", categories.Count);
			
			drawer.set_categories(categories);
		}
		
		public Categories()
		{
			categories = new List<Category>();

			Singleton<SingletonWrapper<Broker>>.Instance.wrapped.get_all_labels(Helpers.RunInMainThread<List<MetaLabel>>(got_labels));	
		}
	}
}
