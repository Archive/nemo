using System;
using System.Collections.Generic;

using Mono.Unix;

namespace Nemo
{
	class PictureItem : Item
	{
		private static string[] images = { "image/png", "image/jpeg", "image/gif" }; 

		public static bool is_image(string mime)
		{
			foreach (string img in images)
				if (mime == img)
					return true;
			return false;
		}

		private string width;
		private string height;

		public PictureItem(File file, string[] fields) : base(file)
		{
			parse_fields_and_update(fields);
		}
        
        // Broker specific
        public static string[] ItemTypes()
        {
        	return new string[] { "width", "height" };
        }
        
		// used when parsing the fields array returned from broker
        static int fields_index;
        
        public static void set_fields_index(int index)
        {
        	fields_index = index;
        	System.Console.WriteLine("fields index i pictures: {0}", fields_index);
        }
        
        private void parse_fields_and_update(string[] fields)
        {
        	mime_type = fields[0];
        	
			height = fields[fields_index];
			width = fields[fields_index+1];
			
//			foreach (string t in fields)
//				System.Console.WriteLine(t);
			
			string resolution = width + "x" + height;
			
			display_item = (DisplayItem) new DisplayPictureItem(resolution, this);
			register_big_starred_change(display_item.update_starred);
			display_item.set_labels();
        }
	}
}
