using System;
using System.Collections.Generic;
using System.Diagnostics;

using Mono.Unix;

namespace Nemo
{
	public class CalendarDriver
	{
		// helper classes/functions
		private CalendarDrawer drawer;

        public VoidFunction search_func;
		public VoidFunction<CalendarDriver.View> set_active_view;

		public CalendarDriver(CalendarDrawer d)
		{
			drawer = d;
			drawer.update_view = update_view;
			drawer.update_view_set_next = update_view_set_next; 
			calendar = new System.Globalization.GregorianCalendar();
			set_default_state();
		}

		public void set_date_to_today()
		{
			centered_date = System.DateTime.Now;
			current_state = new State(current_state.view, centered_date);
			search_func();
		}

		public void update_view_set_next(View new_view, DateTime new_date)
		{
			Singleton<OverlayTracker>.Instance.hide_all_and_die();

			centered_date = new_date;

			if (new_view == View.Year)
				set_year_view();
			else if (new_view == View.Month)
				set_month_view();
			else if (new_view == View.Week)
				set_week_view();
			else if (new_view == View.Day)
				set_day_view();

			set_active_view(current_state.view);
		}

		private void update_view(DateTime new_date)
		{
			Singleton<OverlayTracker>.Instance.hide_all_and_die();

			centered_date = new_date;

			if (current_state.view == View.Year)
				set_month_view();
			else if (current_state.view == View.Month)
				set_week_view();
			else if (current_state.view == View.Week)
				set_day_view();

			set_active_view(current_state.view);
		}

		public enum View { Day, Week, Month, Year, Years }

		public struct State
		{
			public DateTime start_time;
			public DateTime end_time;
			
			public Tuple<DateTime, DateTime> dates()
			{
				print_real_dates();
				return new Tuple<DateTime, DateTime>(start_time, end_time); 
			}
			
			public void print_real_dates()
			{
				System.Console.WriteLine("{0}", start_time);
				System.Console.WriteLine("{0}", end_time);
			}
			
			public View view;
			
			public State(View current_view, DateTime current_selected)
			{
				calendar = new System.Globalization.GregorianCalendar();
			
				view = current_view;
				
				// FIXME: perhaps, let end time be current_selected 
				
				if (view == View.Year) 
				{
					start_time = new System.DateTime(current_selected.Year, 1, 1);	
					end_time = new System.DateTime(current_selected.Year, 12, calendar.GetDaysInMonth(current_selected.Year, 12));	
				} 
				else if (view == View.Month) 
				{
					start_time = new System.DateTime(current_selected.Year, current_selected.Month, 1);
					// always start a week on a monday
					start_time -= new System.TimeSpan(days_from_monday(calendar.GetDayOfWeek(start_time)), 0, 0, 0);

					end_time = new System.DateTime(current_selected.Year, current_selected.Month, calendar.GetDaysInMonth(current_selected.Year, current_selected.Month));	
					// always end a week on a sunday
					end_time += new System.TimeSpan(days_til_monday(calendar.GetDayOfWeek(end_time)), 0, 0, 0);
					
					//System.Console.WriteLine("setting start: {0} - {1}, end: {2} - {3}", 
					//start_time, calendar.GetDayOfWeek(start_time), end_time, calendar.GetDayOfWeek(end_time));
				}
				else if (view == View.Week) 
				{
					start_time = current_selected;
					// always start a week on a monday
					start_time -= new System.TimeSpan(days_from_monday(calendar.GetDayOfWeek(start_time)), 0, 0, 0);
					start_time = new DateTime(start_time.Year, start_time.Month, start_time.Day); // set time to 12am

					end_time = current_selected;
					// always end a week on a sunday
					
					end_time += new System.TimeSpan(days_til_monday(calendar.GetDayOfWeek(end_time)), 0, 0, 0);
					end_time = new DateTime(end_time.Year, end_time.Month, end_time.Day); // set time to 12am
				} 
				else if (view == View.Day) 
				{
					start_time = new DateTime(current_selected.Year, current_selected.Month, current_selected.Day); // set time to 12am
					
					end_time = current_selected;
					end_time += new System.TimeSpan(1, 0, 0, 0);
					end_time = new DateTime(end_time.Year, end_time.Month, end_time.Day); // set time to 12am
				} 
				else
				{
					Debug.Assert(false);
					// should never happen
					start_time = current_selected;
					end_time = current_selected;
				}

				Debug.Assert(start_time != end_time);
			}

			// helper functions
			private static int days_from_monday(System.DayOfWeek day)
			{
				if (day == DayOfWeek.Sunday)
					return 6;
				else if (day == DayOfWeek.Saturday)
					return 5;
				else if (day == DayOfWeek.Friday)
					return 4;
				else if (day == DayOfWeek.Thursday)
					return 3;
				else if (day == DayOfWeek.Wednesday)
					return 2;
				else if (day == DayOfWeek.Tuesday)
					return 1;
				else
					return 0;
			}

			private static int days_til_monday(System.DayOfWeek day)
			{
			    if (day == DayOfWeek.Tuesday)
					return 6;
				else if (day == DayOfWeek.Wednesday)
					return 5;
				else if (day == DayOfWeek.Thursday)
					return 4;
				else if (day == DayOfWeek.Friday)
					return 3;
				else if (day == DayOfWeek.Saturday)
					return 2;
				else if (day == DayOfWeek.Sunday)
					return 1;
				else
					return 7;
			}

			private	System.Globalization.Calendar calendar;
		}

		private State current_state;

		private DateTime centered_date;

		private	System.Globalization.Calendar calendar;

		private void set_default_state()
		{
			centered_date = System.DateTime.Now;

			//System.Console.WriteLine("{0}", centered_date);

			current_state = new State(View.Month, centered_date);
		}			

		public void set_day_view()
		{
			current_state = new State(View.Day, centered_date);
			search_func();
		}

		public void set_week_view()
		{
			current_state = new State(View.Week, centered_date);
			search_func();
		}

		public void set_month_view()
		{
			current_state = new State(View.Month, centered_date);
			search_func();
		}

		public void set_year_view()
		{
			current_state = new State(View.Year, centered_date);
			search_func();
		}

		// hook up med week, month, year

		public void prev()
		{
			if (current_state.view == View.Day)
				prev_day();
			else if (current_state.view == View.Week)
				prev_week();
			else if (current_state.view == View.Month)
				prev_month();
			else if (current_state.view == View.Year)
				prev_year();

			// update calendar drawer
			//System.Console.WriteLine("{0}", centered_date);
			search_func();
		}

		private void prev_day()
		{
			centered_date -= new System.TimeSpan(1, 0, 0, 0);
			current_state = new State(View.Day, centered_date);
		}

		private void prev_week()
		{
			centered_date -= new System.TimeSpan(7, 0, 0, 0);
			current_state = new State(View.Week, centered_date);
		}
		
		private void prev_month()
		{
			int prev_month = centered_date.Month - 1; 
			int prev_year = centered_date.Year;

			if (prev_month == 0) {
				prev_month = 12;
				prev_year -= 1;
			}

			centered_date -= new System.TimeSpan(calendar.GetDaysInMonth(prev_year, prev_month), 0, 0, 0);
			current_state = new State(View.Month, centered_date);
		}

		private void prev_year()
		{
			centered_date -= new System.TimeSpan(calendar.GetDaysInYear(centered_date.Year - 1), 0, 0, 0);
			current_state = new State(View.Year, centered_date);
		}

		// FIXME: next probably doesn't make sense beyond the present
		public void next()
		{
			if (current_state.view == View.Day)
				next_day();
			else if (current_state.view == View.Week)
				next_week();
			else if (current_state.view == View.Month)
				next_month();
			else if (current_state.view == View.Year)
				next_year();
				
			// update calendar drawer
			//System.Console.WriteLine("{0}", centered_date);
			search_func();
		}

		private void next_day()
		{
			centered_date += new System.TimeSpan(1, 0, 0, 0);
			current_state = new State(View.Day, centered_date);
		}

		private void next_week()
		{
			centered_date += new System.TimeSpan(7, 0, 0, 0);
			current_state = new State(View.Week, centered_date);
		}
		
		private void next_month()
		{
			int next_month = centered_date.Month + 1; 
			int next_year = centered_date.Year;

			if (next_month == 13) {
				next_month = 1;
				next_year += 1;
			}

			centered_date += new System.TimeSpan(calendar.GetDaysInMonth(next_year, next_month), 0, 0, 0);
			current_state = new State(View.Month, centered_date);
		}

		private void next_year()
		{
			centered_date += new System.TimeSpan(calendar.GetDaysInYear(centered_date.Year + 1), 0, 0, 0);
			current_state = new State(View.Year, centered_date);
		}

		public Tuple<DateTime, DateTime> restriction()
		{
			return current_state.dates();
		}
		
		public void new_elements_in_view(List<ItemWrapper> elements)
		{
			drawer.draw(current_state, centered_date, elements);
		}
	}
}
