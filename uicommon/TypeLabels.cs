using System;
using System.Collections.Generic;
using System.Text;

namespace Nemo
{
    public class TypeLabels
    {
        public VoidFunction<List<TypeLabel>> update_func;
        public VoidFunction search_func;

        public void update_mainwindow()
        {
            update_func(labels);
        }

        protected List<TypeLabel> labels;

        public IEnumerator<TypeLabel> GetEnumerator()
        {
            return (labels as IEnumerable<TypeLabel>).GetEnumerator();
        }

		public List<long> restrictions()
		{
			List<long> result = new List<long>();
            
            foreach (TypeLabel label in labels)
                if (label.restricted)
					result.Add(label.id());
	
			if (result.Count == 0) // default case: only get results for types we like
	            foreach (TypeLabel label in labels)
					result.Add(label.id());

            return result;
		}

		public void restrict_and_update(long id)
		{
			System.Console.WriteLine("restricting and updating types");
			restrict(id);
			search_func();
		}

		public void restrict(long id)
		{
			System.Console.WriteLine("restrict {0}", id);
		
            foreach (TypeLabel label in labels)
                if (label.id() == id) {
                	label.restricted = !label.restricted; 
					break;
				}
		}

		public void clear_restrictions()
		{
            foreach (TypeLabel label in labels)
               	label.unrestrict();
		}
		
        public void clear()
        {
            labels.Clear();
        }

		private static int CompareLabels(TypeLabel lhs, TypeLabel rhs)
		{
			return (int) (lhs.id() - rhs.id());
		}

		private void on_broker_labels_result(List<Tuple<FileTypeCategory, int>> new_labels)
		{
			if (new_labels.Count == labels.Count)
				return; // optimization, we don't care about number of files for each type
		
			labels.Clear();
		
			int count = 0;

			// FIXME: preserve restrictions
			
			foreach (Tuple<FileTypeCategory, int> label_pair in new_labels)
			{
				labels.Add(new TypeLabel(Singleton<Types>.Instance.get_type((int)label_pair.first)));
				++count;
			}

			labels.Sort(CompareLabels);
				
			System.Console.WriteLine("got {0} type labels from tracker", count);

			update_mainwindow();
			search_func();
		}

        public TypeLabels() 
        {
            labels = new List<TypeLabel>();
			
            Singleton<SingletonWrapper<Broker>>.Instance.wrapped.get_type_labels(Helpers.RunInMainThread<List<Tuple<FileTypeCategory, int>>>(on_broker_labels_result));
        }
    }
}
