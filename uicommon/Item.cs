using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using Mono.Unix;

using System.Diagnostics; // for opening files

namespace Nemo
{
	// FIXME: pattern
	// pointer abstraction
	public class ItemWrapper
	{
		public Item item;
		
		public ItemWrapper(Item item)
		{
			this.item = item;
		}
	}

	public class Item
	{
		public DisplayItem Display()
		{
			return display_item;
		}

		public string mime_type;

		protected DisplayItem display_item;

		public File file; // FIXME: use this more
		
		public DateTime last_modify;

		public DateTime last_accessed;

		protected long long_size;
		
		public string long_name;

		string extension;

//		public string name; // trimmed
		
		public string modify;
		public string accessed;
		public string size;
		
		public string path;

		public List<UserLabel> labels;

		public VoidFunction on_labels_callback;
		public VoidFunction on_data_callback;

		public string name(int length)
		{
			string name = long_name;
			
			if (long_name.Length-extension.Length > length) {
				name = long_name.Substring(0, length-5);
				name += "[...]" + extension;
			}
			
			return name;
		}

		public Item(File file)
		{
			this.file = file;

			setup(file.path);

			long_size = file.bytesize;
			set_size();
			
			last_modify = file.last_modified;
			last_accessed = file.last_accessed;
			
			modify = date_to_string(file.last_modified);
			accessed = date_to_string(file.last_accessed);
			
			long_name = file.path.Substring(file.path.LastIndexOf("/")+1); 
			
			int pos;
			if ((pos = long_name.LastIndexOf(".")) != -1)
				extension = long_name.Substring(pos);

			if (extension.Length > 4)
				extension = extension.Substring(0, 4);

			display_item = new DisplayItem(this);
			register_big_starred_change(display_item.update_starred);

			foreach (MetaLabel label in file.labels)
				labels.Add(new UserLabel(label));
			
			display_item.set_labels();
		}

		private void setup(string path)
		{
			// internal variables
			this.path = path;

			labels = new List<UserLabel>();
		}

		public string image()
		{
			return Singleton<Types>.Instance.get_type((int)file.type.category).icon;
		}
		
		public void open()
		{
		  Process.Start("gnome-open '" + path + "'");
		}

	        public VoidFunction<bool> small_starred_update_function;
   	        VoidFunction<bool> big_starred_update_function;

		public void register_small_starred_change(VoidFunction<bool> update_function)
		{
			small_starred_update_function = update_function;
		}
		
		public void register_big_starred_change(VoidFunction<bool> update_function)
		{
			big_starred_update_function = update_function;
		}
		
		public void update_starred()
		{
			file.starred = !file.starred;
			
			Singleton<SingletonWrapper<Broker>>.Instance.wrapped.set_starred(file, file.starred);

			if (small_starred_update_function != null)
			    small_starred_update_function(file.starred);

			if (big_starred_update_function != null)
			    big_starred_update_function(file.starred);
		}
		
		// labels
	    public void make_label(UserLabel label)
    	{
			// update own labels       		
   			labels.Add(label);

			display_item.set_labels();

			if (on_labels_callback != null)
				on_labels_callback();

			// update tracker
			Singleton<SingletonWrapper<Broker>>.Instance.wrapped.add_label_to_file(file, label.metalabel);
    	}
		
		public VoidFunction search_func;
	
		public void remove_label(UserLabel label)
		{
			List<UserLabel> new_labels = new List<UserLabel>();
		
			foreach (UserLabel tmp_label in labels)
				if (tmp_label.metalabel.db_id != label.metalabel.db_id)
					new_labels.Add(tmp_label);
					
			labels = new_labels;
			
			display_item.set_labels();

			if (on_labels_callback != null)
				on_labels_callback();

			Singleton<SingletonWrapper<Broker>>.Instance.wrapped.remove_label_from_file(file, label.metalabel);
		}

		public bool contains_label(UserLabel label)
		{
			return labels.Exists(delegate (UserLabel tmp_label) {
				return tmp_label.metalabel.db_id == label.metalabel.db_id;
			});
		}
		
		private void set_size()
		{
			if (long_size > 1000*1000*1000)
				size = String.Format("{0:f2} gb", long_size/(1000*1000*1000.0));
			else if (long_size > 1000*1000)
				size = String.Format("{0:f2} mb", long_size/(1000*1000.0));
			else
				size = String.Format("{0:f2} kb", long_size/1000.0);
		}
		
        private string date_to_string(DateTime date)
        {
			return String.Format("{0:HH:mm:ss} {0:dd/MM/yy}", date); // {0:HH:mm:ss} 
        }
	}
}
