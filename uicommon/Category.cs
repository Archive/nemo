using System;
using System.Collections.Generic;

namespace Nemo
{
	public class Category
	{
		public List<UserLabel> labels;
		
		public MetaLabel metalabel;
		
		public VoidFunction search_func;
		
		public bool initial;
		
		// for serialization
		public void clear_restrictions()
		{
			foreach (UserLabel label in labels)
				label.restricted = false;
		}
		
		public Category() // for serialization
		{
		}
		
		public Category(MetaLabel label)
		{
			initial = label.initial;
		
			metalabel = label;
			
			labels = new List<UserLabel>();
		}

        public void add_label(string label_name)
        {
        	Singleton<SingletonWrapper<Broker>>.Instance.wrapped.add_label(metalabel.db_id, label_name);
		}

        public void add_label(MetaLabel l)
        {
            labels.Add(new UserLabel(l));
		}

		public void rename_label(MetaLabel label, string new_name)
		{
			Singleton<SingletonWrapper<Broker>>.Instance.wrapped.rename_label(label, new_name);
			search_func();
		}

        public void remove_label(MetaLabel label)
        {
        	Singleton<SingletonWrapper<Broker>>.Instance.wrapped.remove_label(label);
        	search_func();
        }

		public void create_in_db(string name)
		{
			Singleton<SingletonWrapper<Broker>>.Instance.wrapped.add_label(-1, name);
		}

		public void rename(string new_name)
		{
			rename_label(metalabel, new_name);
		}

		public bool exists(string name)
		{
			return labels.Exists(delegate (UserLabel tmp_label) {
                return tmp_label.metalabel.label == name;
			});
		}

		public void update_restriction_and_update_screen(MetaLabel label)
		{
			update_restriction(label);
			search_func();
		}

		public void update_restriction(MetaLabel restricted)
		{
            foreach (UserLabel label in labels) {
                if (label.metalabel.db_id == restricted.db_id) {
					label.restricted = !label.restricted;
					break;
				}
			}
		}
	}
}
