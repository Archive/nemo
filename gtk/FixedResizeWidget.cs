using System;
using System.Collections.Generic;
using Gtk;

namespace Nemo
{
	public class FixedResizeWidget : Fixed
	{
		private static int unique_id = 0;

		private static int line_height = 22;
		
		private static double spacing;
		private bool start_at_top;

		public VoidFunction<DateTime> update_view;
		public VoidFunction<CalendarDriver.View, DateTime> update_view_set_next;
		public Function<int[]> calendar_rectangle;
		
		int id;
		
		public FixedResizeWidget(double s, bool top_start) : base()
		{
			id = ++unique_id;
		
			spacing = s;
			start_at_top = top_start;

			items = new List<ItemWrapper>();
		}

		public List<ItemWrapper> items;
		public DateTime view_period_start;
		public TimeSpan view_period;

		T create_right_click_button<T>(Item item, int button_width) where T:RightClickItemButton, new()
		{
			T b = new T();
		
			b.set_item(item);
			
			b.starred = item.file.starred;

			// FIXME: call it icon, if it's an icon
			string image = Singleton<Types>.Instance.get_type((int)item.file.type.category).icon;
			if (!String.IsNullOrEmpty(image)) 
			{
				Gtk.HBox tmp = new Gtk.HBox();

				Gtk.Alignment img_alignment = new Gtk.Alignment(0.5F, 0.5F, 0.5F, 0.5F);
				img_alignment.RightPadding = 5;
				
				b.set_picture(img_alignment, image);
				
				item.register_small_starred_change(b.update_starred);
				
//				Gtk.Image img = new Gtk.Image(null, image);
				img_alignment.Add(b.picture);

				tmp.PackStart(img_alignment, false, false, 0); 					
				
				Gtk.Alignment label_alignment = new Gtk.Alignment(0F, 0.5F, 0F, 0.5F);
				label_alignment.RightPadding = 5;
				Gtk.Label tmp_label = new Gtk.Label(item.long_name);
				tmp_label.UseUnderline = false;
				label_alignment.Add(tmp_label);
				tmp.PackStart(label_alignment, true, true, 0);

				if (item.labels.Count > 0) {
					Gtk.HBox colors = new Gtk.HBox();

					foreach (UserLabel label in item.labels)
						colors.Add(label.dot());

					tmp.PackStart(colors, false, false, 0);
				}

				b.Add(tmp);
			
			} else
				b.Label = item.name(25);

			b.BorderWidth = 0;
			b.Relief = Gtk.ReliefStyle.None;
			b.SetAlignment(0, 0);
			b.WidthRequest = button_width;
			
			GtkCommon.create_preview(item, b, b);
			
			return b;
		}

		static protected Dictionary<UserLabel, int> create_more_dictionary(FileLayout.Group group, int c)
		{
			Dictionary<UserLabel, int> count = new Dictionary<UserLabel, int>();
		
			int mc = 0;
			foreach (FileLayout.Element element in group.items)
			{
				if (mc >= c)
				{
					foreach (UserLabel label in element.wrapper.item.labels) {
						bool found = false;
						foreach (KeyValuePair<UserLabel, int> kvp in count)
							if (kvp.Key.metalabel.db_id == label.metalabel.db_id) { 
								count[kvp.Key] += 1;
								found = true;
								break;
							}
							
						if (!found)
							count.Add(label, 1);
					}
				}
				++mc;
			}
			
			return count;
		}

		Gdk.Rectangle last_allocation;
		
		protected override void OnSizeAllocated(Gdk.Rectangle allocation)
		{
			base.OnSizeAllocated(allocation);

			Helpers.RunOnlyOnceInMainLIFO(id.ToString(), delegate { size_changed(allocation); }, 100);
		}
		
		private void size_changed(Gdk.Rectangle allocation)
		{
			if (allocation.Height == last_allocation.Height && allocation.Width == last_allocation.Width)
				return;

			last_allocation = allocation;

			foreach (Widget w in Children)
				w.Destroy();

			List<FileLayout.Element> percent = new List<FileLayout.Element>();
		
			foreach (ItemWrapper wrapper in items) {
				double percentage = (wrapper.item.last_modify - view_period_start).TotalSeconds / view_period.TotalSeconds;
				percent.Add(new FileLayout.Element(percentage, wrapper));				
			}

			int box_height = allocation.Height;
			
			box_height -= 15; // for more

			int added_files = 0;
			
			List<FileLayout.Group> groups = Singleton<FileLayout>.Instance.layout(percent, box_height, line_height, 0, spacing);

			foreach (FileLayout.Group group in groups) {
				int begin = 0, end = 0;
				group.position(box_height, line_height, out begin, out end);

				if (start_at_top && groups.Count == 1)
					begin = 0;

				int c = 0;
				foreach (FileLayout.Element element in group.items) {
					if (c >= group.slots && !group.has_only_one_more(c))
                		break;

					RightClickItemButton b = create_right_click_button<RightClickItemButton>(element.wrapper.item, allocation.Width);

					// go lambda bug, go
					ItemWrapper tmp_wrapper = element.wrapper; 
					int tmp_c = c;

					tmp_wrapper.item.on_labels_callback = delegate {  
						Remove(b);

						b = create_right_click_button<RightClickItemButton>(tmp_wrapper.item, allocation.Width); 
						Put(b, 0, begin+tmp_c*line_height);
						ShowAll();
					};					
					
					tmp_wrapper.item.on_data_callback = delegate {  
						b.set_item(tmp_wrapper.item);
						GtkCommon.create_preview(tmp_wrapper.item, b, b);
					};

					added_files += 1;
					
					Put(b, 0, begin+tmp_c*line_height);
            		
		            c += 1;
				}
				
				if (c < group.items.Count) {
				
					Gtk.Button b = new ClickButton();

					Gtk.VBox more_button = new Gtk.VBox();
					
					Dictionary<UserLabel, int> count = create_more_dictionary(group, c);

					if (count.Count > 0) 
					{
						Gtk.Label label = new Gtk.Label();
						label.Markup = "<small>" + String.Format(Mono.Unix.Catalog.GetString("+ {0} more"), group.items.Count - c) + "</small>";
						
						more_button.Add(label);

						Gtk.Alignment userlabels_alignment = new Gtk.Alignment(0.5f, 0.5f, 0f, 0f);
						Gtk.HBox userlabels = new Gtk.HBox();
						userlabels_alignment.Add(userlabels);
						
						foreach (KeyValuePair<UserLabel, int> kvp in count)
						{
							Gtk.HBox userlabel = new Gtk.HBox();
							userlabel.Add(kvp.Key.dot());
							Gtk.Label tl = new Gtk.Label();

							tl.Markup = "<small>" + String.Format("x{0}", kvp.Value) + "</small>";
				   			GtkCommon.set_foreground_color(tl, new Gdk.Color(kvp.Key.metalabel.color.r, kvp.Key.metalabel.color.g, kvp.Key.metalabel.color.b));

							userlabel.Add(tl);
							
							userlabels.PackStart(userlabel, false, false, 3);
						}

						more_button.PackStart(userlabels_alignment, true, true, 0);
						
					} else {
						Gtk.Label l = new Gtk.Label();
						l.Markup = "<small>" + String.Format(Mono.Unix.Catalog.GetString("+ {0} more"), group.items.Count - c) + "</small>";
						more_button.Add(l);
					}
					
					b.Add(more_button);

					b.BorderWidth = 0;
					b.Relief = Gtk.ReliefStyle.None;
					b.SetAlignment(0, 0);
					b.WidthRequest = allocation.Width;

//					button_width = allocation.Width;

					b.ButtonPressEvent += delegate(object sender, Gtk.ButtonPressEventArgs args) { 
						MorePopup more_popup = new MorePopup(this, percent, delegate {
							int x, y;
							this.ParentWindow.GetOrigin(out x, out y); 
							return new int[] { allocation.Width, x, y };
						});
						Singleton<OverlayTracker>.Instance.add_overlay_and_show(more_popup);
					};

					Put(b, 0, begin+c*line_height);
				}
			}
			
			if (added_files > 0) // the ShowAll call in calendardrawer will take care of drawing it with no elements   
				ShowAll();
		}

		public class MorePopup : Overlay
		{
			private Gtk.Window window;
			private FixedResizeWidget parent;

			int button_width;

			private List<FileLayout.Element> percent;
			Function<int[]> button_position;

			public MorePopup(FixedResizeWidget parent_widget, List<FileLayout.Element> _percent, Function<int[]> button_pos)
			{
				parent = parent_widget;
				window = null;
				percent = _percent;
				
				button_position = button_pos;
				
				set_position();

				name = "more_popup";
			}

			private void set_position()
			{
				int[] button_pos = button_position();
				
				button_width = button_pos[0];

				x = button_pos[1];
				y = button_pos[2];
			}

			public override void shift(int shift_x, int shift_y)
			{
				int cur_x, cur_y;
				window.GetPosition(out cur_x, out cur_y);
				window.Move(cur_x + shift_x, cur_y + shift_y);

				x = cur_x + shift_x;
				y = cur_y + shift_y;
			}
		
			public override void resize()
			{
				set_position();

				if (window != null) {
					window.Destroy(); 
					window = null;
					show();
				}
			}

			public override void show()
			{
				int[] cal_rect = parent.calendar_rectangle();
				int cal_y = cal_rect[1];
				int cal_height = cal_rect[3];

				if (window == null) {
				
					window = new Window(WindowType.Popup);

					Gtk.EventBox box_wrapper = new Gtk.EventBox();

					Gtk.VBox box = new Gtk.VBox();

					box_wrapper.Add(box);

					GtkCommon.set_background_color(box_wrapper, "white");

			        Gtk.Alignment alignment = new Gtk.Alignment(1F, 0.5F, 0F, 0F);
					Gtk.EventBox image_wrapper = new EventBox();
					Gtk.Image close_image = new Gtk.Image(null, "close.png");
					image_wrapper.Add(close_image);
					if (String.IsNullOrEmpty(name))
						throw new Exception("name for an overlay cannot be empty");
					image_wrapper.ButtonPressEvent += delegate { 
						Singleton<OverlayTracker>.Instance.hide_and_die(name); 
					};
					GtkCommon.set_background_color(image_wrapper, "white");
					alignment.Add(image_wrapper);
					box.Add(alignment);

					Gtk.Fixed layout = new Gtk.Fixed();

					int more_box_height = cal_height - 50;

					more_box_height -= 15; // for more

					List<FileLayout.Group> groups = Singleton<FileLayout>.Instance.layout(percent, more_box_height, line_height, 0, 0.5);

					if (groups.Count != 1)
						throw new Exception("File Layout is buggy");

					int slots = 0;
					
					foreach (FileLayout.Group group in groups) {
						int c = 0;
						foreach (FileLayout.Element element in group.items) {
							if (c >= group.slots && !group.has_only_one_more(c))
		                		break;

							RightClickInactiveItemButton b = parent.create_right_click_button<RightClickInactiveItemButton>(element.wrapper.item, (int)(1.5*button_width));

							layout.Put(b, 0, c*line_height);
		            		
				            c += 1;
						}

						if (c < group.items.Count) {

							Gtk.EventBox event_box = new EventBox();

							Gtk.Button b = new ClickButton();

							Gtk.VBox more_button = new Gtk.VBox();
							
							Dictionary<UserLabel, int> count = create_more_dictionary(group, c);

							if (count.Count > 0) 
							{
								Gtk.Label label = new Gtk.Label();
								label.Markup = "<small>" + String.Format(Mono.Unix.Catalog.GetString("+ {0} more"), group.items.Count - c) + "</small>";
								
								more_button.Add(label);

								Gtk.Alignment userlabels_alignment = new Gtk.Alignment(0.5f, 0.5f, 0f, 0f);
								Gtk.HBox userlabels = new Gtk.HBox();
								userlabels_alignment.Add(userlabels);
								
								foreach (KeyValuePair<UserLabel, int> kvp in count)
								{
									Gtk.HBox userlabel = new Gtk.HBox();
									userlabel.Add(kvp.Key.dot());
									Gtk.Label tl = new Gtk.Label();

									tl.Markup = "<small>" + String.Format("x{0}", kvp.Value) + "</small>";
						   			GtkCommon.set_foreground_color(tl, new Gdk.Color(kvp.Key.metalabel.color.r, kvp.Key.metalabel.color.g, kvp.Key.metalabel.color.b));

									userlabel.Add(tl);
									
									userlabels.PackStart(userlabel, false, false, 3);
								}

								more_button.PackStart(userlabels_alignment, true, true, 0);
								
							} else {
								Gtk.Label l = new Gtk.Label();
								l.Markup = "<small>" + String.Format(Mono.Unix.Catalog.GetString("+ {0} more"), group.items.Count - c) + "</small>";
								more_button.Add(l);
							}
							
							b.Add(more_button);

							b.BorderWidth = 0;
							b.Relief = Gtk.ReliefStyle.None;
							b.SetAlignment(0, 0);
							b.WidthRequest = (int)(1.5*button_width);

							event_box.Add(b);

							event_box.ButtonPressEvent += delegate { 
								parent.update_view_set_next(CalendarDriver.View.Day, group.items[0].wrapper.item.last_modify);
							};

							GtkCommon.set_background_color(event_box, "white");

							layout.Put(event_box, 0, c*line_height);
						}

						slots += group.slots;
					}
					
					box.Add(layout);

					Gtk.Frame frame = new Frame();
					frame.Add(box_wrapper);

					window.Add(frame);

					int move_to_x = x, move_to_y = y;

					int box_height = slots*(line_height+2);

					System.Console.WriteLine("total height: {0}, y: {1}, slots: {2}, line height: {3}", cal_height, y, slots, line_height);

					if (y + box_height > cal_y + cal_height) {
						move_to_y -= (y + box_height) - (cal_y + cal_height); // subtract how much it sticks out
						System.Console.WriteLine("it sticks out: {0}", (y + box_height) - (cal_y + cal_height));
					}

					if (move_to_x + 2 * button_width > Gdk.Screen.Default.Width)
						move_to_x = Gdk.Screen.Default.Width - 2 * button_width;

		   			window.Move(move_to_x, move_to_y);
				}
				
	   			window.ShowAll();
			}
			
			public override void hide()
			{
				if (window != null)
					window.Hide();
			}
		}
	}
}
