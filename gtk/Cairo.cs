using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Reflection;
using Cairo;
using Gtk;

namespace Nemo
{	
	static public class CairoDrawing
	{
		private static string tmp_image_path = "/tmp/nemo_cairo.png";

		public static Gtk.Image create_small_starred_image(string pic_path)
		{
			// evil hack because the interface to cairo is pretty bad
			Assembly asm = Assembly.GetCallingAssembly();

			Stream img_s = asm.GetManifestResourceStream(pic_path);

			FileStream img_fs = new System.IO.FileStream(tmp_image_path, FileMode.Create);
			
			for (int i = 0; i < img_s.Length; ++i)
				img_fs.WriteByte((byte)img_s.ReadByte());
				
			img_fs.Close();
			
			using (Cairo.ImageSurface img_surface = new ImageSurface(tmp_image_path)) {
				System.IO.File.Delete(tmp_image_path);

				using (Cairo.Context context = new Context(img_surface)) {
					Stream s = asm.GetManifestResourceStream("starred_right.png");

					FileStream fs = new System.IO.FileStream(tmp_image_path, FileMode.Create);
					
					for (int i = 0; i < s.Length; ++i)
						fs.WriteByte((byte)s.ReadByte());
						
					fs.Close();
					
					using (Cairo.ImageSurface star_surface = new ImageSurface(tmp_image_path)) {
						System.IO.File.Delete(tmp_image_path);
						
						context.SetSource(star_surface, img_surface.Width-star_surface.Width, img_surface.Height-star_surface.Height);
						context.Paint();

						img_surface.WriteToPng(tmp_image_path);
						Gtk.Image img = new Gtk.Image(tmp_image_path);
						System.IO.File.Delete(tmp_image_path);

						return img;
					}
				}
			}
		}

		public static Gtk.Image create_big_starred_image(string pic_path)
		{
			using (Cairo.ImageSurface img_surface = new ImageSurface(pic_path)) {
				using (Cairo.ImageSurface surface = new Cairo.ImageSurface(Cairo.Format.ARGB32, img_surface.Width + 2, img_surface.Height + 2)) {
					using (Cairo.Context context = new Context(surface)) {
						Gdk.Pixbuf tmp_pixbuf = new Gdk.Pixbuf(pic_path);
						if (!tmp_pixbuf.HasAlpha) { // img_surface.Format not available...
							context.Rectangle(0, 0, img_surface.Width+2, img_surface.Height+2); 
							context.Fill();
							context.Stroke();
						}

						context.SetSource(img_surface, 1, 1);
						context.Paint();

						// evil hack because the interface to cairo is pretty bad

						Assembly asm = Assembly.GetCallingAssembly();

						Stream s = asm.GetManifestResourceStream("big_star.png");

						FileStream fs = new System.IO.FileStream(tmp_image_path, FileMode.Create);
						
						for (int i = 0; i < s.Length; ++i)
							fs.WriteByte((byte)s.ReadByte());
							
						fs.Close();
						
						using (Cairo.ImageSurface star_surface = new ImageSurface(tmp_image_path)) {
							System.IO.File.Delete(tmp_image_path);
							
							context.SetSource(star_surface, img_surface.Width-star_surface.Width, img_surface.Height-star_surface.Height);
							context.Paint();

							surface.WriteToPng(tmp_image_path);
							Gtk.Image img = new Gtk.Image(tmp_image_path);
							System.IO.File.Delete(tmp_image_path);
							return img;
						}
					}
				}
			}
		}

		public static Gtk.Image create_image_with_border(string pic_path)
		{
			using (Cairo.ImageSurface img_surface = new ImageSurface(pic_path)) {
				using (Cairo.ImageSurface surface = new Cairo.ImageSurface(Cairo.Format.ARGB32, img_surface.Width + 2, img_surface.Height + 2)) {
					using (Cairo.Context context = new Context(surface)) {
						Gdk.Pixbuf tmp_pixbuf = new Gdk.Pixbuf(pic_path);
						if (!tmp_pixbuf.HasAlpha) { // img_surface.Format not available...
							context.Rectangle(0, 0, img_surface.Width+2, img_surface.Height+2); 
							context.Fill();
							context.Stroke();
						}

						context.SetSource(img_surface, 1, 1);
						context.Paint();

						surface.WriteToPng(tmp_image_path);
						Gtk.Image img = new Gtk.Image(tmp_image_path);
						System.IO.File.Delete(tmp_image_path);
						return img;
					}
				}
			}
		}

		public static Gtk.Image create_dot_image(int r, int g, int b, int a, int width, int height)
		{
			using (Cairo.ImageSurface surface = new Cairo.ImageSurface(Cairo.Format.ARGB32, width, height)) {
				using (Cairo.Context context = new Context(surface)) {
					draw(context, width, height, new Cairo.Color(((double)r)/255, ((double)g)/255, ((double)b)/255, ((double)a)/255));
					surface.WriteToPng(tmp_image_path);
					Gtk.Image img = new Gtk.Image(tmp_image_path);
					System.IO.File.Delete(tmp_image_path);
					return img;
				}
			}
		}

		public static Gtk.Image create_empty_image(int width, int height)
		{
			using (Cairo.ImageSurface surface = new Cairo.ImageSurface(Cairo.Format.ARGB32, width, height)) {
				surface.WriteToPng(tmp_image_path);
				Gtk.Image img = new Gtk.Image(tmp_image_path);
				System.IO.File.Delete(tmp_image_path);
				return img;
			}
		}

        static readonly double M_PI = 3.14159265358979323846;

		static void oval_path(Cairo.Context gr, double xc, double yc, double xr, double yr)
		{
			gr.Translate(xc, yc);
			gr.Scale(1.0, yr / xr);
			gr.MoveTo(new PointD (xr, 0.0));
			gr.Arc(0, 0, xr, 0, 2 * M_PI);
			gr.ClosePath();
		}

		static void draw(Cairo.Context gr, int width, int height, Cairo.Color color)
		{
			double radius = width/2; 
										
			double subradius = radius * (2 / 3.0);
			
			gr.Color = color;
			
			oval_path(gr, width/2, height/2, subradius, subradius);
					
			gr.Fill();
			gr.Stroke();
		}
	}
}
