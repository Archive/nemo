using System;
using Gtk;

namespace Nemo
{
	public partial class Indexing : Gtk.Dialog
	{
		public Indexing()
		{
			this.Build();
			this.Hide(); // not show the on creation, since we can add text before we show the screen 

			buffer = new Gtk.TextBuffer(new TextTagTable());
			log_text_view.Buffer = buffer;
		}

		Gtk.TextBuffer buffer;

		public void add_text(string text)
		{
			buffer.Insert(buffer.StartIter, text);
		}

		public void change_status(bool status)
		{
			if (status)
				this.indexer_status.Text = "running";
			else
				this.indexer_status.Text = "idle";
		}

		protected virtual void OnOkClicked (object sender, System.EventArgs e)
		{
			this.Hide();
		}

		protected virtual void OnFullIndexClick (object sender, System.EventArgs e)
		{
			Singleton<SingletonWrapper<Broker>>.Instance.wrapped.reindex_metadata_store();
		}
	}
}
