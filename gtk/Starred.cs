using System;
using Gtk;

namespace Nemo
{
	public class Starred
	{
		Gtk.ToggleButton starred;
		
		public Starred(Gtk.ToggleButton button)
		{
			starred = button;
			starred.Active = false;
		}

		public bool active()
		{
			return starred.Active;
		}
		
		public Gtk.Button CreateButton()
		{
			starred.Active = false;
			return new Gtk.Button(new Gtk.Image(new Gdk.Pixbuf(null, "stock_about")));
		}
		
		public void Reset()
		{
			starred.Active = false;
		}
	}
}
