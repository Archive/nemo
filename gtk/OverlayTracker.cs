using Gtk;

using System;
using System.Collections.Generic;

namespace Nemo
{
	public abstract class Overlay
	{
		public abstract void hide();
		public abstract void show();
		// x = vertical shift, y = horizontal shift
		public abstract void shift(int x, int y);
		public abstract void resize();
	
		public string name;
		protected int x;
		protected int y;
	}

	sealed class OverlayTracker
	{
		private List<Overlay> overlays;

		// the function used to add make overlaytracker handle the overlay
		public void add_overlay_and_show(Overlay overlay)
		{
			if (String.IsNullOrEmpty(overlay.name))
				throw new Exception("name for an overlay cannot be empty");
		
			hide_and_die(overlay.name);

			overlays.Add(overlay);
		
			overlay.show();
		}
		
		public void hide_all()
		{
			foreach (Overlay overlay in overlays)
				overlay.hide();
		}

		public void hide_and_die(string name)
		{
			int overlays_before = overlays.Count;

			foreach (Overlay overlay in overlays) {
				if (overlay.name == name) {
					overlay.hide();
					overlays.Remove(overlay);
					break;
				}
			}

			// überhack
			if (overlays.Count > 0 && overlays.Count != overlays_before && name != "preview_popup")
				GLib.Timeout.Add(100, delegate { 
					hide_all(); 
					show_all(); 
					return false; 
				});
		}

		public void hide_all_and_die()
		{
			foreach (Overlay overlay in overlays) {
				System.Console.WriteLine("hiding {0}", overlay.name);
				overlay.hide();
			}

			overlays.Clear();
		}

		public void show_all()
		{
			foreach (Overlay overlay in overlays)
				overlay.show();
		}

		public void resize_all()
		{
			foreach (Overlay overlay in overlays)
				overlay.resize();
		}

		public void shift_all(int shift_x, int shift_y)
		{
			foreach (Overlay overlay in overlays)
				overlay.shift(shift_x, shift_y);
		}
	
        private OverlayTracker() 
        {
        	overlays = new List<Overlay>();
        }
	}
}