using System;
using System.Collections.Generic;

using Gtk;

namespace Nemo
{
	public class DisplayDocumentItem : DisplayItem
	{
		protected Label snippet;
		protected Gtk.EventBox snippet_wrapper;
		
		private string pages;
		private string title;
	
		public DisplayDocumentItem(string pages, string title, Item item)
		   : base(item)
		{
			this.pages = pages;
			this.title = title;
		}
		
		public override Gtk.Frame get_representation()
		{
			List<KeyValuePair<string, string>> elements = new List<KeyValuePair<string, string>>();
			if (title.Length != 0)
				elements.Add(new KeyValuePair<string, string>(Mono.Unix.Catalog.GetString("Title:"), title));
			elements.Add(new KeyValuePair<string, string>(Mono.Unix.Catalog.GetString("Pages:"), pages));

			return get_representation(elements);
		}
		
		public void set_snippet(string snippet)
		{
			snippet_wrapper = new Gtk.EventBox();
			Gtk.Label tmp = new Gtk.Label();
			tmp.Markup = snippet;
			snippet_wrapper.Add(tmp);
		}

		public void set_snippet_tip(string text_snippet)
		{
			GtkCommon.tooltip.SetTip(snippet_wrapper, " " + text_snippet + " ", " " + text_snippet + " ");
		}
	}
}