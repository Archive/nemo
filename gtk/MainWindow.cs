using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using Gtk;
using GLib;

using Mono.Unix;

using Nemo; // FIXME: not working 100%?

public partial class MainWindow: Gtk.Window
{
	// helper classes
	private Tray tray;

	// restrictions
	CalendarDriver calendar_driver;
	
	bool starred_button_active;
	
	private Broker broker;
	
	public MainWindow(bool reindex, bool hidden): base ("")
	{
		try {
			OSSpecific.SetProcessName ("nemo");
		} catch (Exception e) {
			Console.Error.WriteLine ("Failed to set process name: {0}", e.Message);
		}
		
		// gettext
		Mono.Unix.Catalog.Init("nemo", "/usr/share/locale");

		if (Environment.GetEnvironmentVariable("MONO_MANAGED_WATCHER") == "1")
		{
			System.Console.WriteLine(Mono.Unix.Catalog.GetString("MONO_MANAGED_WATCHER is buggy, please don't use that."));
			throw new Exception("die");
		}
	
		Build();

		if (hidden)
			hide();

		// restore the position on screen
		Move(Singleton<Configuration>.Instance.data.main_window_x,
		     Singleton<Configuration>.Instance.data.main_window_y);
		
		this.SetSizeRequest(640, 480); // min size

		this.Resize(Singleton<Configuration>.Instance.data.main_window_width,
		            Singleton<Configuration>.Instance.data.main_window_height);

		this.AddMnemonic('s', this.search_input);

		// hide while moving
		this.FocusOutEvent += delegate(object sender, Gtk.FocusOutEventArgs args) {
			Nemo.Singleton<Nemo.OverlayTracker>.Instance.hide_all(); 
		};

		this.FocusInEvent += delegate(object sender, Gtk.FocusInEventArgs args) { 
			Nemo.Singleton<Nemo.OverlayTracker>.Instance.show_all(); 
		};

		this.DeleteEvent += on_close;

		GtkCommon.tooltip = new Gtk.Tooltips();

		if (Singleton<Configuration>.Instance.data.search_tool == "tracker")
			broker = new Tracker(reindex);
		else if (Singleton<Configuration>.Instance.data.search_tool == "xesam")
			broker = new Xesam(reindex);
		else {
			System.Console.WriteLine("Unknown search tool token found in configuration: {0}, falling back to tracker", Singleton<Configuration>.Instance.data.search_tool);
			broker = new Tracker(reindex);
		}

		Singleton<SingletonWrapper<Broker>>.Instance.wrapped = broker;

		System.Console.WriteLine("search tool is: {0}", Singleton<Configuration>.Instance.data.search_tool);
		
		// tray
		tray = new Tray(this);
		tray.main_quit = delegate {
			broker.stop();
		};

		// helpers
		starred_button_active = false;

		Singleton<Categories>.Instance.set_drawer(new CategoriesDrawer(categories_event_box));
		calendar_driver = new CalendarDriver(new CalendarDrawer(calendar_event_box, day_button, week_button, month_button, 
																year_button, today_button, current_date));
		
		// gnome vfs
		Gnome.Vfs.Vfs.Initialize();

		search_input.AddEvents((int)Gdk.EventMask.KeyReleaseMask);

		// update callbacks
		Singleton<Categories>.Instance.set_search_func(do_search);

		Singleton<TypeLabels>.Instance.update_func = update_type_labels;
		Singleton<TypeLabels>.Instance.search_func = do_search;

		calendar_driver.search_func = do_search;
		calendar_driver.set_active_view = set_active_view;

		System.Console.WriteLine("doing initial search");
		
		// register result types
		DocumentItem.set_fields_index(broker.RegisterItemTypes(DocumentItem.ItemTypes()));
		PictureItem.set_fields_index(broker.RegisterItemTypes(PictureItem.ItemTypes()));
	}

	// Tray
	public void hide()
	{
		Ref();
		Hide();

		Singleton<OverlayTracker>.Instance.hide_all();
	}

	// callbacks
	protected void OnDeleteEvent(object sender, DeleteEventArgs a)
	{
		tray.hide_window();
		a.RetVal = true;
	}

	protected virtual void OnPrevious(object sender, System.EventArgs e)
	{
		calendar_driver.prev();
	}

	protected virtual void OnNext(object sender, System.EventArgs e)
	{
		calendar_driver.next();
	}

	protected void set_active_view(CalendarDriver.View view)
	{
		if (view == CalendarDriver.View.Month)
			month_button.Active = true;
		else if (view == CalendarDriver.View.Week)
			week_button.Active = true;
		else if (view == CalendarDriver.View.Day)
			day_button.Active = true;
	}

	protected virtual void OnDayClick(object sender, System.EventArgs e)
	{
		calendar_driver.set_day_view();
		day_button.Active = false;
	}

	protected virtual void OnWeekClick(object sender, System.EventArgs e)
	{
		calendar_driver.set_week_view();
		week_button.Active = false;
	}

	protected virtual void OnMonthClick(object sender, System.EventArgs e)
	{
		calendar_driver.set_month_view();
		month_button.Active = false;
	}

	protected virtual void OnYearClick(object sender, System.EventArgs e)
	{
		calendar_driver.set_year_view();
		year_button.Active = false;
	}

    protected void on_key_release(object sender, Gtk.KeyReleaseEventArgs e)
    {
    	bool quick_search = false;
    
    	if (e.Event.Key == Gdk.Key.Return || e.Event.Key == Gdk.Key.KP_Enter)
    		quick_search = true;
    	else if (e.Event.Key == Gdk.Key.Escape) 
    	{
    		Singleton<OverlayTracker>.Instance.hide_all_and_die();
    		return;
    	}
    	else if (e.Event.Key == Gdk.Key.Alt_L || e.Event.Key == Gdk.Key.Alt_R ||
    			 e.Event.Key == Gdk.Key.Shift_L || e.Event.Key == Gdk.Key.Shift_R ||
    			 e.Event.Key == Gdk.Key.Control_L || e.Event.Key == Gdk.Key.Control_R)
    		return; // skip input that doesn't change the search word
    	
		do_search_text(quick_search);
    }
	
	protected void on_search_button_click(object sender, System.EventArgs e)
	{
		do_search_text(true);
	}

	string last_search_text;
	bool search_result_valid;

    protected void do_search_text(bool quick_search)
    {
    	if (String.IsNullOrEmpty(search_input.Text))
    	{
			Singleton<OverlayTracker>.Instance.hide_and_die("search_popup");
    		search_result_valid = false;
    		return;
    	}
    	
    	if (last_search_text == search_input.Text && !quick_search)
    		return;
    
    	last_search_text = search_input.Text;
    	search_result_valid = true;

    	System.Console.WriteLine("searching using text: {0}", search_input.Text);
    
		broker.SearchForText(search_input.Text, quick_search, 
			Helpers.RunInMainThread<List<Tuple<string[], File>>>(delegate (List<Tuple<string[], File>> search_results) {  
				Nemo.Singleton<Nemo.OverlayTracker>.Instance.hide_and_die("search_popup");
				on_search_text_results(search_results, search_input.Text);
			})
		);
	}

    protected void do_search()
	{
		Tuple<DateTime, DateTime> dates = calendar_driver.restriction();

		List<long> types = Singleton<TypeLabels>.Instance.restrictions();
        List<MetaLabel> keywords = Singleton<Categories>.Instance.restrictions(); 

		System.Console.WriteLine("searching for results: {0}, {1} keywords, {2} types", search_input.Text, keywords.Count, types.Count);
		
		broker.Search(keywords, types, dates.first, dates.second, starred_button_active, 
					  Helpers.RunInMainThread<List<File>>(on_search_results));
    }

	protected void on_search_results(List<File> search_results)
	{
		System.Console.WriteLine("got results {0}", search_results.Count);
		
		Singleton<OverlayTracker>.Instance.hide_all_and_die();

		List<ItemWrapper> items = new List<ItemWrapper>();

		List<string> files = new List<string>();
		Dictionary<string, VoidFunction<string[]>> callbacks = new Dictionary<string, VoidFunction<string[]>>(); 

		foreach (File file in search_results) 
		{
			string path = "file://" + file.path;
			
			if (callbacks.ContainsKey(path)) {
				continue;
			}
			
			ItemWrapper wrapper = new ItemWrapper(new Nemo.Item(file));
			
			files.Add(file.path);
			
			callbacks.Add(path, Helpers.RunInMainThread<string[]>(delegate(string[] result) {
				
//				System.Console.WriteLine("results: {0}", result.Length);
				System.Console.WriteLine("result: {0} for {1}", result[0], path);

				if (Nemo.DocumentItem.is_document(result[0]))
				{
					Nemo.VoidFunction data_callback = wrapper.item.on_data_callback;
					Nemo.VoidFunction label_callback = wrapper.item.on_labels_callback;
					Nemo.VoidFunction<bool> small_starred_update = wrapper.item.small_starred_update_function;
					
					// in case something was changed before we got data from tracker
					wrapper.item.file.labels.Clear();
					foreach (Nemo.UserLabel l in wrapper.item.labels)
						wrapper.item.file.labels.Add(l.metalabel);

					wrapper.item = new Nemo.DocumentItem(wrapper.item.file, result, search_input.Text); 

					if (data_callback != null) {
						wrapper.item.on_data_callback = data_callback;
						wrapper.item.on_data_callback();
					}
					
					if (label_callback != null)
						wrapper.item.on_labels_callback = label_callback;

					if (small_starred_update != null)
					    wrapper.item.small_starred_update_function = small_starred_update;
				} 
				else if (Nemo.PictureItem.is_image(result[0])) 
				{
					Nemo.VoidFunction data_callback = wrapper.item.on_data_callback;
					Nemo.VoidFunction label_callback = wrapper.item.on_labels_callback;
					Nemo.VoidFunction<bool> small_starred_update = wrapper.item.small_starred_update_function;

					// in case something was changed before we got data from tracker
					wrapper.item.file.labels.Clear();
					foreach (Nemo.UserLabel l in wrapper.item.labels)
						wrapper.item.file.labels.Add(l.metalabel);

					wrapper.item = new Nemo.PictureItem(wrapper.item.file, result); 

					if (data_callback != null) {
						wrapper.item.on_data_callback = data_callback;
						wrapper.item.on_data_callback();
					}
					
					if (label_callback != null)
						wrapper.item.on_labels_callback = label_callback;

					if (small_starred_update != null)
					    wrapper.item.small_starred_update_function = small_starred_update;
				} else 
					wrapper.item.mime_type = result[0];
			}));

			wrapper.item.search_func = do_search;

			items.Add(wrapper);
		}

		broker.get_metadata(files, callbacks); 

		calendar_driver.new_elements_in_view(items);
    }

	protected void on_search_text_results(List<Tuple<string[], File>> search_results, string search_text)
	{
		if (!search_result_valid)
			return;
	
		System.Console.WriteLine("got search text results {0}", search_results.Count);

		List<Nemo.Item> items = new List<Nemo.Item>();

//		int height_left = calendar_event_box.Allocation.Height;

		foreach (Tuple<string[], File> result in search_results) 
		{
			Nemo.Item item;
			
			System.Console.WriteLine("type {0}", result.first[0]);
			
			if (DocumentItem.is_document(result.first[0]))
				item = new DocumentItem(result.second, result.first, search_input.Text);
			else if (PictureItem.is_image(result.first[0])) 
				item = new PictureItem(result.second, result.first);
			else 
			{
				item = new Nemo.Item(result.second);
				item.mime_type = result.first[0];
			}
			
			item.search_func = do_search;

			items.Add(item);
		}

//		System.Console.WriteLine("{0} - {1} - {2} - {3}", root_x, root_y, search_input.Allocation.Width, search_input.Allocation.Height);

		SearchPopup popup = new SearchPopup(calendar_driver.update_view_set_next);
		popup.get_size = delegate { return calendar_event_box.Allocation.Height; };
		popup.calculate_start_position = delegate {
			int root_x = 0, root_y = 0;
			calendar_event_box.GdkWindow.GetOrigin(out root_x, out root_y);

			int tmp = 0; // throw away
			search_input.GdkWindow.GetOrigin(out root_x, out tmp);
			
			return new Nemo.Tuple<int, int>(root_x + search_input.Allocation.Width, root_y + calendar_event_box.Allocation.Height);
		};

		popup.set_files_and_show(items, search_text);

		Singleton<OverlayTracker>.Instance.add_overlay_and_show(popup);
	}

	void update_type_labels(List<TypeLabel> labels)
	{
		foreach (Widget w in type_labels.Children) 
			w.Destroy(); 

		foreach (TypeLabel label in labels) 
		{
			Gtk.ToggleButton b = label.representation(Singleton<TypeLabels>.Instance);

       		type_labels.PackStart(b, false, false, 0);
		}
		
		type_labels.ShowAll();
	}

	int move_from_x, move_from_y, width_before, height_before;

	protected virtual void GotFocus (object o, Gtk.FocusInEventArgs args)
	{
		int move_to_x, move_to_y, width, height;

		args.Event.Window.GetPosition(out move_to_x, out move_to_y);
		args.Event.Window.GetSize(out width, out height);

		if (height_before != height || width_before != width)
			Singleton<OverlayTracker>.Instance.resize_all();

		if (move_from_x != move_to_x || move_from_y != move_to_y)
			Singleton<OverlayTracker>.Instance.shift_all(move_to_x - move_from_x, move_to_y - move_from_y);
	}

	protected virtual void FocusLeave (object o, Gtk.FocusOutEventArgs args)
	{
		args.Event.Window.GetPosition(out move_from_x, out move_from_y);
		args.Event.Window.GetSize(out width_before, out height_before);
	}

	protected virtual void on_close (object o, Gtk.DeleteEventArgs args)
	{
		tray.hide_window();
 		args.RetVal = true;
	}

	protected virtual void OnTodayPressed (object sender, System.EventArgs e)
	{
		System.Console.WriteLine("pressed today");
		calendar_driver.set_date_to_today();
	}

	protected virtual void OnAllFilesPressed (object sender, System.EventArgs e)
	{
		search_input.Text = String.Empty;

		Singleton<TypeLabels>.Instance.clear_restrictions();
		Singleton<Categories>.Instance.clear_restrictions(); 

		do_search();
	}

	protected virtual void OnStarredPressed (object sender, System.EventArgs e)
	{
		starred_button_active = !starred_button_active;
		do_search();
	}

	protected virtual void MainKeyRelease (object o, Gtk.KeyReleaseEventArgs args)
	{
    	if (args.Event.Key == Gdk.Key.Escape) 
    		Singleton<OverlayTracker>.Instance.hide_all_and_die();
	}
}
