using System;
using System.Collections.Generic;

using Gtk;

using Gnome;
using Gnome.Vfs;

using Mono.Unix;
using System.IO;

using System.Diagnostics; // for opening files

namespace Nemo
{
	public class DisplayItem
	{
		Gtk.EventBox picture_wrapper;
		protected Gtk.Image picture;
		protected string pic_path;

//		protected Gtk.Label filename;
		protected Gtk.HBox labels;
		
		protected Gtk.Label size;
		protected Gtk.Label accessed;
		protected Gtk.Label modified;

		protected Gtk.Table extra_info;
		
		protected Gtk.VBox box;
		Gtk.EventBox box_wrapper;

		private Item item;
		
		// outer frame used by main window
		protected Gtk.Frame representation;
		
		public virtual Gtk.Frame get_representation(List<KeyValuePair<string, string>> new_fields)
		{
			create_representation(new_fields);
			return representation;
		}

		public virtual Gtk.Frame get_representation()
		{
			create_representation(new List<KeyValuePair<string, string>>());
			return representation;
		}

		uint row;

		protected void add_line(string type, string type_value)
		{
			Gtk.Alignment left_alignment = new Gtk.Alignment(1f, 0.5f, 1f, 0.5f);
			Gtk.Label left_text = new Gtk.Label();
			left_text.SetAlignment(1, 0); // right aligned
			left_text.Text = type;
			left_alignment.RightPadding = 5;
			left_alignment.Add(left_text);
			
			extra_info.Attach(left_alignment, 0, 1, row, row+1);
			
			Gtk.Label right_text = new Gtk.Label();
			right_text.SetAlignment(0, 0); // left aligned
			right_text.Text = type_value;
			
			extra_info.Attach(right_text, 1, 2, row, row+1);
			
			row += 1;
		}
		
		protected void add_line_updateable(string type, string type_value, Gtk.Label right_text)
		{
			Gtk.Alignment left_alignment = new Gtk.Alignment(0.5f, 0.5f, 0.5f, 0.5f);
			Gtk.Label left_text = new Gtk.Label();
			left_text.SetAlignment(1, 0); // right aligned
			left_text.Text = type;
			left_alignment.RightPadding = 5;
			left_alignment.Add(left_text);
			
			extra_info.Attach(left_alignment, 0, 1, row, row+1);
			
			right_text.SetAlignment(0, 0); // left aligned
			right_text.Text = type_value;
			
			extra_info.Attach(right_text, 1, 2, row, row+1);
			
			row += 1;
		}

		protected void create_representation(List<KeyValuePair<string, string>> new_fields)
		{
			box = new VBox();

			set_picture_path();

			try {
			    if (item.file.starred)
				picture = CairoDrawing.create_big_starred_image(pic_path);
			    else
				picture = CairoDrawing.create_image_with_border(pic_path);
			} catch (Exception e) {
			    System.Console.WriteLine("failed to generate thumbnail, error: {0}", e.Message);
			}

			Gtk.Table picture_resizer = new Table(1,1, true); // for size
			picture_wrapper = new EventBox(); // for events
			GtkCommon.set_background_color(picture_wrapper, "white");
			picture_wrapper.Add(picture);
			picture_resizer.Attach(picture_wrapper, 0, 1, 0, 1, AttachOptions.Shrink, AttachOptions.Shrink, 0, 0);

			Gtk.Alignment picture_alignment = new Gtk.Alignment(0.5f, 0.5f, 0, 0);

			picture_alignment.Add(picture_resizer);
			
			GtkCommon.show_hand_and_tooltip(picture_wrapper, Mono.Unix.Catalog.GetString("Open"));

			box.PackStart(picture_alignment, true, true, 3);

			string image = Singleton<Types>.Instance.get_type((int)item.file.type.category).icon;
			if (!String.IsNullOrEmpty(image)) 
			{
				Gtk.HBox tmp = new Gtk.HBox();

				Gtk.Alignment img_alignment = new Gtk.Alignment(1F, 0.5F, 0F, 0.5F);
				img_alignment.RightPadding = 5;
				Gtk.Image img = new Gtk.Image(null, image);
				img_alignment.Add(img);

				tmp.PackStart(img_alignment, true, true, 0); 					
				
				Gtk.Alignment label_alignment = new Gtk.Alignment(0F, 0.5F, 0F, 0.5F);
				Gtk.Label tmp_label = new Gtk.Label();
				tmp_label.Markup = "<b>" + item.name(35) + "</b>";
				tmp_label.UseUnderline = false;
				label_alignment.Add(tmp_label);
				tmp.PackStart(label_alignment, true, true, 0);

				box.PackStart(tmp, false, true, 3);

			} else {

				Gtk.Label title = new Gtk.Label();
				title.Markup = "<b>" + item.name(35) + "</b>";
			
				box.PackStart(title, true, true, 3);
			}

			Gtk.Alignment labels_alignment = new Gtk.Alignment(0.5f, 0.5f, 0f, 0f);

			labels = new Gtk.HBox();

			create_labels();

			labels_alignment.Add(labels);

			box.PackStart(labels_alignment, false, false, 0);

			extra_info = new Gtk.Table((uint)(3 + new_fields.Count), 2, false);
			
			row = 0;

			foreach (KeyValuePair<string, string> kvp in new_fields)
				add_line(kvp.Key, kvp.Value);

			string path = item.path;
			
			string homedir = Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
			
			if (path.StartsWith(homedir))
				path = path.Replace(homedir, "~");
			
			int index = -1;
			if ((index = path.LastIndexOf('/')) != -1)
				if (index > 27) {
					path = path.Substring(0, 25);
					path += "...";
				} else
					path = path.Substring(0, index+1);
			
			add_line(Mono.Unix.Catalog.GetString("Path:"), path);
			add_line(Mono.Unix.Catalog.GetString("Size:"), item.size);
			add_line(Mono.Unix.Catalog.GetString("Last modified:"), item.accessed);
			add_line(Mono.Unix.Catalog.GetString("Last accessed:"), item.modify);
   	   		
			box.PackStart(extra_info, false, false, 3);

			Gtk.Alignment box_alignment = new Alignment(0.5f, 1f, 0, 0);
			box_alignment.BottomPadding = 12;
			box_alignment.TopPadding = 12;
			box_alignment.LeftPadding = 12;
			box_alignment.RightPadding = 12;

			box_alignment.Add(box);

			box_wrapper = new EventBox(); // for bg color
			box_wrapper.Add(box_alignment);
			GtkCommon.set_background_color(box_wrapper, "white");

			box_wrapper.ButtonPressEvent += GtkCommon.ButtonPressEventWrapper(item);
			
			representation = new Frame();
			representation.Add(box_wrapper);
			
			representation.ShowAll();
		}
		
		public void update_starred(bool starred)
		{
			if (picture == null)
				set_picture_path(); // path
			
			if (picture_wrapper != null)
				foreach (Widget w in picture_wrapper.Children)
					w.Destroy();
			
			if (item.file.starred)
				picture = CairoDrawing.create_big_starred_image(pic_path);
			else
				picture = CairoDrawing.create_image_with_border(pic_path);
				
			if (picture_wrapper != null)
				picture_wrapper.Add(picture);

			if (representation != null)
				representation.ShowAll();
		}
		
		public DisplayItem(Item item)
		{
			this.item = item;
		}

		// set up functions

		private List<UserLabel> user_labels;

		public void set_labels()
		{
			this.user_labels = item.labels;
			
			if (representation != null) 
			{
				create_labels();
				labels.ShowAll();
			}
		}

		private void create_labels()
		{
			foreach (Widget w in labels.Children)
				w.Destroy();
		
			foreach (UserLabel label in user_labels) 
			{
				Gtk.HBox tmp = new HBox();

				Gtk.Image img = label.dot();
				Gtk.Alignment img_alignment = new Gtk.Alignment(0.5f, 0.5f, 0.5f, 0.5f);
				img_alignment.RightPadding = 3;
				img_alignment.Add(img);

				tmp.Add(img_alignment);

				Gtk.Label l = new Gtk.Label(label.metalabel.label);
				GtkCommon.set_foreground_color(l, new Gdk.Color(label.metalabel.color.r, label.metalabel.color.g, label.metalabel.color.b));

				tmp.Add(l);
				
				labels.PackStart(tmp, true, false, 5);
			}
		}
	
 		public void set_picture_path()
		{
			string uri = Gnome.Vfs.Uri.GetUriFromLocalPath(item.path);
		
			string image_uri = Gnome.Thumbnail.PathForUri(uri, Gnome.ThumbnailSize.Normal);
			if (System.IO.File.Exists(image_uri))
			{
				pic_path = image_uri;
			} 
			else
			{
				using (Gnome.ThumbnailFactory factory = new Gnome.ThumbnailFactory(Gnome.ThumbnailSize.Normal)) {

					System.DateTime mtime = System.IO.File.GetLastWriteTime(item.path);

					using (Gnome.Vfs.Uri vfs_uri = new Gnome.Vfs.Uri(item.path)) {
						string mime = vfs_uri.MimeType.Name;

						if (factory.HasValidFailedThumbnail(uri, mtime)) {
							pic_path = "/usr/share/nemo/no-preview.png";
							return; 
						}

						System.Console.WriteLine("trying to thumbnail {0}", item.path);

						System.Console.WriteLine(mime);

						if (factory.CanThumbnail(uri, mime, mtime)) 
						{
							Gdk.Pixbuf thumb = factory.GenerateThumbnail(uri, mime);
							if (thumb == null) 
							{
								System.Console.WriteLine("failed to generate thumbnail");
								factory.CreateFailedThumbnail(uri, mtime);
								pic_path = "/usr/share/nemo/no-preview.png";
							} 
							else {
								factory.SaveThumbnail(thumb, uri, mtime);
								set_picture_path(); // use the new picture
							}
						}
						else
						{
							factory.CreateFailedThumbnail(uri, mtime);
							System.Console.WriteLine("Can't thumbnail");
							pic_path = "/usr/share/nemo/no-preview.png";
						}
					}
				}
			}
		}
	}
}
