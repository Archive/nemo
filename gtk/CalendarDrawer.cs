using System;
using System.Collections.Generic;
using Gtk;

namespace Nemo
{
	public class CalendarHeader
	{
		protected Gtk.EventBox header_wrapper;
		protected Gtk.Alignment header_alignment;
		protected Gtk.Label header;
	
		public CalendarHeader()
		{
			header = new Gtk.Label();
			header_alignment = new Gtk.Alignment(1,1,1,1);
			header_wrapper = new Gtk.EventBox();

			header_alignment.Add(header);
			header_wrapper.Add(header_alignment);

			GtkCommon.set_background_color(header_wrapper, "white");
		}
		
		public virtual Gtk.Widget representation()
		{
			return header_wrapper;		
		}
	}

	public class WeekCalendarHeader
	{
		protected Gtk.EventBox header_wrapper;
		protected Gtk.VBox header_box;
		protected Gtk.Label header;

		protected Gtk.Alignment header_border;
	
		public WeekCalendarHeader(System.DateTime date, VoidFunction<DateTime> update_view)
		{
			header = new Gtk.Label();
			header_box = new Gtk.VBox();
			header_wrapper = new Gtk.EventBox();

			header_box.PackStart(header, false, false, 6);
			header_wrapper.Add(header_box);

			GtkCommon.set_background_color(header_wrapper, "white");

            // 0 = day of week, 1 = day of month, 2 = month
			header.Text = String.Format(Mono.Unix.Catalog.GetString("{0}, {1}/{2}"), date.DayOfWeek, date.Day, date.Month);
			header_wrapper.ButtonPressEvent += delegate { update_view(date); };

			header_border = new Gtk.Alignment(1,1,1,1);
			header_border.BottomPadding = 1;
			header_border.Add(header_wrapper);

			GtkCommon.show_hand_and_tooltip(header_wrapper, String.Format(Mono.Unix.Catalog.GetString("Zoom to {0}"), header.Text));
		}

		public WeekCalendarHeader(System.DateTime start_date, System.DateTime end_date, VoidFunction<DateTime> update_view)
		{
			header = new Gtk.Label();
			header_box = new Gtk.VBox();
			header_wrapper = new Gtk.EventBox();

			header_box.PackStart(header, false, false, 6);
			header_wrapper.Add(header_box);

			GtkCommon.set_background_color(header_wrapper, "white");

			header.Text = String.Format(Mono.Unix.Catalog.GetString("Sat, {0}/{1} - Sun, {2}/{3}"), start_date.Day, start_date.Month, end_date.Day, end_date.Month); 
			header_wrapper.ButtonPressEvent += delegate { update_view(start_date); };

			header_border = new Gtk.Alignment(1,1,1,1);
			header_border.BottomPadding = 1;
			header_border.Add(header_wrapper);

			GtkCommon.show_hand_and_tooltip(header_wrapper, String.Format(Mono.Unix.Catalog.GetString("Zoom to Sat {0} {1}"), start_date.Month, start_date.Day));
		}
		
		public Gtk.Widget representation()
		{
			return header_border;
		}
	}

	public class SideWeekCalendarHeader : CalendarHeader
	{
		public SideWeekCalendarHeader(System.DateTime date, VoidFunction<DateTime> update_view)
		{
			System.Globalization.Calendar cal = new System.Globalization.GregorianCalendar();
			System.Globalization.CultureInfo culture = System.Globalization.CultureInfo.CreateSpecificCulture("da");

			int week = cal.GetWeekOfYear(date, culture.DateTimeFormat.CalendarWeekRule,
				                         culture.DateTimeFormat.FirstDayOfWeek);

			header.Text = String.Format(Mono.Unix.Catalog.GetString("Week {0}"), week);
			
			header.Angle = 90;
			header.Justify = Gtk.Justification.Center;

			header_alignment.Set(0.5f,0.10f,0f,0f);
			header_alignment.LeftPadding = 2;
			header_alignment.RightPadding = 2;

			header_wrapper.ButtonPressEvent += delegate { update_view(date); };

			GtkCommon.show_hand_and_tooltip(header_wrapper, String.Format(Mono.Unix.Catalog.GetString("Zoom to {0}"), header.Text));
		}
	}

	public class MonthCalendarHeader : CalendarHeader
	{
		public MonthCalendarHeader(System.DateTime date)
		{
			header.Text = GtkCommon.day_name(date);
		}

		public MonthCalendarHeader(System.DateTime start_date, System.DateTime end_date)
		{
			header.Text = GtkCommon.day_name(start_date) + " - "  + GtkCommon.day_name(end_date);
		}
	}

	public class CalendarElementBase
	{
		protected Gtk.Label header;
		protected EventBox header_wrapper;

		// body implemented in classes inheriting

		protected Gtk.VBox box;
		protected Gtk.Alignment box_wrapper;
		protected Gtk.EventBox box_wrapper_wrapper;

		protected VoidFunction<DateTime> update_view;

		protected void setup(bool has_header, bool small_header, Gtk.Widget body, bool is_gray, bool is_today)
		{
			box_wrapper = new Gtk.Alignment(1,1,1,1);
			box_wrapper_wrapper = new Gtk.EventBox();
			box = new VBox();

			if (has_header) {
				header = new Gtk.Label();

				header_wrapper = new EventBox(); // for bg color

				if (!small_header) {
					VBox header_box = new VBox();
					header_box.PackStart(header, false, false, 6);
					header_wrapper.Add(header_box);
					GtkCommon.set_background_color(header_wrapper,  "white");
					
					Gtk.EventBox header_alignment_event_box = new Gtk.EventBox();

					Gtk.Alignment header_alignment = new Alignment(1, 1, 1, 1);
					header_alignment.BottomPadding = 1;
					header_alignment.Add(header_wrapper);

					header_alignment_event_box.Add(header_alignment);

					GtkCommon.set_background_color(header_alignment_event_box,  "darkgray");

					box.PackStart(header_alignment_event_box, false, false, 0);
					
				} else {

					header_wrapper.Add(header);
					if (is_today)
						GtkCommon.set_background_color(header_wrapper,  "white");
					else if (!is_gray)
						GtkCommon.set_background_color(header_wrapper,  "white");

					Gtk.EventBox header_alignment_event_box = new Gtk.EventBox();

					Gtk.Alignment header_alignment = new Alignment(1, 1, 1, 1);
					header_alignment.BottomPadding = 1;
					header_alignment.Add(header_wrapper);

					header_alignment_event_box.Add(header_alignment);

					GtkCommon.set_background_color(header_alignment_event_box,  "darkgray");

					box.PackStart(header_alignment_event_box, false, false, 0);
				}
			}

			EventBox body_wrapper = new EventBox(); // for bg color
			body_wrapper.Add(body);

			if (!is_gray && !is_today)
				GtkCommon.set_background_color(body_wrapper,  "white");
			
			box.PackStart(body_wrapper, true, true, 0);
			
			box_wrapper.BorderWidth = 0;
			
			box_wrapper.Add(box);
			box_wrapper_wrapper.Add(box_wrapper);
		}

		public void set_header(string header_text, bool active, string tooltip_text)
		{
			if (active) 
			{
				header.Markup = "<span underline=\"low\">" + header_text + "</span>";
			
				GtkCommon.show_hand_and_tooltip(header_wrapper, tooltip_text);
			} 
			else
				header.Text = header_text;

			header.SetAlignment(0.95f, 0.5f);
		}

		public void set_large_header(string header_text, string tooltip_text)
		{
			header.Text = header_text;

			header.SetAlignment(0.5f, 0.5f);

			GtkCommon.show_hand_and_tooltip(header_wrapper, tooltip_text);
		}

        public Gtk.Widget representation()
        {
			return box_wrapper_wrapper;
		}
	}
	
	public class CalendarElement : CalendarElementBase
	{
		Gtk.VBox body;

		public CalendarElement(bool has_header, bool small_header)
		{
			body = new VBox();

			setup(has_header, small_header, body, false, false);
		}

		public void set_update_view(VoidFunction<DateTime> _update_view)
		{
			update_view = _update_view;
		}

		public void set_files(List<Item> files)
		{
			foreach (Item item in files) 
			{
				Item file = item;

				RightClickItemButton b = new RightClickItemButton();

				b.set_item(file);
				b.Label += file.name(30);
				string image = item.image();
				if (!String.IsNullOrEmpty(image))
					b.Image = new Gtk.Image(null, image);
				b.BorderWidth = 0;
				b.Relief = Gtk.ReliefStyle.None;
				b.SetAlignment(0, 0);

				GtkCommon.create_preview(item, b, b);

				body.PackStart(b, false, false, 0);
			}
		}
	}

	public class CalendarFixedElement : CalendarElementBase
	{
		public FixedResizeWidget body;

		public CalendarFixedElement(bool has_header, bool small_header, double spacing, Function<int[]> calendar_rectangle, bool start_at_top,
									bool is_gray, bool is_today)
		{
			body = new FixedResizeWidget(spacing, start_at_top);
			body.calendar_rectangle = calendar_rectangle;

			setup(has_header, small_header, body, is_gray, is_today);
			
			update_view_set_next = null;
		}

		VoidFunction<CalendarDriver.View, DateTime> update_view_set_next;

		public void set_update_view(VoidFunction<DateTime> _update_view, VoidFunction<CalendarDriver.View, DateTime> _update_view_set_next)
		{
			update_view = _update_view;
			update_view_set_next = _update_view_set_next;
			body.update_view = _update_view;
			body.update_view_set_next = _update_view_set_next;
		}

		public void set_dates(DateTime start, DateTime stop, DateTime header_start_date, CalendarDriver.View next)
		{
			body.view_period = stop - start;
			body.view_period_start = start;

			if (header_wrapper != null) { // has header

				if (update_view_set_next == null) // sanity check
					throw new Exception("Error: update view set next not set before calling set_dates");

				header_wrapper_handler = delegate { update_view_set_next(next, header_start_date); };
				header_wrapper.ButtonPressEvent += header_wrapper_handler;
			}
		}

		Gtk.ButtonPressEventHandler header_wrapper_handler;
					
		public void reset()
		{
			if (header_wrapper_handler != null) {
				header_wrapper.ButtonPressEvent -= header_wrapper_handler;
				header_wrapper_handler = null;
			}
		}
		
		public void set_files(List<ItemWrapper> files)
		{
			body.items = files;
		}
	}

	public class CalendarDrawer
	{
		private Gtk.EventBox calendar_event_box;

		private Gtk.ToggleButton day_button;
		private Gtk.ToggleButton week_button;
		private Gtk.ToggleButton month_button;
		private Gtk.ToggleButton year_button;
		private Gtk.Button today_button;
		private Gtk.Label current_date;

		private	System.Globalization.Calendar cal;
		private System.Globalization.CultureInfo culture;
		
		public VoidFunction<DateTime> update_view;
		public VoidFunction<CalendarDriver.View, DateTime> update_view_set_next;
		
		public CalendarDrawer(Gtk.EventBox event_box, Gtk.ToggleButton day, Gtk.ToggleButton week, Gtk.ToggleButton month, 
							  Gtk.ToggleButton year, Gtk.Button today, Gtk.Label date)
		{
			calendar_event_box = event_box;
		
			day_button = day;
			week_button = week;
			month_button = month;
			year_button = year;

			today_button = today;

			current_date = date;

			month_button.Active = true;

			cal = new System.Globalization.GregorianCalendar();
			culture = System.Globalization.CultureInfo.CreateSpecificCulture("da");
			
			day_offset = 0;
		}

		int[] size()
		{
			int root_x = 0, root_y = 0;
			//calendar_event_box.GdkWindow.GetRootOrigin(out root_x, out root_y);
			calendar_event_box.GdkWindow.GetOrigin(out root_x, out root_y);
			return new int[] { root_x, root_y, calendar_event_box.Allocation.Width, calendar_event_box.Allocation.Height };
		}

		Gtk.Table calendar_body;
		List<CalendarFixedElement> calendar_elements = new List<CalendarFixedElement>();
		
		int day_offset;
		int days_per_page = 30;
		
		List<ItemWrapper> last_elements;
		
		private void draw_day(Gtk.VBox calendar, CalendarDriver.State state, System.DateTime centered_date, List<ItemWrapper> elements)
		{
			Gtk.Viewport viewport = new Gtk.Viewport();

			Gtk.VBox viewbox = new VBox();
			viewbox.Add(calendar_body);
			
			viewport.Add(viewbox);
			
			Gtk.ScrolledWindow scrolled = new Gtk.ScrolledWindow();
			scrolled.Add(viewport);
			
			calendar.PackStart(scrolled, true, true, 0);

			today_button.Label = "Today";
			current_date.Markup = "<b>" + String.Format("{0:dddd} {0:dd/MM/yy}", centered_date) + "</b>";

			Gdk.Rectangle last_allocation = new Gdk.Rectangle();
			
			foreach (Widget w in calendar_body.Children)
				w.Destroy();

			uint cols = 3;

			if (calendar_event_box.Allocation.Width >= 350)
				cols = (uint)calendar_event_box.Allocation.Width/350;
			
			uint rows;
			
			if (elements.Count >= days_per_page) {
				int elements_in_view = days_per_page;
				if ((day_offset+1)*days_per_page > elements.Count) // last page
					elements_in_view = elements.Count - day_offset*days_per_page;
				rows = (uint)Math.Ceiling((elements_in_view*1.0)/cols);
			} else
				rows = (uint)Math.Ceiling((elements.Count*1.0)/cols);

			System.Console.WriteLine("elements: {0}, day_offset {1}", elements.Count, day_offset);
			System.Console.WriteLine("cols {0} - rows {1}", cols, rows);
			System.Console.WriteLine("width {0}, height {1}", calendar_event_box.Allocation.Width, calendar_event_box.Allocation.Height);

			calendar_body.Resize(rows, cols);
		
			uint col = 0, row = 0; 

			int element = 0;
			
			// fill in elements
			foreach (ItemWrapper wrapper in elements)
			{
				if (!(element >= day_offset*days_per_page && element < (day_offset+1)*days_per_page)) {
					++element;
					continue;
				}

				uint tmp_col = col;
				uint tmp_row = row;
				ItemWrapper tmp_wrapper = wrapper; // oh lambda bug, we love ye
		
				wrapper.item.on_data_callback = delegate {
					calendar_body.Attach(tmp_wrapper.item.Display().get_representation(), tmp_col, tmp_col+1, tmp_row, tmp_row+1, 
							     	     AttachOptions.Shrink | AttachOptions.Fill | AttachOptions.Expand, 
										 AttachOptions.Shrink | AttachOptions.Fill,
										 0,0);
				};
				
				wrapper.item.on_data_callback();

	            col = ++col % cols;
	            if (col == 0)
	                ++row;
				
				++element;
			}

			if (elements.Count > days_per_page)
			{
				Gtk.HBox page_navigation = new Gtk.HBox();
				page_navigation.Homogeneous = true;
				
				if (day_offset > 0) {
					// prev button
					Gtk.EventBox link_wrapper = new Gtk.EventBox();
					Gtk.Alignment alignment = new Alignment(1,1,1,1);
					alignment.BorderWidth = 15;
					Gtk.Label link = new Gtk.Label();
					string text = Mono.Posix.Catalog.GetString("Previous page");
					link.Markup = "<u>" + text + String.Format(" ({0}/{1})", day_offset, Math.Ceiling(elements.Count*1.0/days_per_page)) + "</u>";
	
					GtkCommon.show_hand_and_tooltip(link_wrapper, text);
	
					link_wrapper.Add(alignment);
					alignment.Add(link);
					link_wrapper.ButtonPressEvent += delegate {
						day_offset -= 1;
						draw(state, centered_date, elements);
					};
					
					GtkCommon.set_background_color(link_wrapper, "white");
					page_navigation.PackStart(link_wrapper, false, true, 0);
				}
				
				if ((day_offset+1)*days_per_page <= elements.Count)
				{
					// next button
					Gtk.EventBox link_wrapper = new Gtk.EventBox();
					Gtk.Alignment alignment = new Alignment(1,1,1,1);
					alignment.BorderWidth = 15;
					Gtk.Label link = new Gtk.Label();
					string text = Mono.Posix.Catalog.GetString("Next page");
					link.Markup = "<u>" + text + String.Format(" ({0}/{1})", day_offset+2, Math.Ceiling(elements.Count*1.0/days_per_page))  + "</u>";
	
					GtkCommon.show_hand_and_tooltip(link_wrapper, text);
	
					link_wrapper.Add(alignment);
					alignment.Add(link);
					link_wrapper.ButtonPressEvent += delegate {
						day_offset += 1;
						draw(state, centered_date, elements);
					};
					
					GtkCommon.set_background_color(link_wrapper, "white");
					page_navigation.PackStart(link_wrapper, false, true, 0);
				}

				viewbox.Add(page_navigation);					
			}
				
			if (elements.Count == 0)
			{
				Gtk.EventBox eventbox = new Gtk.EventBox();
				Gtk.Alignment alignment = new Alignment(0.5f, 0.5f, 0f, 0f);
			
				Gtk.VBox box = new Gtk.VBox();
			
				Gtk.Image img = new Gtk.Image(null, "blue_guy_med.png");
				box.PackStart(img, true, true, 10);
				
				Gtk.Label label = new Gtk.Label(Mono.Unix.Catalog.GetString("Sorry, no files for this day"));
				box.PackStart(label, true, true, 10);
				
				alignment.Add(box);
				eventbox.Add(alignment);
				
				GtkCommon.set_background_color(eventbox, "white");

				calendar_body.Resize(1, 1);
			
				calendar_body.Attach(eventbox, 0, 1, 0, 1, 
							     	 AttachOptions.Shrink | AttachOptions.Fill | AttachOptions.Expand, 
									 AttachOptions.Shrink | AttachOptions.Fill | AttachOptions.Expand, 0,0);
			}
			
			week_button.Active = false;
			month_button.Active = false;				
			year_button.Active = false;				
		}

		private void draw_week(Gtk.VBox calendar, Gtk.Table calendar_header, int days_in_week, 
							   CalendarDriver.State state, System.DateTime centered_date, List<ItemWrapper> elements)
		{
			calendar.PackStart(calendar_header, false, false, 0);
			calendar.PackStart(calendar_body, true, true, 0);

			int week = cal.GetWeekOfYear(centered_date, culture.DateTimeFormat.CalendarWeekRule,
				                         culture.DateTimeFormat.FirstDayOfWeek);

			today_button.Label = Mono.Unix.Catalog.GetString("This week");
			current_date.Markup = "<b>" + String.Format(Mono.Unix.Catalog.GetString("Week {0} - {1}"), System.Convert.ToString(week), 
													    System.Convert.ToString(centered_date.Year)) + "</b>";

			calendar_header.Resize(1, 6);
			calendar_body.Resize(3, 6);
			
			uint col = 0, row = 0; 

			System.DateTime header_date = state.start_time;

			// fill in headers
			for (int i = 0; i < days_in_week; ++i)
			{
				bool full_weekend = (days_in_week == 6 && col == 5); 

				WeekCalendarHeader header;
				
				if (full_weekend)
					header = new WeekCalendarHeader(header_date, header_date.AddDays(1), update_view);
				else
					header = new WeekCalendarHeader(header_date, update_view);

				calendar_header.Attach(header.representation(), col, col+1, row, row+1, 
					       		 	   AttachOptions.Shrink | AttachOptions.Fill | AttachOptions.Expand, 
						       		   AttachOptions.Shrink | AttachOptions.Fill | AttachOptions.Expand, 0, 0);
				
	            col = ++col;
	            if (col == 0)
	                ++row;
	                
	           header_date = header_date.AddDays(full_weekend ? 2 : 1);
			}

			col = 0; row = 0;

			// fill in periods
			for (int i = 0; i < 3*days_in_week; ++i)
			{
				CalendarFixedElement element = new CalendarFixedElement(false, false, 0.5, size, true, false, 
					System.DateTime.Now.ToShortDateString() == state.start_time.AddDays(col).ToShortDateString());
				calendar_elements.Add(element);
				
				element.set_update_view(update_view, update_view_set_next);
			
				List<ItemWrapper> filenames = new List<ItemWrapper>();

				// fixme: logic in UI

				if (days_in_week == 6 && i != 0 && (i + 1) % days_in_week == 0) {

					System.DateTime date_start = state.start_time.AddDays(col);
					if (row == 1)
						date_start = date_start.AddHours(12);
					else if (row == 2)
						date_start = date_start.AddHours(18);

					System.DateTime date_end = state.start_time.AddDays(col);
					if (row == 0)
						date_end = date_end.AddHours(12);
					else if (row == 1)
						date_end = date_end.AddHours(18);
					else if (row == 2)
						date_end = date_end.AddHours(24);

					DateTime saturday_start = date_start;
					DateTime sunday_end = date_end.AddDays(1);

					foreach (ItemWrapper wrapper in elements) 
						if (wrapper.item.last_modify > saturday_start && wrapper.item.last_modify < sunday_end)
							filenames.Add(wrapper);

					if (filenames.Count > 0) {
						element.set_dates(saturday_start, sunday_end, date_start, CalendarDriver.View.Day);
						element.set_files(filenames);
					}
					
				} else {

					System.DateTime date_start = state.start_time.AddDays(col);
					if (row == 1)
						date_start = date_start.AddHours(12);
					else if (row == 2)
						date_start = date_start.AddHours(18);

					System.DateTime date_end = state.start_time.AddDays(col);
					if (row == 0)
						date_end = date_end.AddHours(12);
					else if (row == 1)
						date_end = date_end.AddHours(18);
					else if (row == 2)
						date_end = date_end.AddHours(24);

					foreach (ItemWrapper wrapper in elements)
						if (wrapper.item.last_modify > date_start && wrapper.item.last_modify < date_end)
							filenames.Add(wrapper);

					if (filenames.Count > 0) {
						element.set_dates(date_start, date_end, date_start, CalendarDriver.View.Day);
						element.set_files(filenames);
					}
				}

				calendar_body.Attach(element.representation(), col, col+1, row, row+1, 
						       	     AttachOptions.Shrink | AttachOptions.Fill | AttachOptions.Expand, 
						     	     AttachOptions.Shrink | AttachOptions.Fill | AttachOptions.Expand, 0, 0);

	            col = ++col % (uint)days_in_week;
	            if (col == 0)
	                ++row;
			}

			Gtk.EventBox label_wrapper = new Gtk.EventBox();
			Gtk.Label label = new Gtk.Label();
			label.Markup = "<span foreground=\"darkgray\" size=\"small\">12:00</span>";
			label_wrapper.Add(label);
			GtkCommon.set_background_color(label_wrapper, "white");

			Gtk.EventBox label2_wrapper = new Gtk.EventBox();
			Gtk.Label label2 = new Gtk.Label();
			label2.Markup = "<span foreground=\"darkgray\" size=\"small\">18:00</span>";
			label2_wrapper.Add(label2);
			GtkCommon.set_background_color(label2_wrapper, "white");

//				int[] cal_size = size();
//
//				int x_pos = cal_size[0] + cal_size[2] - 33;
//
//				System.Console.WriteLine("{0}  {1} {2} {3}", cal_size[0], cal_size[1], cal_size[2], cal_size[3]);
//
//				System.Console.WriteLine("{0}  {1} {2}", x_pos, cal_size[1] + cal_size[3]/3, cal_size[1] + 2*cal_size[3]/3);

			// above line
//					TransparentWindow overlay1 = new TransparentWindow(label_wrapper, x_pos - 33, cal_size[1] + cal_size[3]/3 + 5);
//					TransparentWindow overlay2 = new TransparentWindow(label2_wrapper, x_pos - 33, cal_size[1] + 2*cal_size[3]/3 - 5);

			Function<int> x_pos_func = delegate { 
				int[] cal_size = size(); 
				return cal_size[0] + cal_size[2] - 33; 
			};

			Function<int> time_1_y_pos_func = delegate { 
				int[] cal_size = size(); 
				return cal_size[1] + cal_size[3]/3 + 13; 
			};

			Function<int> time_2_y_pos_func = delegate { 
				int[] cal_size = size(); 
				return cal_size[1] + 2*cal_size[3]/3 + 4; 
			};

			// -33?

			// on line :-)
			Singleton<OverlayTracker>.Instance.add_overlay_and_show(new TransparentWindow(label_wrapper, x_pos_func, time_1_y_pos_func, "time_overlay1"));
			Singleton<OverlayTracker>.Instance.add_overlay_and_show(new TransparentWindow(label2_wrapper, x_pos_func, time_2_y_pos_func, "time_overlay2"));
			
			day_button.Active = false;
			month_button.Active = false;
			year_button.Active = false;
		}

		private void draw_month(Gtk.VBox calendar, Gtk.Table calendar_header, int days_in_week, 
							    CalendarDriver.State state, System.DateTime centered_date, List<ItemWrapper> elements)
		{
			System.Console.WriteLine("month view");
		
			Gtk.Table calendar_side_header = new Gtk.Table(1, 1, true);
			calendar_side_header.RowSpacing = 2;
			calendar_side_header.ColumnSpacing = 2;

			Gtk.HBox calendar_middle = new Gtk.HBox(); 
			Gtk.HBox calendar_top = new Gtk.HBox();

			int days = (state.end_time - state.start_time).Days;
			int weeks = days/7;

			Gtk.Alignment side_header_alignment = new Gtk.Alignment(1, 1, 1, 1);
			side_header_alignment.RightPadding = 2;
			side_header_alignment.Add(calendar_side_header);

			calendar_middle.PackStart(side_header_alignment, false, false, 0);
			calendar_middle.PackStart(calendar_body, true, true, 0);

			Gtk.Alignment top_alignment = new Gtk.Alignment(1, 1, 1, 1);
			top_alignment.BottomPadding = 2;
			top_alignment.Add(calendar_top);

			calendar.PackStart(top_alignment, false, false, 0);
			calendar.PackStart(calendar_middle, true, true, 0);

			today_button.Label = Mono.Unix.Catalog.GetString("This month");
			current_date.Markup = "<b>" + GtkCommon.month_name(centered_date.Month) + " " + System.Convert.ToString(centered_date.Year) + "</b>";

			calendar_side_header.Resize((uint)weeks, 1);

			calendar_header.Resize(1, (uint)days_in_week);
			calendar_body.Resize((uint)weeks, (uint)days_in_week);
			
			uint col = 0, row = 0; 

			System.DateTime header_date = state.start_time;

			// fill in side headers
			for (int i = 0; i < weeks; ++i)
			{
				calendar_side_header.Attach(new SideWeekCalendarHeader(header_date, update_view).representation(), col, col+1, row, row+1, 
						       		 	    AttachOptions.Shrink | AttachOptions.Fill | AttachOptions.Expand, 
						       		 	    AttachOptions.Shrink | AttachOptions.Fill | AttachOptions.Expand, 0, 0);
				
	            ++row;
	                
	            header_date = header_date.AddDays(7);
			}

			header_date = state.start_time;

			col = 0; row = 0;

			// fill in headers
			for (int i = 0; i < days_in_week; ++i)
			{
				bool full_weekend = (days_in_week == 6 && col == 5); 
			
				MonthCalendarHeader header;
			
				if (full_weekend)
					header = new MonthCalendarHeader(header_date, header_date.AddDays(1));
				else
					header = new MonthCalendarHeader(header_date);
					
				calendar_header.Attach(header.representation(), col, col+1, row, row+1, 
						       		   AttachOptions.Shrink | AttachOptions.Fill | AttachOptions.Expand, 
						       		   AttachOptions.Shrink | AttachOptions.Fill | AttachOptions.Expand, 0, 0);
	                
	            header_date = header_date.AddDays(full_weekend ? 2 : 1);

				++col;
			}

			Gtk.EventBox tmp_wrapper = new EventBox();
			Gtk.Label tmp = new Gtk.Label();
			tmp.Markup = "<span foreground=\"white\">Week</span>";
			tmp.Angle = 90;

			Gtk.Alignment tmp_tmp_alignment = new Gtk.Alignment(1, 1, 1, 1);
			tmp_tmp_alignment.RightPadding = 4;
			tmp_tmp_alignment.Add(tmp);

			tmp_wrapper.Add(tmp_tmp_alignment);
			GtkCommon.set_background_color(tmp_wrapper, "white");

			Gtk.Alignment tmp_alignment = new Gtk.Alignment(1, 1, 1, 1);
			tmp_alignment.RightPadding = 2;
			tmp_alignment.Add(tmp_wrapper);

			calendar_top.PackStart(tmp_alignment, false, false, 0);
			calendar_top.PackStart(calendar_header, true, true, 0);

			header_date = state.start_time;

			col = 0; row = 0;

			int days_to_show = days;
			if (days_in_week == 6)
				days_to_show = days-weeks;

			// fill in days
			for (int i = 0; i < days_to_show; ++i)
			{
				bool end_of_week = (days_in_week == 6 && i != 0 && (i + 1) % days_in_week == 0);

				CalendarFixedElement element = new CalendarFixedElement(true, true, 0.5, size, true, header_date.Month != centered_date.Month, 
																		System.DateTime.Now.ToShortDateString() == header_date.ToShortDateString());
		
				calendar_elements.Add(element);

				string header_string = string.Empty;
				
				if (end_of_week)
					header_string = System.Convert.ToString(header_date.Day) + " - " + System.Convert.ToString(header_date.AddDays(1).Day);
				else
					header_string = System.Convert.ToString(header_date.Day);
				
				List<ItemWrapper> filenames = new List<ItemWrapper>();

				DateTime date_start = header_date, date_end;

				// fixme: logic in UI
				if (days_in_week == 6 && i != 0 && (i + 1) % days_in_week == 0)
					date_end = header_date.AddDays(2);
				else
					date_end = header_date.AddDays(1);

				foreach (ItemWrapper wrapper in elements)
					if (wrapper.item.last_modify > date_start && wrapper.item.last_modify < date_end)
						filenames.Add(wrapper);

				string tooltip_string = String.Format(Mono.Unix.Catalog.GetString("Zoom to {0} {1}, {2}"), GtkCommon.month_name(header_date.Month), 
													  header_date.Day, header_date.Year);

				element.set_header(header_string, filenames.Count > 0, tooltip_string);
				element.set_update_view(update_view, update_view_set_next);

				if (filenames.Count > 0) {
					element.set_dates(date_start, date_end, header_date, CalendarDriver.View.Day);
					element.set_files(filenames);
				}

				calendar_body.Attach(element.representation(), col, col+1, row, row+1, 
						     	     AttachOptions.Shrink | AttachOptions.Fill | AttachOptions.Expand, 
						     	     AttachOptions.Shrink | AttachOptions.Fill | AttachOptions.Expand, 0, 0);

				col = ++col % (uint)days_in_week;
	            if (col == 0)
	                ++row;

	            header_date = header_date.AddDays((days_in_week == 6 && col == 0) ? 2 : 1);
			}
			
			day_button.Active = false;
			week_button.Active = false;
			year_button.Active = false;
		}

		private void draw_year(Gtk.VBox calendar, CalendarDriver.State state, System.DateTime centered_date,
							   List<ItemWrapper> elements)
		{
			calendar.PackStart(calendar_body, true, true, 0);

			today_button.Label = Mono.Unix.Catalog.GetString("This year");
			current_date.Markup = "<b>" + System.Convert.ToString(centered_date.Year) + "</b>";

			calendar_body.Resize(2, 6);
			
			uint col = 0, row = 0; 

			System.DateTime header_date = state.start_time;

			// fill in months
			for (int i = 0; i < 12; ++i)
			{
				CalendarFixedElement element = new CalendarFixedElement(true, false, 0.5, size, true, false, 
																		String.Format("{0:MM/yy}", System.DateTime.Now) == String.Format("{0:MM/yy}", 
																		header_date));
				calendar_elements.Add(element);

				element.set_large_header(GtkCommon.month_name(header_date.Month), 
										 String.Format(Mono.Unix.Catalog.GetString("Zoom to {0}, {1}"), GtkCommon.month_name(header_date.Month), 
										 header_date.Year));
				element.set_update_view(update_view, update_view_set_next);

				List<ItemWrapper> filenames = new List<ItemWrapper>();

				DateTime date_start = header_date;
				DateTime date_end = header_date.AddMonths(1);

				foreach (ItemWrapper wrapper in elements) 
					if (wrapper.item.last_modify > date_start && wrapper.item.last_modify < date_end)
						filenames.Add(wrapper);

				if (filenames.Count > 0) {
					element.set_dates(date_start, date_end, header_date, CalendarDriver.View.Month);
					element.set_files(filenames);
				}

				calendar_body.Attach(element.representation(), col, col+1, row, row+1, 
						     	     AttachOptions.Shrink | AttachOptions.Fill | AttachOptions.Expand, 
						     	     AttachOptions.Shrink | AttachOptions.Fill | AttachOptions.Expand, 0, 0);

	            col = ++col % 6;
	            if (col == 0)
	                ++row;

				header_date = header_date.AddMonths(1);
			}
			
			day_button.Active = false;
			week_button.Active = false;
			month_button.Active = false;
		}

		public void draw(CalendarDriver.State state, System.DateTime centered_date, List<ItemWrapper> elements)
		{
			if (last_elements != null) {
				if (last_elements != elements) {
					last_elements = elements;
					day_offset = 0;
				}
			} else
				last_elements = elements;
			
			if (elements.Count > 0)
				System.Console.WriteLine("hurray! elements");

			if (calendar_body != null) {
				System.Console.WriteLine("cleaning up body of size:{0}", calendar_body.Children.Length);
				foreach (Widget w in calendar_body.Children)
					w.Destroy();
				calendar_body.Dispose();
			}
			
			foreach (CalendarFixedElement element in calendar_elements)
				element.reset(); // workaround GTK-sharp signal bug
				
			calendar_elements.Clear();

            // cleanup
            foreach (Widget w in calendar_event_box.Children)
				w.Destroy();
			
			Gtk.VBox calendar = new Gtk.VBox();
		
			calendar_event_box.Add(calendar);
			GtkCommon.set_background_color(calendar_event_box, "gray");

			Gtk.Table calendar_header = new Gtk.Table(1, 1, true);
			calendar_header.RowSpacing = 2;
			calendar_header.ColumnSpacing = 2;

			calendar_body = new Gtk.Table(1, 1, true);
			calendar_body.RowSpacing = 2;
			calendar_body.ColumnSpacing = 2;

			int days_in_week = 6; // lumb weekends together

			Singleton<OverlayTracker>.Instance.hide_and_die("time_overlay1");
			Singleton<OverlayTracker>.Instance.hide_and_die("time_overlay2");

			if (state.view == CalendarDriver.View.Day)
			{
				draw_day(calendar, state, centered_date, elements);
			}
			else if (state.view == CalendarDriver.View.Week)
			{
				draw_week(calendar, calendar_header, days_in_week, state, centered_date, elements);
			}
			else if (state.view == CalendarDriver.View.Month)
			{
				draw_month(calendar, calendar_header, days_in_week, state, centered_date, elements);
			}
			else if (state.view == CalendarDriver.View.Year)
			{
				draw_year(calendar, state, centered_date, elements);
			}

			System.Console.WriteLine("calendar show all");
			
			calendar.ShowAll();
		}
	}
}
