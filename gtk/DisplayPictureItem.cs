using System;
using System.Collections.Generic;

using Gtk;

namespace Nemo
{
	public class DisplayPictureItem : DisplayItem
	{
		string resolution;
	
		public DisplayPictureItem(string resolution, Item item)
		   : base(item)
		{
			this.resolution = resolution;
		}
		
		public override Gtk.Frame get_representation()
		{
			List<KeyValuePair<string, string>> elements = new List<KeyValuePair<string, string>>();
			elements.Add(new KeyValuePair<string, string>(Mono.Unix.Catalog.GetString("Resolution:"), resolution));

			return get_representation(elements);
		}
	}
}