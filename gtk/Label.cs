using Gtk;

using System;
using System.Collections.Generic;

// FIXME: should be split into UI and non-UI

namespace Nemo
{
	public abstract class Label
	{
        public bool restricted;
        
		public void unrestrict()
		{
			if (restricted) {
				if (b != null)
					b.Active = false;
				restricted = false;
			}
		}

		protected Gtk.ToggleButton b;

		public abstract Gtk.ToggleButton representation(TypeLabels label_class);
	}

	public class TypeLabel : Label
	{
		private Type type;

		public long id()
		{
			return type.id;
		}
		
		public TypeLabel(Type type)
		{
			this.type = type;
		}

		public override Gtk.ToggleButton representation(TypeLabels label_class)
		{
			string spacing = " ";

			b = new ToggleButton(spacing + type.name);

			b.Toggled += delegate(object sender, System.EventArgs e) { label_class.restrict_and_update(type.id); };
			if (!String.IsNullOrEmpty(type.icon))
				b.Image = new Gtk.Image(null, type.icon);
			else
				b.Image = CairoDrawing.create_empty_image(16, 16);

			GtkCommon.set_background_color(b, "white");

			b.BorderWidth = 0;
			b.Relief = Gtk.ReliefStyle.None;
			b.SetAlignment(0, 0);

			return b;
		}
	}

	public class UserLabel : Label
	{
		public MetaLabel metalabel;

		public Gtk.Image dot()
		{
			return CairoDrawing.create_dot_image(metalabel.color.r, metalabel.color.g, metalabel.color.b, 255, 14, 14);
		}

		public UserLabel(MetaLabel label)
		{
			metalabel = label;
		}

		public void rename_to(string new_name)
		{
			// call broker
			throw new System.NotImplementedException();
		}

		public void rename_category(string new_cat)
		{
			// call broker
			throw new System.NotImplementedException();
		}

		public override Gtk.ToggleButton representation(TypeLabels label_class)
		{
			throw new System.NotImplementedException();
		}
	}
}