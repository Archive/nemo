using System;
using System.Collections.Generic;

using Gtk;

namespace Nemo
{
	public class CategoryDrawer
	{
		public Gtk.VBox representation; 

		private Category category;

		public CategoryDrawer(Category cat)
		{
			category = cat;

			// create representation
			representation = new Gtk.VBox();
			representation.PackStart(create_category_button(cat), false, false, 6);
			representation.Name = category.metalabel.label;

			foreach (UserLabel label in cat.labels)
				representation.PackStart(create_button(label), false, false, 0);
		}

		public void clear_restrictions()
		{
			foreach (Widget w in representation)
			{
				if (w is RightClickLabelButton)
					(w as RightClickLabelButton).Active = false;
			}
		}

		private Gtk.EventBox create_category_button(Category cat)
		{
			Gtk.EventBox header_wrapper = new Gtk.EventBox();
			Gtk.Alignment header_alignment = new Gtk.Alignment(0, 0, 0, 0);

			Gtk.HBox header = new Gtk.HBox();
			header.Add(CairoDrawing.create_empty_image(22, 16));
			header.Add(new Gtk.Label(" " + cat.metalabel.label));

			header_alignment.Add(header);
			header_wrapper.Name = cat.metalabel.label;
			header_wrapper.Add(header_alignment);
			header_wrapper.ButtonPressEvent += delegate (object sender, ButtonPressEventArgs args) { 
				header_click(args, cat); 
			};
			return header_wrapper;
		}

		private RightClickLabelButton create_button(UserLabel label)
		{
			RightClickLabelButton b = new RightClickLabelButton();
			b.Name = label.metalabel.label;

			Gtk.Alignment box_alignment = new Gtk.Alignment(0, 0, 0, 0);
			Gtk.HBox box = new Gtk.HBox(); 

			Gtk.Label l = new Gtk.Label(" " + label.metalabel.label);

			Gtk.Image img = label.dot(); 
			
			box.Add(img);

			GtkCommon.set_foreground_color(l, new Gdk.Color(label.metalabel.color.r, label.metalabel.color.g, label.metalabel.color.b));

			box.Add(l);

			box_alignment.LeftPadding = 3;
			box_alignment.Add(box);

			b.Add(box_alignment);

			b.BorderWidth = 0;
			b.Relief = Gtk.ReliefStyle.None;
			
			b.ButtonPressEvent += delegate (object sender, ButtonPressEventArgs args) { 
				item_click(args, label, b); 
			};

			return b;
		}

		private void item_click(ButtonPressEventArgs args, UserLabel label, RightClickLabelButton b)
		{
			if (args.Event.Button == 1) {

				category.update_restriction_and_update_screen(label.metalabel);
				b.Active = !b.Active;

			} else if (args.Event.Button == 3) {
			 	Menu popupMenu = new Menu();

				MenuItem rename_item = new MenuItem(Mono.Unix.Catalog.GetString("Rename label"));
				rename_item.Activated += delegate { rename_label(label); };
				popupMenu.Add(rename_item);

				MenuItem delete_item = new MenuItem(Mono.Unix.Catalog.GetString("Delete label"));
				delete_item.Activated += delegate { remove_label(label.metalabel); };
				popupMenu.Add(delete_item);

				popupMenu.Add(new Gtk.SeparatorMenuItem());

				MenuItem create_label = new MenuItem(Mono.Unix.Catalog.GetString("Create a new label"));
				create_label.Activated += delegate { add_new_label(); };
				popupMenu.Add(create_label);

				popupMenu.ShowAll(); 
      			popupMenu.Popup(null, null, null, args.Event.Button, args.Event.Time);
			}
		}

		private void add_new_label()
		{
			System.Console.WriteLine("writing new label");
		
			Gtk.Entry new_label = new Gtk.Entry();
			new_label.Name = "new_label";
			new_label.AddEvents((int)Gdk.EventMask.KeyReleaseMask);
			new_label.KeyReleaseEvent += on_new_key_release;

			representation.Add(new_label);
			representation.ShowAll();

			new_label.GrabFocus();
		}

		private void remove_label(MetaLabel label)
		{
			foreach (Widget w in representation) {
				if (w.Name == label.label) {
					w.Destroy();
					break;
				}
			}
			
			category.remove_label(label);
		}

		UserLabel renamed_label;

		private void cancel_rename_label()
		{
			int pos = 0;
			foreach (Widget w in representation) {
				if (w.Name == "rename_label") {
					w.Destroy();
					break;
				} else
					++pos;
			}

			RightClickLabelButton old_label = create_button(renamed_label);

			representation.PackStart(old_label, false, false, 0);
			representation.ReorderChild(old_label, pos);
			representation.ShowAll();
		}

		private void rename_label(UserLabel label)
		{
			renamed_label = label;

			Gtk.Entry rename_label = new Gtk.Entry();
			rename_label.Text = label.metalabel.label;
			rename_label.Name = "rename_label";
			rename_label.AddEvents((int)Gdk.EventMask.KeyReleaseMask);
			rename_label.KeyReleaseEvent += on_rename_key_release;

			int pos = 0;
			foreach (Widget w in representation) {
				if (w.Name == label.metalabel.label) {
					w.Destroy();
					break;
				} else
					++pos;
			}
				
			representation.Add(rename_label);
			representation.ReorderChild(rename_label, pos);
			representation.ShowAll();

			rename_label.GrabFocus();
		}

		private void cancel_rename_category()
		{
			int pos = 0;
			foreach (Widget w in representation) {
				if (w.Name == "rename_cat") {
					w.Destroy();
					break;
				} else
					++pos;
			}

			Gtk.EventBox old_cat = create_category_button(category);

			representation.PackStart(old_cat, false, false, 6);
			representation.ReorderChild(old_cat, pos);
			representation.ShowAll();
		}

		public void rename_category(bool is_create)
		{
			Gtk.Entry rename_entry = new Gtk.Entry();
			rename_entry.Text = category.metalabel.label;
			rename_entry.Name = "rename_cat";
			rename_entry.AddEvents((int)Gdk.EventMask.KeyReleaseMask);
			rename_entry.KeyReleaseEvent += delegate (object sender, Gtk.KeyReleaseEventArgs e) { 
				on_rename_cat_key_release(e, is_create); 
			};

			int pos = 0;
			foreach (Widget w in representation) {
				if (w.Name == category.metalabel.label) {
					w.Destroy();
					break;
				} else
					++pos;
			}

			representation.PackStart(rename_entry, false, false, 6);
			representation.ReorderChild(rename_entry, pos);
			representation.ShowAll();

			rename_entry.GrabFocus();
		}

		protected void on_new_key_release(System.Object sender, Gtk.KeyReleaseEventArgs e)
		{
			VoidFunction<Widget> on_element_found = null;
		
			if (e.Event.Key == Gdk.Key.Escape) // cancel
			{
				on_element_found = delegate(Widget w) { w.Destroy(); };
			}
			else if (e.Event.Key == Gdk.Key.Return || e.Event.Key == Gdk.Key.KP_Enter) // ok
			{
				on_element_found = delegate(Widget w) {
					if (w.Name == "new_label") {
						string text = ((Gtk.Entry)w).Text;

						if (category.exists(text))
							return;

						category.add_label(text);
					}
				};
			}
			
			if (on_element_found != null) // do it
				foreach (Gtk.Widget w in representation)
					if (w.Name == "new_label") {
						on_element_found(w);
						break;
					}
		}

		protected void on_rename_key_release(object sender, Gtk.KeyReleaseEventArgs e)
		{
			if (e.Event.Key == Gdk.Key.Escape) // cancel
			{
				cancel_rename_label();
			}
			else if (e.Event.Key == Gdk.Key.Return || e.Event.Key == Gdk.Key.KP_Enter) // ok
			{
				foreach (Gtk.Widget w in representation)
				{
					if (w.Name == "rename_label") 
					{
						string text = (w as Gtk.Entry).Text;
						
						if (renamed_label.metalabel.label != text && category.exists(text)) 
							return;
						
						category.rename_label(renamed_label.metalabel, text);
					}
				}
			}
		}

		protected void on_rename_cat_key_release(Gtk.KeyReleaseEventArgs e, bool is_create)
		{
			if (e.Event.Key == Gdk.Key.Escape) // cancel
			{
				if (is_create)
					remove_category_from_ui_callback(category.metalabel.label);
				else
					cancel_rename_category();
			}
			else if (e.Event.Key == Gdk.Key.Return || e.Event.Key == Gdk.Key.KP_Enter) // ok
			{
				foreach (Gtk.Widget w in representation)
					if (w.Name == "rename_cat") {
						string text = ((Gtk.Entry)w).Text;
						
						if (is_create)
							category.create_in_db(text);
						else
							category.rename(text);

						break;
					} 
			}
		}

		public VoidFunction<long> remove_category_callback;
		public VoidFunction add_new_category_to_ui_callback;
		public VoidFunction<string> remove_category_from_ui_callback;
		
		private void header_click(ButtonPressEventArgs args, Category cat)
		{
			if (args.Event.Button == 3) {
			 	Menu popupMenu = new Menu();

				MenuItem create_label = new MenuItem(Mono.Unix.Catalog.GetString("Create a new label"));
				create_label.Activated += delegate { add_new_label(); };
				popupMenu.Add(create_label);

				MenuItem rename_item = new MenuItem(Mono.Unix.Catalog.GetString("Rename category"));
				rename_item.Activated += delegate { rename_category(false); };
				popupMenu.Add(rename_item);
				
				if (!category.initial) {
					MenuItem delete_item = new MenuItem(Mono.Unix.Catalog.GetString("Delete category"));
					delete_item.Activated += delegate { remove_category_callback(cat.metalabel.db_id); };
					popupMenu.Add(delete_item);
				}
				
				popupMenu.Add(new Gtk.SeparatorMenuItem());

				MenuItem create_item = new MenuItem(Mono.Unix.Catalog.GetString("Create a new category"));
				create_item.Activated += delegate { add_new_category_to_ui_callback(); };
				popupMenu.Add(create_item);

				popupMenu.ShowAll(); 
      			popupMenu.Popup(null, null, null, args.Event.Button, args.Event.Time);
			}
		}
	}
}
