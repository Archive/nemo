using System;
using System.Collections.Generic;

using Gtk;

namespace Nemo
{
	public class SearchPopup : Overlay
	{
		Gtk.Window popup;
		Gtk.VBox elements;
		
		VoidFunction<CalendarDriver.View, DateTime> set_view_callback;
		public Function<int> get_size;
		public Function<Tuple<int, int>> calculate_start_position;
		
		List<Item> items;
		string search_text;
		int offset;
		Stack<int> sizes_of_last_pages;
		int current_page_size;
		
		public SearchPopup(VoidFunction<CalendarDriver.View, DateTime> set_view)
		{
			popup = null;
			elements = null;
			set_view_callback = set_view;
			name = "search_popup";
			offset = 0;
			sizes_of_last_pages = new Stack<int>();
		}

		public override void hide()
		{
			popup.Hide();
		}
		
		public override void show()
		{
			popup.Show();
		}
		
		public override void shift(int shift_x, int shift_y)
		{
			int x = 0, y = 0;
			popup.GdkWindow.GetPosition(out x, out y);
			popup.Move(x + shift_x, y + shift_y);
		}
		
		public override void resize()
		{
			add_elements();
		}
		
		public void set_files_and_show(List<Item> items, string search_text)
		{
			this.items = items;
			this.search_text = search_text;

			add_elements();
		}

		private void add_elements()
		{
			Tuple<int, int> pos = calculate_start_position();

			create_window(search_text, pos.first, pos.second);

			popup.SetSizeRequest(500, -1);

			int height = get_size();

			int index = 0;

			foreach (Item item in items) {
				++index;
				if (index < offset)
					continue;

				if (item is DocumentItem)
					height -= 75;
				else
					height -= 55;
					
				if (height <= 0)
					break;

				elements.PackStart(create_element(item, search_text, "", delegate { popup.ShowAll(); }), false, false, 5);
			}
			
			current_page_size = index - offset;
			
			if (items.Count == 0) {
				Gtk.Alignment tmp = new Gtk.Alignment(0.5f, 1f, 1f, 1f);
				tmp.BorderWidth = 5;
				tmp.Add(new Gtk.Label(Mono.Unix.Catalog.GetString("No elements found")));
				elements.Add(tmp);
			}

			elements.SizeAllocated += delegate(object sender, Gtk.SizeAllocatedArgs args) {
				popup.Move(pos.first, pos.second - args.Allocation.Height);
			};

			Gtk.HBox page_navigation = new Gtk.HBox();

			if (offset > 0) // prev
				add_page_link(Mono.Unix.Catalog.GetString("Prev page"), page_navigation, offset, -1);

			if (items.Count > index) // next
				add_page_link(Mono.Unix.Catalog.GetString("Next page"), page_navigation, offset, current_page_size);
			else {
				Gtk.Alignment empty_box = new Gtk.Alignment(1,1,1,1);
				empty_box.LeftPadding = 27;
				empty_box.RightPadding = 27;
				empty_box.Add(new Gtk.Label());
				page_navigation.PackStart(empty_box, true, true, 5);
			}
			
	        Gtk.Alignment alignment = new Gtk.Alignment(1F, 1F, 0F, 0F);
			alignment.Add(page_navigation);
			elements.PackStart(alignment, true, true, 5);

			popup.ShowAll();

			System.Console.WriteLine("elements size {0}", items.Count);
		}
		
		private void add_page_link(string text, Gtk.HBox page_navigation, int offset, int current_page)
		{
			Gtk.EventBox link_wrapper = new Gtk.EventBox();
			Gtk.Label link = new Gtk.Label();
			link.Markup = "<u><small>" + text + "</small></u>";
			
			GtkCommon.show_hand_and_tooltip(link_wrapper, text);
			
			System.Console.WriteLine("adding a page link: {0} with offset on click {1}", text, offset);
			
			link_wrapper.Add(link);
			link_wrapper.ButtonPressEvent += delegate {
				if (current_page != -1) { // next
					sizes_of_last_pages.Push(current_page);
					this.offset = offset + current_page;
				} else {
					int last_page = sizes_of_last_pages.Pop();
					this.offset = offset - last_page;
				}
				add_elements();
			};
			
			GtkCommon.set_background_color(link_wrapper, "white");
			page_navigation.PackStart(link_wrapper, false, false, 5);
		}
		
		private void create_window(string search_text, int x, int y)
		{
			System.Console.WriteLine("creating search window");

			if (popup == null) {
				popup = new Gtk.Window(Gtk.WindowType.Popup);
				elements = new VBox();
			} else {
				foreach (Widget w in popup.Children)
					w.Destroy();
			}

	        Gtk.Alignment alignment = new Gtk.Alignment(1F, 0.5F, 0F, 0F);
			Gtk.EventBox image_wrapper = new EventBox();
			Gtk.Image close_image = new Gtk.Image(null, "close.png");
			image_wrapper.Add(close_image);

			if (String.IsNullOrEmpty(name))
				throw new Exception("name for an overlay cannot be empty");

			image_wrapper.ButtonPressEvent += delegate { 
				Singleton<OverlayTracker>.Instance.hide_and_die(name); 
			};
			GtkCommon.set_background_color(image_wrapper, "white");
			alignment.Add(image_wrapper);
			elements.PackStart(alignment, false, false, 0);
			
			Gtk.Label header = new Gtk.Label();
			header.Markup = "<big>" + String.Format(Mono.Unix.Catalog.GetString("Search results for: {0}"),  "<b>" + search_text + "</b></big>");
			
			elements.PackStart(header, false, false, 0);
			elements.PackStart(new Gtk.HSeparator(), false, false, 0);
			
			Gtk.EventBox border_wrapper = new Gtk.EventBox();

			Gtk.Frame border = new Gtk.Frame();

			Gtk.Alignment border_alignment = new Gtk.Alignment(1, 1, 1, 1);
			border_alignment.BottomPadding = 5;
			border_alignment.TopPadding = 5;
			border_alignment.LeftPadding = 5;
			border_alignment.RightPadding = 5;
			
			border_alignment.Add(elements);

			border.Add(border_alignment);
			
			border_wrapper.Add(border);

			GtkCommon.set_background_color(border_wrapper, "white");

			popup.Add(border_wrapper);
		}

		private void add_link(string text, CalendarDriver.View view, System.DateTime item_modified_date, Gtk.HBox links)
		{
			Gtk.EventBox link_wrapper = new Gtk.EventBox();
			Gtk.Label link = new Gtk.Label();
			link.Markup = "<u><small>" + text + "</small></u>";
			
			GtkCommon.show_hand_and_tooltip(link_wrapper, text);
			
			link_wrapper.Add(link);
			link_wrapper.ButtonPressEvent += delegate(object sender, Gtk.ButtonPressEventArgs args) {
				set_view_callback(view, item_modified_date); 
			};

			GtkCommon.set_background_color(link_wrapper, "white");
			links.PackStart(link_wrapper, false, false, 5);
		}

		private Gtk.HBox create_element(Item item, string search_text, string snippet_text, VoidFunction update)
		{
			Gtk.HBox element = new Gtk.HBox();
			Gtk.VBox lines = new Gtk.VBox();
			Gtk.HBox title_date = new Gtk.HBox();

			Gtk.EventBox title_wrapper = new Gtk.EventBox();
			Gtk.Label title = new Gtk.Label();
			
			string str_title = item.name(40);
			
			string capitalized = search_text.Substring(0, 1).ToUpper() + search_text.Substring(1); 
			
			if (str_title.Contains(search_text)) {
				int index = str_title.IndexOf(search_text);
				str_title = str_title.Substring(0, index) + "<b>" + search_text + "</b>" + str_title.Substring(index+search_text.Length);
			} else if (str_title.Contains(capitalized)) {
				int index = str_title.IndexOf(capitalized);
				str_title = str_title.Substring(0, index) + "<b>" + capitalized + "</b>" + str_title.Substring(index+capitalized.Length);
			}
			
			title.Markup = "<big>" + str_title + "</big>";
			title.UseUnderline = false;
			title.SetAlignment(0, 0); // left aligned
			title_wrapper.ButtonPressEvent += delegate(object sender, Gtk.ButtonPressEventArgs args) {
				if (args.Event.Button == 1)
					item.open(); 
			};
			title_wrapper.Add(title);
			GtkCommon.set_background_color(title_wrapper, "white");

			GtkCommon.show_hand_and_tooltip(title_wrapper, String.Format(Mono.Unix.Catalog.GetString("Open: {0}"), item.long_name));

			GtkCommon.create_preview(item, lines, title_wrapper);

			title_date.PackStart(title_wrapper, true, true, 0);
			
			Gtk.Alignment date_alignment = new Gtk.Alignment(1, 0, 0, 0);
			Gtk.Label date = new Gtk.Label();
			date.Markup = "<big>" + String.Format("{0:dd/MM/yy}", item.last_modify) + "</big>";
			date_alignment.Add(date);
			date_alignment.LeftPadding = 10;
			title_date.PackStart(date_alignment, true, true, 0);

			lines.PackStart(title_date, true, true, 0);
			
			try {
				DocumentItem doc_item = item as DocumentItem;

				if (doc_item != null) {
					doc_item.on_snippet_callback = delegate (string snippet) {
						if (!String.IsNullOrEmpty(snippet)) {

							System.Console.WriteLine("got snippet: {0}", snippet);

							Gtk.Label snippet_label = new Gtk.Label();
							snippet_label.SetAlignment(0, 0); // left aligned
							snippet_label.Markup = snippet;
							lines.PackStart(snippet_label, true, true, 2);
							lines.ReorderChild(snippet_label, 1);

							update();
						}
					};
					if (!String.IsNullOrEmpty(doc_item.snippet))
						doc_item.on_snippet_callback(doc_item.snippet);
				}
			} catch (Exception) { // not document item
			}

			Gtk.HBox label_links = new Gtk.HBox();

			Gtk.HBox bubbles = new Gtk.HBox();

			foreach (UserLabel label in item.labels)
				bubbles.Add(label.dot());

			label_links.PackStart(bubbles, false, false, 0);

			item.on_labels_callback = delegate() {
				foreach (Widget w in bubbles)
					w.Destroy();
			
				foreach (UserLabel label in item.labels)
					bubbles.Add(label.dot());
					
				element.ShowAll();
			};

			Gtk.Alignment links_alignment = new Gtk.Alignment(1,1,0,0);
			Gtk.HBox links = new Gtk.HBox();

			add_link(Mono.Unix.Catalog.GetString("Go to week"), CalendarDriver.View.Week, item.last_modify, links);
			add_link(Mono.Unix.Catalog.GetString("Go to month"), CalendarDriver.View.Month, item.last_modify, links);
			add_link(Mono.Unix.Catalog.GetString("Go to year"), CalendarDriver.View.Year, item.last_modify, links);

			links_alignment.Add(links);

			label_links.PackStart(links_alignment, true, true, 0);
			
			lines.PackStart(label_links, true, true, 2);

			string image = String.Empty;
			
			if (item.file != null)
				image = Singleton<Types>.Instance.get_type((int)item.file.type.category).icon;

			Gtk.Alignment img_alignment = new Gtk.Alignment(0, (float)0.05, 0,0);

			if (!String.IsNullOrEmpty(image)) 
			{
				Gtk.Image img;
				
				if (item.file.starred)
					img = CairoDrawing.create_small_starred_image(image);
				else
					img = new Gtk.Image(null, image); 
				
				item.register_small_starred_change(delegate(bool starred) {
					foreach (Widget w in img_alignment)
						w.Destroy();
					if (starred)
						img = CairoDrawing.create_small_starred_image(image);
					else
						img = new Gtk.Image(null, image); 
					img_alignment.Add(img);
					img_alignment.ShowAll();
				});
				
				img_alignment.Add(img);
				img_alignment.ShowAll();
			}
			
			element.PackStart(img_alignment, false, false, 5);

			element.PackStart(lines, true, true, 0);

			return element;
		}
	}
}
