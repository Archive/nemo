using System;
using Gtk;

namespace Nemo
{
	public class Tray
	{
		private MainWindow main_window;
	
		public bool hidden;

		public void restore_position()
		{
			main_window.Move(Singleton<Configuration>.Instance.data.main_window_x,
				             Singleton<Configuration>.Instance.data.main_window_y);
			Singleton<OverlayTracker>.Instance.show_all();
		}

		public void hide_window()
		{
			save_main_window_pos();
			main_window.hide();
			hidden = true;
		}

		private StatusIcon icon;		
		
		public Tray(MainWindow window)
		{
			main_window = window;
			hidden = false;
			
			icon =  new StatusIcon(Gdk.Pixbuf.LoadFromResource("small_blue_guy.png"));
			icon.Activate += popupActivated;
			icon.PopupMenu += popupMenuHandler;
			icon.Tooltip = " Nemo ";
		}
		
		private void popupActivated(object o, System.EventArgs args)
		{
   			if (hidden) 
   			{
				main_window.ShowAll();
				restore_position();
				hidden = false;
				main_window.Unref(); // restore correct reference count
   			}
			else 
			{
				if (main_window.HasToplevelFocus)
					hide_window();
				else
					main_window.Present();
			}
		}
		
		private void popupMenuHandler(object o, Gtk.PopupMenuArgs args)
		{
		    Menu popupMenu = new Menu();

			// add show
  			ImageMenuItem menuPopup1;
  			if (hidden)
  				menuPopup1 = new ImageMenuItem(Mono.Unix.Catalog.GetString("Show window"));
			else
  				menuPopup1 = new ImageMenuItem(Mono.Unix.Catalog.GetString("Hide window"));
  			menuPopup1.Image = new Image(Stock.Refresh, IconSize.Menu); 
  			popupMenu.Add(menuPopup1); 
  			menuPopup1.Activated += delegate {
  				if (hidden)
  				{
					main_window.ShowAll();
					restore_position();
					hidden = false;
					main_window.Unref(); // restore correct reference count
  				}
  				else
  				{
					hide_window();
  				}
  			};

  			ImageMenuItem menuPopup0 = new ImageMenuItem(Mono.Unix.Catalog.GetString("Indexing status"));
  			popupMenu.Add(menuPopup0);
  			menuPopup0.Activated += delegate {
				Singleton<Indexing>.Instance.Show();
  			};

  			ImageMenuItem menuPopup2 = new ImageMenuItem(Mono.Unix.Catalog.GetString("About"));
  			menuPopup2.Image = new Image(Stock.About, IconSize.Menu); 
  			popupMenu.Add(menuPopup2); 
  			menuPopup2.Activated += delegate {
				Gtk.AboutDialog about = new AboutDialog();
				about.Name = "Nemo";
				about.Authors = new string[] { "Anders Rune Jensen", "Lau Bech Lauritzen", "Ole Laursen" };
				about.Artists = new string[] { "Linda Nhu", "Sune Theodorsen" };
				about.Comments = Mono.Unix.Catalog.GetString("Nemo is a file manager for those who would rather have their files manage themselves.");
				about.Logo = new Gdk.Pixbuf(null, "blue_guy_med.png");
				about.Response += delegate {
					about.Destroy();
				};
				about.Show();
  			};

			// add quit
  			ImageMenuItem menuPopup3 = new ImageMenuItem (Mono.Unix.Catalog.GetString("Quit"));
  			menuPopup3.Image = new Image(Stock.Quit, IconSize.Menu); 
  			popupMenu.Add(menuPopup3); 
  			menuPopup3.Activated += new EventHandler(this.OnPopupClick);
  			
			popupMenu.ShowAll(); 
  			popupMenu.Popup(null, null, null, (uint)args.Args[0], (uint)args.Args[1]);
		}
		
		private void save_main_window_pos()
		{
			int x, y, w, h; // used to restore position on the screen
			main_window.GetPosition(out x, out y);
			main_window.GetSize(out w, out h);
			Singleton<Configuration>.Instance.data.main_window_x = x;
			Singleton<Configuration>.Instance.data.main_window_y = y;
			Singleton<Configuration>.Instance.data.main_window_width = w;
			Singleton<Configuration>.Instance.data.main_window_height = h;
		}
		
		public VoidFunction main_quit;
		
		private void OnPopupClick(object o, EventArgs args)
		{
			if (!hidden)  
				save_main_window_pos();

			Singleton<Configuration>.Instance.save_configuration();
			main_quit();

			Application.Quit();
		}
	}
}
