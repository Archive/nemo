using System;
using System.Collections.Generic;

using Gtk;

namespace Nemo
{
	public class CategoriesDrawer
	{
		public Function<Category> create_new_empty_category_callback;
		public VoidFunction<long> delete_category_callback;

		private Gtk.EventBox outer_event_box;
	
		public CategoriesDrawer(Gtk.EventBox box)
		{
			outer_event_box = box;
			category_drawers = new List<CategoryDrawer>();
		}
		
		public void clear_restrictions()
		{
			foreach (CategoryDrawer drawer in category_drawers)
				drawer.clear_restrictions();
		}
		
		List<CategoryDrawer> category_drawers;
		Gtk.VBox box;
		
		public void set_categories(List<Category> categories)
		{
			box = new Gtk.VBox();
		
			bool first = true;
			foreach (Category cat in categories)
			{
				CategoryDrawer c = new CategoryDrawer(cat);
				c.add_new_category_to_ui_callback = add_new_category;
				c.remove_category_callback = remove_category;
				category_drawers.Add(c);

				if (!first)
					box.PackStart(new Gtk.HSeparator(), false, false, 5);
				else
					first = false;

				box.PackStart(c.representation, false, false, 0);
			}

			foreach (Widget w in outer_event_box.Children)
				w.Destroy();

			outer_event_box.Add(box);

			outer_event_box.ShowAll();
		}

		private void remove_category_from_ui(string label)
		{
			foreach (CategoryDrawer drawer in category_drawers)
				if (drawer.representation.Name == label) {
					category_drawers.Remove(drawer);
					break;
				}

			Widget prev_widget = null;
			foreach (Widget w in box.Children) {
				if (w.Name == label) {
					w.Destroy();
					if (prev_widget != null)
						prev_widget.Destroy();
					break;
				} else
					prev_widget = w;
			}
		}

		private void remove_category(long db_id)
		{
			delete_category_callback(db_id);
		}

		private void add_new_category()
		{
			Category cat = create_new_empty_category_callback();

			CategoryDrawer c = new CategoryDrawer(cat);
			c.add_new_category_to_ui_callback = add_new_category;
			c.remove_category_callback = remove_category;
			c.remove_category_from_ui_callback = remove_category_from_ui;
			category_drawers.Add(c);

			if (box.Children.Length > 0)
				box.PackStart(new Gtk.HSeparator(), false, false, 5);
			
			box.PackStart(c.representation, false, false, 0);

			c.rename_category(true);

			outer_event_box.ShowAll();
		}
	}
}
