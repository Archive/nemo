using Gtk;

using Mono.Unix;

using System.Collections.Generic;

using System.Diagnostics; // for opening files

namespace Nemo
{
	static class GtkCommon
	{
		public static void show_hand_and_tooltip(Gtk.Widget widget, string tooltip_text)
		{
			GtkCommon.tooltip.SetTip(widget, " " + tooltip_text + " ", " " + tooltip_text + " ");

			widget.Realized += delegate {
				widget.GdkWindow.Cursor = new Gdk.Cursor(Gdk.CursorType.Hand2);
			};
		}
		
		public static void create_preview(Item item, Gtk.Widget placement, Gtk.Widget hover)
		{
			PreviewPopup popup = new PreviewPopup(item, placement);
			
			hover.EnterNotifyEvent += delegate (object sender,  EnterNotifyEventArgs args) { 
				Singleton<OverlayTracker>.Instance.add_overlay_and_show(popup);
				popup.show((int)(args.Event.XRoot - args.Event.X), (int)(args.Event.YRoot - args.Event.Y)); 
			};
			
			hover.LeaveNotifyEvent += delegate { 
				Singleton<OverlayTracker>.Instance.hide_and_die("preview_popup"); 
			};

			hover.Destroyed += delegate { 
				// make sure the window is always hidden when the item dies
				Singleton<OverlayTracker>.Instance.hide_and_die("preview_popup"); 
			}; 
		}

		public static Gtk.ButtonPressEventHandler ButtonPressEventWrapper(Item item)
		{
			return delegate (object o, Gtk.ButtonPressEventArgs args) { OnButtonPressEvent(args.Event, item); };
		}
	
		public static VoidFunction<Gdk.EventButton> OnButtonPressEventWrapper(Item item)
		{
			return delegate (Gdk.EventButton evnt) { OnButtonPressEvent(evnt, item); };
		}

		public static void OnButtonPressEvent(Gdk.EventButton evnt, Item item)
		{
			if (evnt.Button == 1)
			{
				item.open();
			}
			else if (evnt.Button == 3)
			{
			 	Menu popup_menu = new Menu();

				MenuItem open_item = new MenuItem(Mono.Unix.Catalog.GetString("Open"));
				open_item.Activated += delegate { item.open(); };
				popup_menu.Add(open_item);
				
				MenuItem open_with_item = new MenuItem(Mono.Unix.Catalog.GetString("Open with"));

				Gnome.Vfs.MimeApplication[] application_handlers = Gnome.Vfs.Mime.GetAllApplications(item.mime_type);

				if (application_handlers.Length > 0) {
					Menu handlers_menu = new Menu();
					
					foreach (Gnome.Vfs.MimeApplication m in application_handlers)
					{
						MenuItem handlers_menu_item = new MenuItem(m.Name);
						GLib.List tmp = new GLib.List(typeof(System.String)); // fixme, use better name
						tmp.Append(Gnome.Vfs.Uri.GetUriFromLocalPath(item.path));
						Gnome.Vfs.MimeApplication tmp_m = m; // go lambda bug, go
						handlers_menu_item.Activated += delegate { tmp_m.Launch(tmp); };
						handlers_menu.Add(handlers_menu_item);
					}

					open_with_item.Submenu = handlers_menu;
				}

				popup_menu.Add(open_with_item);

				MenuItem open_folder = new MenuItem(Mono.Unix.Catalog.GetString("Open dir containing file"));
				open_folder.Activated += delegate { 
					string path = item.path;
					int index = -1;
					if ((index = path.LastIndexOf('/')) != -1) {
						Gnome.Vfs.MimeApplication[] folder_handlers = Gnome.Vfs.Mime.GetAllApplications("inode/directory");
						GLib.List tmp = new GLib.List(typeof(System.String)); // fixme, use better name
						tmp.Append(Gnome.Vfs.Uri.GetUriFromLocalPath(path.Substring(0, index+1)));
						folder_handlers[0].Launch(tmp);
					}
				};
				popup_menu.Add(open_folder);

				MenuItem clipboard_item = new MenuItem(Mono.Unix.Catalog.GetString("Copy path to clipboard"));
				clipboard_item.Activated += delegate {
					Gtk.Clipboard clipboard_x = Gtk.Clipboard.Get(Gdk.Atom.Intern("PRIMARY", true));
					clipboard_x.Text = item.path;
					Gtk.Clipboard clipboard = Gtk.Clipboard.Get(Gdk.Atom.Intern("CLIPBOARD", true));
					clipboard.Text = item.path;
				};
				popup_menu.Add(clipboard_item);

				MenuItem labels_item = new MenuItem(Mono.Unix.Catalog.GetString("Labels"));
				Menu categories_menu = new Menu();

				bool first_category = true;

				foreach (Category cat in Singleton<Categories>.Instance.categories) {

					if (cat.labels.Count > 0 || Singleton<Categories>.Instance.categories.Count == 1) {

						if (first_category)
							first_category = false;
						else
							categories_menu.Add(new Gtk.SeparatorMenuItem());		

						MenuItem cat_item = new MenuItem(cat.metalabel.label);
						categories_menu.Add(cat_item);

						foreach (UserLabel tmp_label in cat.labels) {
							UserLabel label = tmp_label; // go lambda bug, go

							if (!item.contains_label(label)) 
							{
								ImageMenuItem label_item = new ImageMenuItem();

								Gtk.Label l = new Gtk.Label(label.metalabel.label);
								l.SetAlignment(0f, 0.5f);
								GtkCommon.set_foreground_color(l, new Gdk.Color(label.metalabel.color.r, label.metalabel.color.g, label.metalabel.color.b));
								label_item.Add(l);

								label_item.Image = CairoDrawing.create_dot_image(0, 0, 0, 0, 14, 14);

								label_item.Activated += delegate { item.make_label(label); };
								categories_menu.Add(label_item);
							} 
							else 
							{
								ImageMenuItem label_item = new ImageMenuItem();
								
								Gtk.Label l = new Gtk.Label(label.metalabel.label);
								l.SetAlignment(0f, 0.5f);
								GtkCommon.set_foreground_color(l, new Gdk.Color(label.metalabel.color.r, label.metalabel.color.g, label.metalabel.color.b));
								label_item.Add(l);

								label_item.Image = label.dot();
								
								label_item.Activated += delegate { item.remove_label(label); };
								categories_menu.Add(label_item);
							}
						}
					}
				}

				labels_item.Submenu = categories_menu;
			
				popup_menu.Add(labels_item);

				popup_menu.Add(new Gtk.SeparatorMenuItem());

				string starred_text = Mono.Unix.Catalog.GetString("Add star");
				if (item.file.starred)
					starred_text = Mono.Unix.Catalog.GetString("Remove star");

				ImageMenuItem starred_item = new ImageMenuItem(starred_text);
				starred_item.Image = new Gtk.Image(new Gdk.Pixbuf(null, "stock_about.png"));
				starred_item.Activated += delegate { item.update_starred(); };
				
				popup_menu.Add(starred_item);

				popup_menu.ShowAll(); 
      			popup_menu.Popup(null, null, null, evnt.Button, evnt.Time);

			}
		}
	
		public static void set_foreground_color(Gtk.Widget widget, Gdk.Color col) 
		{
			widget.ModifyFg(StateType.Normal, col);
			widget.ModifyFg(StateType.Active, col);
			widget.ModifyFg(StateType.Insensitive, col);
			widget.ModifyFg(StateType.Prelight, col);
			widget.ModifyFg(StateType.Selected, col);
		}

		public static void set_background_color(Gtk.Widget widget, string color) 
		{
			Gdk.Color col = new Gdk.Color();
			Gdk.Color.Parse(color, ref col);
			widget.ModifyBg(StateType.Normal, col);
			widget.ModifyBg(StateType.Active, col);
			widget.ModifyBg(StateType.Insensitive, col);
			widget.ModifyBg(StateType.Prelight, col);
			widget.ModifyBg(StateType.Selected, col);
		}

		public static void reset_background_color(Gtk.Widget widget) 
		{
			widget.ModifyBg(StateType.Normal);
		}
		
		public static Gtk.Tooltips tooltip;
		
		public static string month_name(int month)
		{
			switch (month)
			{
				case 1:
					return Catalog.GetString("January");
				case 2:
					return Catalog.GetString("February");
				case 3:
					return Catalog.GetString("March");
				case 4:
					return Catalog.GetString("April");
				case 5:
					return Catalog.GetString("May");
				case 6:
					return Catalog.GetString("June");
				case 7:
					return Catalog.GetString("July");
				case 8:
					return Catalog.GetString("August");
				case 9:
					return Catalog.GetString("September");
				case 10:
					return Catalog.GetString("October");
				case 11:
					return Catalog.GetString("November");
				case 12:
					return Catalog.GetString("December");
				default:
					return string.Empty;
			}
		}

		public static string day_name(System.DateTime date)
		{
			if (date.DayOfWeek == System.DayOfWeek.Monday)
				return Mono.Unix.Catalog.GetString("Monday");
			else if (date.DayOfWeek == System.DayOfWeek.Tuesday)
				return Mono.Unix.Catalog.GetString("Tuesday");
			else if (date.DayOfWeek == System.DayOfWeek.Wednesday)
				return Mono.Unix.Catalog.GetString("Wednesday");
			else if (date.DayOfWeek == System.DayOfWeek.Thursday)
				return Mono.Unix.Catalog.GetString("Thursday");
			else if (date.DayOfWeek == System.DayOfWeek.Friday)
				return Mono.Unix.Catalog.GetString("Friday");
			else if (date.DayOfWeek == System.DayOfWeek.Saturday)
				return Mono.Unix.Catalog.GetString("Saturday");
			else if (date.DayOfWeek == System.DayOfWeek.Sunday)
				return Mono.Unix.Catalog.GetString("Sunday");
			return string.Empty; // make compiler happy
		}
	}
}
