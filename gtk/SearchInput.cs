using System;

namespace Nemo
{
	public class SearchInput
	{
		private Gtk.Button search_word;

		public SearchInput()
		{
			search_word = null;
		}

		public void NewSearchInput(Gtk.Button new_search_word)
		{
			if (search_word != null)
				search_word.Destroy();
			search_word = new_search_word;
		}
		
		public void SearchWordDeleted(object obj, Gtk.DestroyEventArgs args)
		{
			search_word = null;
		}
	}
}