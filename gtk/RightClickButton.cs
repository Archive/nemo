using System;
using Gtk;

namespace Nemo
{
	public class RightClickItemButton : Gtk.Button
	{
		public Gtk.Image picture;
		private string pic_path;
		
		public bool starred;

		Gtk.Alignment img_alignment;
	
		public void set_picture(Gtk.Alignment img_alignment, string pic_path)
		{
			this.img_alignment = img_alignment;
			
			if (picture == null && starred)
				picture = CairoDrawing.create_small_starred_image(pic_path);
			else if (picture == null)
				picture = new Gtk.Image(null, pic_path); 

			this.pic_path = pic_path;
		}
		
		public void update_starred(bool starred)
		{
			this.starred = starred;
			
			foreach (Widget w in img_alignment.Children)
				w.Destroy();
			
			if (picture != null && starred)
				picture = CairoDrawing.create_small_starred_image(pic_path);
			else
				picture = new Gtk.Image(null, pic_path); 

			img_alignment.Add(picture);

			img_alignment.ShowAll();
		}

		VoidFunction<Gdk.EventButton> button_press_handler;

		protected override bool OnButtonPressEvent(Gdk.EventButton evnt)
		{
			if (button_press_handler != null)
				button_press_handler(evnt);

			return true;
		}
		
		public void set_item(Item item)
		{
			button_press_handler = GtkCommon.OnButtonPressEventWrapper(item);
		}
	}

	public class RightClickInactiveItemButton : RightClickItemButton 
	{
		protected override bool OnButtonPressEvent (Gdk.EventButton evnt)
		{
			return false;
		}
	}

	public class RightClickLabelButton : Gtk.ToggleButton
	{
		protected override bool OnButtonPressEvent (Gdk.EventButton evnt)
		{
			return false;
		}
	}

	public class ClickButton : Gtk.Button
	{
		protected override bool OnButtonPressEvent (Gdk.EventButton evnt)
		{
			return false;
		}
	}
}
