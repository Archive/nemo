using System;
using Gtk;

namespace Nemo
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			Application.Init ();

			bool reindex = false;
			bool hidden = false;

			if (args.Length > 0) {
				if (args[0] == "--reindex" || args.Length > 1 && args[1] == "--reindex")
					reindex = true;
				else if (args[0] == "--hidden" || args.Length > 1 && args[1] == "--hidden")
					hidden = true;
				for (int i = 0; i < args.Length; ++i)
				  if (args[i] == "--help") {
				    System.Console.WriteLine("Usage: nemo [options]");
				    System.Console.WriteLine(" where options include:");
				    System.Console.WriteLine("");
				    System.Console.WriteLine(" --reindex: Reindex all files");
				    System.Console.WriteLine(" --hidden:  Start nemo hidden");
				    return;
				  }
				else
					System.Console.WriteLine(args[0]);
			}
			
			MainWindow win = new MainWindow(reindex, hidden);
			if (!hidden)
				win.Show();

			Application.Run();
		}
	}
}
