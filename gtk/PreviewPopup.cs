using System;
using System.Collections.Generic;
using Gtk;

namespace Nemo
{
	public class PreviewPopup : Overlay
	{
		Popup popup;
		Item item;
		Widget widget;

		bool shown;

		public PreviewPopup(Item item, Widget widget)
		{
			popup = new Popup();
			shown = false;
			this.item = item;
			this.widget = widget;
			name = "preview_popup";
		}

		public void show(int x, int y)
		{
			popup.show(item, x, y, widget);
			shown = true;
		}

		public override void hide()
		{
			if (shown)
				popup.hide();
		}
		
		public override void show()
		{
		}
		
		public override void shift(int x, int y)
		{
		
		}
		
		public override void resize()
		{
		
		}

		public class Popup
		{
			private Gtk.Window window;

			public void show(Item item, int x, int y, Gtk.Widget widget)
			{
				if (window == null) {
					window = new Window(WindowType.Popup);

					window.Add(item.Display().get_representation());
				}
			
				int window_width = 0, window_height = 0;
				window.GetSize(out window_width, out window_height);

				int screen_height = Gdk.Screen.Default.Height; 
				int screen_width = Gdk.Screen.Default.Width; 

				int move_to_x = 0, move_to_y = 0;

				if (y + window_height <= screen_height) {
					if (x + window_width + widget.Allocation.Width < screen_width) { // button, right
						move_to_x = x + widget.Allocation.Width;
						move_to_y = y;
					} else { // button, left
						move_to_y = y;
						move_to_x = x - window_width;
					}
				} else {
					move_to_y = y - window_height + widget.Allocation.Height;
					if (x + window_width + widget.Allocation.Width < screen_width) // top, right
						move_to_x = x + widget.Allocation.Width;
					else // top, left
						move_to_x = x - window_width;
				}

	   			window.Move(move_to_x, move_to_y);
	   			window.Show();
			}
				
			public void hide()
			{
				window.Hide();
			}
		}
	}
}
