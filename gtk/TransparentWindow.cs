using System;

using Gtk;

namespace Nemo
{
	public class TransparentWindow : Overlay
	{
		Gtk.Window window;
	
		Function<int> x_pos_function;
		Function<int> y_pos_function;
		
		public TransparentWindow(Gtk.Widget main_widget, Function<int> x_pos_func, Function<int> y_pos_func, string the_name)
		{
			x_pos_function = x_pos_func;
			y_pos_function = y_pos_func;

			name = the_name;

			window = new Window(WindowType.Popup);

//			window.ExposeEvent += Expose;

			window.Add(main_widget);
				
			window.Move(x_pos_function(), y_pos_function());

   			window.ShowAll();		
		}
		
		public void Destroy()
		{
			window.Destroy();
		}
		
		public override void hide()
		{
			window.Hide();			
		}
		
		public override void show()
		{
   			window.ShowAll();		
		}

		public override void shift(int shift_x, int shift_y)
		{
			window.Move(x_pos_function(), y_pos_function());
		}

		public override void resize()
		{
			// we need the underlying window to settle down before trying to do our thing
			GLib.Timeout.Add(100, delegate { 
				window.Move(x_pos_function(), y_pos_function());
				return false; 
			});
		}

#if false
		private void Expose(object o, Gtk.ExposeEventArgs args)
		{
			System.Console.WriteLine("expose");
			
			int width;
			int height;
		
			window.GetSize(out width, out height);

			Gdk.Pixmap pm = new Gdk.Pixmap(null, width, height, 1);
			window.InputShapeCombineMask(pm, 0, 0);
		}
#endif
	}
}
