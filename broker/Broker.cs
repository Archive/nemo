using System;
using System.Collections.Generic;
using System.Threading;

namespace Nemo
{
	public abstract class Broker
	{
		protected MetadataStore meta;	
		
		// comes in the following order:
		// mime, width, height, title, page count
	    protected string[] registered_fields;
	    protected int registered_fields_offset;

		protected Dictionary<string, string> types;

	    protected Broker(bool reindex)
	    {
			types = new Dictionary<string,string>();	    
	    
			System.Console.WriteLine("reindex: {0}, first run {1}", reindex, Singleton<Configuration>.Instance.data.first_run);
			
			if (reindex || Singleton<Configuration>.Instance.data.first_run)
			{
				reindex_metadata_store();
				Singleton<Configuration>.Instance.data.first_run = false;
				Singleton<Configuration>.Instance.save_configuration();
			}
	    
			cur_search_id = 0;

			// metadata store
			if (meta == null) {
				meta = new MetadataStore(Singleton<Configuration>.Instance.data.watch_dir);
	        	meta.start(System.IO.Path.Combine(Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), ".nemo/"));
	        }
		}

		public abstract void stop();

	    protected void stop_broker()
	    {
	        meta.stop();
	    }

		public void reindex_metadata_store()
		{
		    System.Console.WriteLine("Reindexing filesystem!");

			if (meta == null) {
				meta = new MetadataStore(Singleton<Configuration>.Instance.data.watch_dir);
	        	meta.start(System.IO.Path.Combine(Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), ".nemo/"));
	        }

			System.Console.WriteLine(Singleton<Configuration>.Instance.data.watch_dir);

	        meta.sync_with_filesystem();
		}

	 	public int RegisterItemTypes(string[] fields) 
		{
			string[] new_registered_fields = new string[fields.Length + registered_fields.Length]; 
			
			int i = 0;
			
			foreach (string field in registered_fields)
			{
				new_registered_fields[i++] = field;
			}
			
			foreach (string field in fields)
			{
				new_registered_fields[i++] = types[field];
			}
			
			int registered_index = registered_fields.Length + registered_fields_offset;  
			
			registered_fields = new_registered_fields;

			System.Console.WriteLine("registering item type! {0}", registered_fields.Length);
			
			return registered_index;
		}

		public abstract void get_metadata(List<string> filenames, Dictionary<string, VoidFunction<string[]>> callback);
	 
	 	public void get_file_from_metadata_store(string filename, VoidFunction<File> callback)
	 	{
		 	ThreadPool.QueueUserWorkItem(delegate {
	 			meta.get_file_from_path(filename, callback);
	 		});
	 	}
	 
	 	protected int cur_search_id;

		public abstract void SearchForText(string search_string, bool fast, VoidFunction<List<Tuple<string[], File>>> callback);

	    public void Search(List<MetaLabel> keywords, List<long> types, DateTime date_start, DateTime date_end, bool starred, 
	    				   VoidFunction<List<File>> callback)
	    {
			Query q = new Query();
			q.labels = keywords;
			q.types = types;
			q.date_start = date_start;
			q.date_end = date_end;
			q.starred = starred;

	#if false
			meta.set_query_result_callback(callback);

			meta.set_query(q);
			meta.trigger_query_result_changed();
			
	#endif

	#if true
			meta.set_query_result_callback(delegate (List<File> files) {
				meta.set_query_result_callback(null);
				callback(files);
			});

	        ++cur_search_id;

	        int search_id = cur_search_id;

			meta.search(q, delegate(List<File> files) { 
				if (cur_search_id == search_id)
					callback(files);
			});
	#endif		
	    }

	    public void get_all_labels(VoidFunction<List<MetaLabel>> callback)
	    {
			meta.set_labels_changed_callback(callback);
			meta.trigger_labels_changed();
	    }

	    public void get_type_labels(VoidFunction<List<Tuple<FileTypeCategory, int>>> callback)
	    {
			meta.set_type_labels_changed_callback(callback);
			meta.trigger_type_labels_changed();
	    }

	    public void add_label(long parent_id, string label)
	    {
	        MetaLabel new_label = new MetaLabel();
	        new_label.label = label;
	        new_label.parent_db_id = parent_id;

			meta.add_label(new_label, delegate {});
	    }	
		
		public void remove_label(MetaLabel label)
	    {
			meta.remove_label(label, delegate {});
	    }
	    
		public void rename_label(MetaLabel label, string new_name)
		{
			meta.rename_label(label, new_name);
		}
		
	    public void add_label_to_file(File file, MetaLabel label)
		{
	        meta.add_label_to_file(file, label, delegate {});		
	    }

		public void remove_label_from_file(File file, MetaLabel label)
		{
	        meta.remove_label_from_file(file, label, delegate {});
	    }
			
		public void set_starred(File file, bool starred)
		{
		    meta.set_starred(file, starred, delegate {});	
	    }	
		
	    public abstract void search_snippet(string uri, string search_string, VoidFunction<string> callback);

#if false
	    public void get_text(string uri, VoidFunction<string> callback)
	    {
	    	// FIXME: remember thread pool
	//    	System.Console.WriteLine("TRACKER IS FUBARED");
			//string result = bus_files.GetTextContents(uri, 0, 100);
			callback(""); // result
	    }
#endif
	}
}