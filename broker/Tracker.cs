using System;
using System.Collections.Generic;
using System.Threading;
using NDesk.DBus;

[Interface("org.freedesktop.Tracker.Metadata")]
public interface Metadata
{
    string[] GetRegisteredTypes(string metadata_class);
	void RegisterType(string name, string datatype);
    string[] Get(string service, string id, string[] keys);
//    void Set(string service, string id, string[] keys, string[] values);
}

[Interface("org.freedesktop.Tracker.Search")]
public interface Search
{
//    string[] Text(int live_query_id, string service, string search_text, int offset, int max_hits);
    string GetSnippet(string service, string id, string search_text);
    string[][] Query(int live_query_id, string service, string[] fields, string search_text, string keywords,
                     string query_condition, bool sort_by_service, int offset, int max_hits);
    string[][] TextDetailed(int live_query_id, string service, string search_text, int offset, int max_hits);
}

[Interface("org.freedesktop.Tracker")]
public interface ITracker
{
    int GetVersion();
}

namespace Nemo
{
	public class Tracker : Broker
	{
	    private ITracker bus;
	    private Search bus_search;
	    private Metadata bus_metadata;
	    private Connection con;
		
	    private bool iterating;

		public Tracker(bool reindex) : base(reindex)
		{
			types.Add("width", "Image:Width");
			types.Add("height", "Image:Height");
			types.Add("title", "Doc:Title");
			types.Add("pagecount", "Doc:PageCount");
		
			finished = false;
			iterating = false;
			
			try {
			    con = Bus.Session;
			} catch {
			    System.Console.WriteLine("");
			    System.Console.WriteLine(Mono.Unix.Catalog.GetString("ERROR: Could not connect to the Dbus session"));
			    System.Console.WriteLine(Mono.Unix.Catalog.GetString("Please check the DBUS_SESSION_BUS_ADDRESS environment variable"));
			    System.Console.WriteLine(Mono.Unix.Catalog.GetString("This is normally set by running export `dbus-launch`"));
			    System.Environment.Exit(0);
			    
			}

			//string addr = "unix:path=/tmp/dbus-ABCDEFGHIJ";
	        //con = new Connection (addr);

	        ObjectPath opath = new ObjectPath("/org/freedesktop/Tracker/Search");

	        bus = con.GetObject<ITracker>("org.freedesktop.Tracker", new ObjectPath("/org/freedesktop/Tracker"));
	        bus_search = con.GetObject<Search>("org.freedesktop.Tracker", new ObjectPath("/org/freedesktop/Tracker/Search"));
	        bus_metadata = con.GetObject<Metadata>("org.freedesktop.Tracker", new ObjectPath("/org/freedesktop/Tracker/Metadata"));

	        registered_fields = new string[] { "File:Mime" }; 

	        start_iterating();
		}

		bool finished;

	    public void start_iterating() 
	    {
	        if (iterating) // re-entrance protection
	            return;

			ThreadPool.QueueUserWorkItem(delegate(Object stateInfo) {
	            while (!finished)
	                con.Iterate();
			});

	        iterating = true;
	    }

	    public override void stop() 
	    {
	    	stop_broker();
	        finished = true;
	        bus.GetVersion(); // final iteration
	        iterating = false;
	    }

	 	public override void get_metadata(List<string> filenames, Dictionary<string, VoidFunction<string[]>> callbacks)
	 	{
	 		System.Console.WriteLine("searching for metadata for {0} files", filenames.Count);
	 		foreach (string filename in filenames) {
				string tmp_filename = filename; // go lambda, go
			 	ThreadPool.QueueUserWorkItem(delegate {
		 			callbacks["file://" + tmp_filename](bus_metadata.Get("Files", tmp_filename, registered_fields));
		 		});
		 	}
	 	}

		// result returned as array of [type, uri]
		public override void SearchForText(string search_string, bool fast, VoidFunction<List<Tuple<string[], File>>> callback)
		{
			ThreadPool.QueueUserWorkItem(delegate {
	            ++cur_search_id;

	            int search_id = cur_search_id;

	            if (!fast)
	                Thread.Sleep(1000);

	            if (cur_search_id == search_id)
	            {
					// the interface for searching using tracker is borken so we need to get 500 to make sure we get some we like...
					string[][] result = bus_search.TextDetailed(-1, "Files", search_string, 0, 500);

					if (cur_search_id == search_id) {

						List<string> filenames = new List<string>();

						foreach (string[] r in result) {

							System.Console.WriteLine("{0} - {1}", r[0], r[1]);
				
							if (r[1] == "Folders") // sigh, skip folders
								continue;

							filenames.Add(r[0]);
						}
						
						System.Console.WriteLine("search metadata store for {0} files", filenames.Count);
						
						meta.get_files_from_paths(filenames, Helpers.RunInMainThread<List<File>>(delegate (List<File> files) {

							System.Console.WriteLine("got {0} files from metadata store", files.Count);

							List<Tuple<string[], File>> real_results = new List<Tuple<string[], File>>();

							foreach (File file in files)
								real_results.Add(new Tuple<string[], File>(bus_metadata.Get("Files", file.path, registered_fields), file));

							callback(real_results);
						}));
					}
	            }
	        });
		}

	    public override void search_snippet(string uri, string search_string, VoidFunction<string> callback)
	    {
			ThreadPool.QueueUserWorkItem(delegate {
				string result = string.Empty;
				if (search_string.Length != 0)
					result = bus_search.GetSnippet("Files", uri, search_string);
				callback(result);
			});
	    }
	}
}
