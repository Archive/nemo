using System;
using System.Collections.Generic;
using System.Threading;
using NDesk.DBus;
using org.freedesktop.DBus;

[Interface("org.freedesktop.xesam.Search")]
public interface IXesamSearch
{
    string NewSession();
    void CloseSession(string id);
    object GetProperty(string id, string prop);
    object SetProperty(string id, string prop, object val);
    string NewSearch(string id, string xml);
    object[][] GetHits(string id, UInt32 amount);
    void StartSearch(string id);

    event Nemo.VoidFunction<string, UInt32> HitsAdded;
    event Nemo.VoidFunction<string> SearchDone;
}

namespace Nemo
{
	public class Xesam : Broker
	{
	    private IXesamSearch bus_search;
	    private Connection con;

	    private bool iterating;
	    
	    private string[] supported_mimetypes;

		public Xesam(bool reindex) : base(reindex)
		{
			types.Add("width", "xesam:width");
			types.Add("height", "xesam:height");
			types.Add("title", "xesam:title");
			types.Add("pagecount", "xesam:pageCount");

			finished = false;
			iterating = false;
			
			session_id = string.Empty;
			
			try {
			    BusG.Init ();
			} catch {
			    System.Console.WriteLine("");
			    System.Console.WriteLine(Mono.Unix.Catalog.GetString("ERROR: Could not connect to the Dbus session"));
			    System.Console.WriteLine(Mono.Unix.Catalog.GetString("Please check the DBUS_SESSION_BUS_ADDRESS environment variable"));
			    System.Console.WriteLine(Mono.Unix.Catalog.GetString("This is normally set by running export `dbus-launch`"));
			    System.Environment.Exit(0);
			    
			}

	        con = Bus.Session;
			
	        ObjectPath opath = new ObjectPath("/org/freedesktop/xesam/searcher/main");

	        bus_search = con.GetObject<IXesamSearch>("org.freedesktop.xesam.searcher", opath);

			bus_search.HitsAdded += hits_callback_handler;
			bus_search.SearchDone += hits_done_handler;
			
	        registered_fields = new string[] { "xesam:url", "xesam:mimeType" };
	        
	        registered_fields_offset = -1; // url
	        
	        supported_mimetypes = new string[] { "application/msword", "application/vnd.oasis.opendocument.text",
	        									 "application/pdf", "application/vnd.ms-excel", "application/vnd.oasis.opendocument.spreadsheet",
	        									 "application/vnd.sun.xml.writer", "application/vnd.ms-powerpoint", 
	        									 "application/vnd.oasis.opendocument.presentation", "image/png", "image/jpeg", "image/gif",
												 "video/mpeg", "video/avi", "video/x-avi", "video/x-matroska", "application/x-bzip-compressed-tar",
												 "application/x-bzip", "application/x-compressed-tar", "application/x-gzip", "application/zip" };  

			hits_added_callbacks = new Dictionary<string, Callback>();
			hits_done_callbacks = new Dictionary<string, VoidFunction>();
		}

		private class Callback
		{
			public int number_of_expected_results;
			public int cur_number_of_results;

			// Hurray for generics...
		    public delegate void get_results_callback(ref int count, UInt32 amount);
		    public get_results_callback get_results;
		    
			public Callback(int number_of_expected_results, get_results_callback get_results)
			{
				cur_number_of_results = 0;
				this.number_of_expected_results = number_of_expected_results;
				this.get_results = get_results;
			}
			
			public bool done()
			{
				return cur_number_of_results >= number_of_expected_results;
			}
		}

		private void hits_callback_handler(string session_id, UInt32 amount)
		{
			System.Console.WriteLine("the handler is handling {0}", session_id);

			// for text search, we only want the first results so we need this hack 
			if (!hits_added_callbacks.ContainsKey(session_id))
				return;

			System.Console.WriteLine("handler ok {0}", session_id);

			Callback callback = hits_added_callbacks[session_id];
			
			callback.get_results(ref callback.cur_number_of_results, amount);
		}

		private void hits_done_handler(string search_id)
		{
			System.Console.WriteLine("is done {0}", search_id);

			// for text search, we only want the first results so we need this hack 
			if (!hits_added_callbacks.ContainsKey(search_id))
				return;

			System.Console.WriteLine("done callback {0}", search_id);

			VoidFunction callback = hits_done_callbacks[search_id];
			callback();
			hits_done_callbacks.Remove(search_id);
		}

		private Dictionary<string, Callback> hits_added_callbacks;
		private Dictionary<string, VoidFunction> hits_done_callbacks;

		bool finished;
		
		string session_id;

	    public override void stop() 
	    {
			stop_broker();
	    }

	 	public override void get_metadata(List<string> all_filenames, Dictionary<string, VoidFunction<string[]>> callbacks)
	 	{
	 		int offset = 0;

	 		// split into queries of 100 since that's all that beagle will return to us
	 		foreach (List<string> filenames in Helpers.split_list<string>(all_filenames, 100))
			{
				int tmp_offset = offset; // go lambda
				List<string> tmp_filenames = filenames; // go lambda

				ThreadPool.QueueUserWorkItem(delegate {

					System.Console.WriteLine("trying to get new session id");

					string this_session_id = bus_search.NewSession();

					System.Console.WriteLine("got session id {0} -- registered_fields {1}", this_session_id, registered_fields.Length);

					bus_search.SetProperty(this_session_id, "hit.fields", registered_fields);
					
					System.Console.WriteLine("search xesam for {0} files with session id {1}, trying to get {2}", tmp_filenames.Count, this_session_id, tmp_filenames.Count);

					System.Text.StringBuilder b = new System.Text.StringBuilder(
                         @"<?xml version=""1.0"" encoding=""UTF-8""?>
						   <request xmlns=""http://freedesktop.org/standards/xesam/1.0/query"">
						   <query>
						   <inSet><field name=""xesam:url""/>");

					foreach (string filename in tmp_filenames) {
						b.Append("<string>file://");
						b.Append(filename);
						b.Append("</string>");
					}

					b.Append(@"</inSet>
							   </query>
							   </request>");

//					System.Console.WriteLine(xml_string);

					string search_id = bus_search.NewSearch(this_session_id, b.ToString()); 

					hits_added_callbacks.Add(search_id, new Callback(tmp_filenames.Count, delegate(ref int count, UInt32 amount) {
						System.Console.WriteLine("hits added in get metadata: {0} - {1}", amount, this_session_id);

						object[][] results = bus_search.GetHits(search_id, amount);
						
						System.Console.WriteLine("ze hits in get metadata!");

						foreach (object[] result in results) {
							string[] s = new string[result.Length];
							string filename = result[0] as string;

							int si = 0;
							bool first = true;
							foreach (object r in result)
								if (first) {
									first = false;
									continue;
								} else
									s[si++] = r as string;
								
							if (!callbacks.ContainsKey(filename))
								System.Console.WriteLine("warning, file not found in calllback {0}", s[0]);
							else
								callbacks[filename](s);
							++count;
						}
					}));

					hits_done_callbacks.Add(search_id, delegate  { 
						System.Console.WriteLine("closing session {0}, search {1}", this_session_id, search_id);
						bus_search.CloseSession(this_session_id);
						hits_added_callbacks.Remove(search_id);
					});

					bus_search.StartSearch(this_session_id);
		        });

				offset += 100;
			}
	 	}

	    public override void search_snippet(string uri, string search_string, VoidFunction<string> callback)
		{
       		string session_id = bus_search.NewSession();
			bus_search.SetProperty(session_id, "hit.fields", new string[] { "snippet" });

			string xml_string = String.Format(@"<?xml version=""1.0"" encoding=""UTF-8""?>
					<request xmlns=""http://freedesktop.org/standards/xesam/1.0/query"">
  						<query>
   <and>
    <inSet>
      <field name=""xesam:url""/>
      <string>file://{0}</string> 
    </inSet> 
    <fullText> 
      <string>{1}</string> 
    </fullText> 
   </and>
  </query> 
</request>", uri, search_string);
	
			string search_id = bus_search.NewSearch(session_id, xml_string);

			hits_added_callbacks.Add(search_id, new Callback(1, delegate(ref int count, UInt32 amount) {
				object[][] result = bus_search.GetHits(search_id, amount);
				if (result.Length > 0)
					callback(result[0][0] as string);
			}));

			hits_done_callbacks.Add(search_id, delegate  { 
				bus_search.CloseSession(session_id);
				hits_added_callbacks.Remove(search_id);
			});

			bus_search.StartSearch(session_id);		
		}

		// delagates not mighty enough to support yield
		public static IEnumerable<string> string_start()
		{
			while (true)
				yield return "<string>";
		}

		public static IEnumerable<string> string_end()
		{
			while (true)
				yield return "</string>";
		}

		// result returned as array of [type, uri]
		public override void SearchForText(string search_string, bool fast, VoidFunction<List<Tuple<string[], File>>> callback)
		{
			System.Console.WriteLine("searching for text");
		
			ThreadPool.QueueUserWorkItem(delegate {
	            ++cur_search_id;

	            int unique_search_id = cur_search_id;

	            if (!fast)
	                Thread.Sleep(1000);

				System.Console.WriteLine("searching for text 2");

	            if (cur_search_id == unique_search_id)
	            {
					System.Console.WriteLine("new session?");
            		session_id = bus_search.NewSession();
					System.Console.WriteLine("just before hit fields are called");
					bus_search.SetProperty(session_id, "hit.fields", registered_fields);
					bus_search.SetProperty(session_id, "hit.fields.extended", new string[] { "snippet" });
					System.Console.WriteLine("got session id {0}", session_id);
				
					System.Console.WriteLine("searching for text 3");

					bool got_results = false;
	
					string xml_string = String.Format(@"<?xml version=""1.0"" encoding=""UTF-8""?>
<request xmlns=""http://freedesktop.org/standards/xesam/1.0/query"">
  <query>
    <and>
    <inSet>
      <field name=""mime""/>{0}
    </inSet>
    <fullText>
      <string>{1}</string>
    </fullText>
    </and>
  </query>
</request>",
Helpers.reduce_to_string(Helpers.map_strings(string_start(),
supported_mimetypes, string_end())), search_string);

//					System.Console.WriteLine(xml_string);

					string search_id = bus_search.NewSearch(session_id, xml_string);

					List<Tuple<string[], File>> results = new List<Tuple<string[],File>>();

					bool search_closed = false;

					hits_added_callbacks.Add(search_id, new Callback(10, delegate(ref int count, UInt32 amount) {
						System.Console.WriteLine("hits added: {0} - {1}", amount, session_id);

						object[][] result = bus_search.GetHits(search_id, amount);
						
						System.Console.WriteLine("ze hits!");

						if (cur_search_id == unique_search_id) {

							System.Console.WriteLine("hits ok");

							List<string> filenames = new List<string>();

							foreach (object[] r in result) {
								filenames.Add((r[0] as string).Substring(7)); // cut file://
								//System.Console.WriteLine((r[0] as string).Substring(7));
							}
							
							System.Console.WriteLine("search metadata store for {0} files", filenames.Count);

							meta.get_files_from_paths(filenames, Helpers.RunInMainThread<List<File>>(delegate (List<File> files) {

								// we get results back from Xesam out of order
								// we can't use filenames since mono is buggy

								foreach (object[] r in result)
								{
									string path = (r[0] as string).Substring(7);
									foreach (File file in files)
										if (file.path == path) {
											string[] s = new string[r.Length];

											int si = 0;
											bool first = true;
											foreach (object o in r)
												if (first) {
													first = false;
													continue;
												} else
													s[si++] = o as string;

											results.Add(new Tuple<string[], File>(s, file));
											break;
										}
								}

								got_results = true;

#if false
								// We don't want more results
								if (hits_added_callbacks.ContainsKey(search_id))
									hits_added_callbacks.Remove(search_id);
								if (hits_done_callbacks.ContainsKey(search_id))
									hits_done_callbacks.Remove(search_id);
								if (!search_closed) {
									bus_search.CloseSession(session_id);
									search_closed = true;
								}
#endif								
							}));
						}
					}));

					hits_done_callbacks.Add(search_id, delegate  { 
						System.Console.WriteLine("closing session {0}", session_id);

						if (!search_closed) {
							bus_search.CloseSession(session_id);
							search_closed = true;
						}

						hits_added_callbacks.Remove(search_id);

						if (!got_results) // no results, FIXME: not needed
							callback(new List<Tuple<string[], File>>());
						else
							callback(results);
					});

					bus_search.StartSearch(session_id);
	            }
	        });
		}
	}
}
