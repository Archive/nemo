using System;

namespace Nemo
{
	public class Configuration
	{
		public ConfigurationData data;
		
		private Configuration()
		{
			load_configuration();
		}
		
		public void save_configuration()
		{
			Helpers.save<ConfigurationData>("configuration.xml", data);
		}
		
		private void load_configuration()
		{
			try {
				data = Helpers.load<ConfigurationData>("configuration.xml");
				if (data.watch_dir == "~")
					data.watch_dir = Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
				else if (data.watch_dir.Contains("~/"))
					data.watch_dir = data.watch_dir.Replace("~/", Environment.GetFolderPath(System.Environment.SpecialFolder.Personal) + "/");
			} catch (Exception) {
				data = new ConfigurationData();
				data.default_values();
			}
		}
	}
	
	public class ConfigurationData
	{
		public string watch_dir;
		public int main_window_width;
		public int main_window_height;
		public int main_window_x;
		public int main_window_y;
		public string exclude_dirs;
		public string exclude_files;
		public string include_files;
		public string search_tool;
		public bool first_run;
		
		public void default_values()
		{
			watch_dir = Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
			main_window_width = 800;
			main_window_height = 600;
			main_window_x = 0;
			main_window_y = 0;
			exclude_dirs = "/. /CVS/ /CVSROOT/";
			exclude_files = "~ .db .o";
			include_files = ".txt .doc .docx .pdf .png .jpg .gif .ppt .xls .xlsx .avi .mpg .mkv .tar.gz .tar.bz2 .zip .odt .ods .odp .xfc";
			search_tool = "tracker";
			first_run = true;
		}
	}
}
