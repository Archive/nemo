using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.Threading;
using System.IO;

namespace Nemo
{
    public struct Tuple<TFirst,TSecond> : IEquatable<Tuple<TFirst,TSecond>>
	{
        public TFirst first;
        public TSecond second;
        
        public Tuple(TFirst first, TSecond second) 
        { 
			this.first = first;
			this.second = second; 
		}
		
		public bool Equals(Tuple<TFirst,TSecond> other)
	    {
			return first.Equals(other.first)&& second.Equals(other.second);
	    }

		public static bool operator==(Tuple<TFirst,TSecond> lhs, Tuple<TFirst,TSecond> rhs)
	    {
			return lhs.Equals(rhs);
	    }

	    public static bool operator!=(Tuple<TFirst,TSecond> lhs, Tuple<TFirst,TSecond> rhs)
	    {
			return !(lhs == rhs);
	    }
    }

	static class Helpers 
    {
		public static VoidFunction<A> RunInMainThread<A>(VoidFunction<A> b)
		{
			return delegate(A a) { Gtk.Application.Invoke(delegate { b(a); }); };
		}

		public static VoidFunction RunInMainThread(VoidFunction b)
		{
			return delegate { Gtk.Application.Invoke( delegate { b(); }); };
		}

		// uses one shared timer, so will only allow one caller type to use this at a time
		public static void RunOnlyOnceFIFO(VoidFunction func, int timeout /* ms */)
		{
            if (update_timer != null)
                return;
            
            update_timer = new Timer(delegate(Object s) { RunOnlyOnceTimedOut(func); }, null,
                         	         timeout /* msecs */, Timeout.Infinite);
        }

		public static void RunOnlyOnceLIFO(VoidFunction func, int timeout /* ms */)
		{
            if (update_timer != null) {
	            update_timer.Dispose();
	            update_timer = null;
	         }
            
            update_timer = new Timer(delegate(Object s) { RunOnlyOnceTimedOut(func); }, null,
                         	         timeout /* msecs */, Timeout.Infinite);
        }

		private static Timer update_timer;

		private static void RunOnlyOnceTimedOut(VoidFunction func)
		{
            func();
            update_timer.Dispose();
            update_timer = null;
		}

		// allows any number of callers to use this abstraction by identifying themselves by name
		public static void RunOnlyOnceInMainLIFO(string name, VoidFunction func, uint timeout /* ms */)
		{
			if (!main_timeout_id.ContainsKey(name))
				main_timeout_id.Add(name, GLib.Timeout.Add(timeout, delegate { main_timeout_id.Remove(name); func(); return false; } ));
			else {
				GLib.Source.Remove(main_timeout_id[name]);
				main_timeout_id[name] = GLib.Timeout.Add(timeout, delegate { main_timeout_id.Remove(name); func(); return false; } );
			}			
        }

		static Dictionary<string, uint> main_timeout_id = new Dictionary<string,uint>();

		public static Type load<Type>(string filename)
		{
			Type loaded = default(Type);
			XmlSerializer s = new XmlSerializer(typeof(Type));
			TextReader r = new StreamReader(System.IO.Path.Combine(System.IO.Path.Combine(Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), ".nemo/"), filename));
			loaded = (Type)s.Deserialize(r);
			r.Close();
			return loaded;
		}

		public static void save<Type>(string filename, Type to_save)
		{
			XmlSerializer s = new XmlSerializer(typeof(Type));
			System.IO.DirectoryInfo dir = new DirectoryInfo(System.IO.Path.Combine(Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), ".nemo"));
			if (!dir.Exists)
				dir.Create();
			TextWriter w = new StreamWriter(System.IO.Path.Combine(System.IO.Path.Combine(Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), ".nemo/"), filename));
			s.Serialize(w, to_save);
			w.Close();
		}

		public static string strip_html_tags(string text)
		{
			string val = text;
			int index = 0;
			while ((index = val.IndexOf("<")) != -1)
			{
				int index2 = val.IndexOf(">");
				if (index2 != -1 && index2 < index + 25 && index2 > index)
					val = val.Remove(index, index2 - index + 1);
				else
					break;
			}

			return val;
		}

		public static List<List<T>> split_list<T>(List<T> l, int split_size)
		{
			List<List<T>> lists = new List<List<T>>();

			int index = 0;
			while (index < l.Count)
			{
				if (index + split_size > l.Count)
					split_size = l.Count - index; 

				lists.Add(l.GetRange(index, split_size));
				index += split_size;
			}
			
			return lists;
		}
		
		public static IEnumerable<int> ints()
		{
			int i = 0;
			while (true)
				yield return i++;
		}

		public static IEnumerable<string> readfile(string filename)
		{
		    using (TextReader r = new StreamReader(filename))
            {
				string result;
				while ((result = r.ReadLine()) != null)
				    yield return result;
		    }
		}

		static List<IEnumerator<T>> to_enumerators<T>(params IEnumerable<T>[] l)
		{
			List<IEnumerator<T>> r = new List<IEnumerator<T>>(); 
			foreach (IEnumerable<T> i in l)
				r.Add(i.GetEnumerator());
			return r;
		}

		static IEnumerable<bool> to_bool(IEnumerable<IEnumerator<string>> l)
		{
            IEnumerator<IEnumerator<string>> i = l.GetEnumerator();
			while (i.MoveNext())
				yield return i.Current.MoveNext();
		}

		static IEnumerable<string> to_value(IEnumerable<IEnumerator<string>> l)
		{
            IEnumerator<IEnumerator<string>> i = l.GetEnumerator();
			while (i.MoveNext())
				yield return i.Current.Current;
		}

		public static IEnumerable<string> map_strings(params IEnumerable<string>[] l)
		{
			List<IEnumerator<string>> enumerators = to_enumerators<string>(l);

            while (reduce(to_bool(enumerators)))
                yield return reduce_to_string(to_value(enumerators));
		}

		public static string reduce_to_string(IEnumerable<string> l)
		{
            IEnumerator<string> i = l.GetEnumerator();

			System.Text.StringBuilder builder = new System.Text.StringBuilder();

            while (i.MoveNext())
                builder.Append(i.Current);
            
            return builder.ToString();
		}
		
		// does short-circuiting
		public static bool reduce(IEnumerable<bool> l)
		{
            IEnumerator<bool> i = l.GetEnumerator();

			bool status = true;

			while (i.MoveNext()) {
			    status = i.Current;
			    if (!status)
					break;
			}
            
            return status;
		}

        public static IEnumerable<Tuple<T1,T2>> zip<T1, T2>(IEnumerable<T1> l1, IEnumerable<T2> l2)
        {
            IEnumerator<T1> i1 = l1.GetEnumerator();
            IEnumerator<T2> i2 = l2.GetEnumerator();

            while (i1.MoveNext() && i2.MoveNext())
                yield return new Tuple<T1, T2>(i1.Current, i2.Current);
        }

        public static IEnumerable<Tuple<int,T>> enumerate<T>(IEnumerable<T> ie)
        {
        	return zip<int, T>(ints(), ie);
        }

        public static bool sorted_lists_equal<T>(T l1, T l2) where T:System.Collections.Generic.ICollection<T>
        {
            if (l1.Count != l2.Count)
                return false;
            foreach (Tuple<T, T> t in zip<T,T>(l1, l2)) {
                if (!t.first.Equals(t.second))
                    return false;
            }
			
            return true;
        }
    }

	public class Color
	{
		public byte r;
		public byte g;
		public byte b;
		public byte a;
		
		public Color(byte r, byte g, byte b)
		{
			this.r = r;
			this.g = g;
			this.b = b;
			this.a = 0;
		}

		// serialization
		public Color()
		{}
	}

    // function templates
    public delegate void VoidFunction();

    public delegate void VoidFunction<A>(A a);
    public delegate void VoidFunction<A, B>(A a, B b);
    public delegate void VoidFunction<A, B, C>(A a, B b, C c);
    public delegate void VoidFunction<A, B, C, D>(A a, B b, C c, D d);

    public delegate R Function<R>();
    public delegate R Function<R, A>(A a);
    public delegate R Function<R, A, B>(A a, B b);
    public delegate R Function<R, A, B, C>(A a, B b, C c);
    public delegate R Function<R, A, B, C, D>(A a, B b, C c, D d);

	public class SingletonWrapper<T>
	{
		public T wrapped;
	}

	static public class Singleton<T>
	{
	    private static T _instance;
	    private static readonly object LOCK = new object();

	    public static T Instance
	    {
	        get
	        {
		        lock (LOCK)
	                if (_instance == null)
	                    _instance = (T)System.Activator.CreateInstance(typeof(T), true);
	            return _instance;
	        }
	    }
	}
}
